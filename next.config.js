/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  // output: "export",
  transpilePackages: ['@mui/x-charts'],
  eslint: {
    ignoreDuringBuilds: true,
  },
}

module.exports = nextConfig
