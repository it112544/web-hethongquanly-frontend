
declare module '@ckeditor/ckeditor5-build-classic' {
    const ClassicEditorBuild: any;

    export = ClassicEditorBuild;
}
declare module 'quill-image-resize-module-react' {
    const ImageResize: any;
    export default ImageResize;
}

declare module '@mui/icons-material/RemoveCircleOutline';
declare module '@mui/icons-material/Visibility';
declare module '@mui/icons-material/VisibilityOff';
declare module '@mui/icons-material/WorkHistory';
declare module '@mui/icons-material/VpnKey';
declare module '@mui/icons-material/VisibilityOutlined';
declare module '@mui/icons-material/RemoveRedEyeTwoTone';
declare module '@mui/icons-material/Queue';
declare module '@mui/icons-material/RequestQuote';
declare module '@mui/icons-material/UploadFileOutlined';
declare module '@mui/icons-material/ZoomOutMap';
declare module '@mui/icons-material/RemoveCircle';
declare module '@mui/icons-material/ZoomIn';
declare module '@mui/icons-material/PictureAsPdf';
