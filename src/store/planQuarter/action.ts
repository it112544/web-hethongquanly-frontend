import { PlanQuarter } from '@/interfaces/plan'
import { createAction } from '@reduxjs/toolkit'


export const GET_ALL = createAction<{ plan: PlanQuarter[] }>('@planQuarter/GET_ALL')
export const ADD_PLAN_QUARTER = createAction<{ plan: PlanQuarter }>('@planQuarter/ADD_PLAN_QUARTER')
export const UPDATE_PLAN_QUARTER = createAction<{ plan: PlanQuarter, id: number }>('@planQuarter/UPDATE_PLAN_QUARTER')
export const DELETE_PLAN_QUARTER = createAction<{ id: number }>('@planQuarter/DELETE_PLAN_QUARTER')