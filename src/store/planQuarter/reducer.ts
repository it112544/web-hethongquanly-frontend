import { createReducer } from '@reduxjs/toolkit'
import { ADD_PLAN_QUARTER, DELETE_PLAN_QUARTER, GET_ALL, UPDATE_PLAN_QUARTER } from './action'
import { PlanQuarter } from '@/interfaces/plan';

export const initialState: PlanQuarter[] = []
export default createReducer(initialState, (builder) =>
    builder
        .addCase(GET_ALL, (state, action) => {
            return action.payload.plan;
        })
        .addCase(ADD_PLAN_QUARTER, (state, action) => {
            state.push(action.payload.plan)
        })
        .addCase(UPDATE_PLAN_QUARTER, (state, action) => {
            const updatedItems = state.map(item => {
                if (item.quyID === action.payload.id) {
                    return { ...item, ...action.payload.plan };
                }
                return item;
            });
            return updatedItems
        })
        .addCase(DELETE_PLAN_QUARTER, (state, action) => {
            return state.filter(item => item.quyID !== action.payload.id)
        })
)