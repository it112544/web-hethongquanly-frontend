
export interface BidReportLevel {
    capBaoCaoThauID?: number;
    tenCapBaoCaoThau: string,
    chuDauTu?: ChuDauTu[],
}
export interface ChuDauTu {
    baoCaoThauID: number,
    chuDauTu: string | number,
    tinhID: number,
    huyenID: number,
}