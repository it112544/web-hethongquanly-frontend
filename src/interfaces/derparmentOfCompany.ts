export interface DerparmentOfCompany {
    phongBanID: number,
    tenPhongBan: string,
    chiTiet: string,
    congTyID: number,
    active?: boolean
    congTy?: string,
    chucVu?: string,
}