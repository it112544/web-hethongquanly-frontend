export interface ContentWorkPlan{
    id: number,
    noiDen: string,
    tenBuocThiTruong: string,
    chiTiet: string,
    ngayThucHien: string,
    ghiChu: string,
    coQuanID: number,
    ngayID: number
}