export interface ImportResult {
    error: number,
    success: number,
    importError: ImportError[],
}
export interface ImportError {
    stt: string,
    maLoi: string,
}
