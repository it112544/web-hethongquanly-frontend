export interface StaffInCharge {
  id: number;
  tenNguoiPhuTrach: string;
  soDienThoai: string;
  chucVu: string;
}
