import { BidReportLevel } from "./bidReportLevel";
import { BidReportType } from "./bidReportType";
import { Contractors } from "./contractors";
import { District } from "./district";
import { FieldType } from "./fieldType";
import { Province } from "./province";

export interface BidReport {
    baoCaoThauID?: number,
    tenGoiThau: string,
    tbmt: string,
    benMoiThau: string,
    chuDauTu: string,
    thoiGianThucHienHopDong: string,
    ngayDangTai: string,
    thoiGianDongMo: string,
    giaGoiThau: number;
    tienDamBaoDuThau: number,
    nguonVon: string,
    nhaThauTrung?: string,
    diaChi?: string,
    nguoiDaiDien?: string,
    maDinhDanh?: string,
    tinhID?: number;
    huyenID?: number;
    capBaoCaoThauID: number;
    loaiBaoCaoThauID: number;
    nhaThauID?: number;
    tinh?: Province,
    huyen?: District,
    loaiLinhVucs: FieldType[],
    nhaThau?: Contractors;
    capBaoCaoThau?: BidReportLevel;
    loaiBaoCaoThau?: BidReportType;

}
export interface CreateBidReportModel {
    baoCaoThauID?: number,
    tenGoiThau: string,
    tBMT: string,
    benMoiThau: string,
    chuDauTu: string,
    thoiGianThucHienHopDong: string,
    ngayDangTai: string,
    thoiGianDongMo: string,
    giaGoiThau: number;
    tienDamBaoDuThau: number,
    nguonVon: string,
    nhaThauTrung: string,
    diaChi: string,
    nguoiDaiDien: string,
    tinhID?: number;
    huyenID?: number;
    LoaiLinhVucIDs: number,
    nhaThau: Contractors;
}
export interface CreateBidReportDto {
    baoCaoThauDto: BidReport
    fileBaoCaoThauDtos: FileBidReportDto[]
}
export interface FileBidReportDto {
    fileID?: number | null,
    fileName: string,
    fileType: string,
    fileUrl: string,
    link?: string;
    loai?: number
}