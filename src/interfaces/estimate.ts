interface Estimates {
  duToanID?: number;
  tenDuToan: string;
  ghiChu?: string;
  sanPhamDuToan: string;
  tenNguoiTao: string;
  tenTrangThai: string;
  trangThaiID: number;
  loaiGia: string;
  tongGiaVon: number;
  tongGiaBan: number;
}
interface StatusEstimates {
  trangThaiID: number;
  tenTrangThai: string;
}
