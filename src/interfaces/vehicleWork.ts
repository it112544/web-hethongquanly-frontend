export interface VehicleWork {
    id: number,
    noiDi: string,
    noiDen: string,
    kmTamTinh: number,
    ngaySuDung: string,
    ghiChu: string,
    ngayID: number
}