export interface SampleEmail {
    sampleEmailID?:number;
    subject: string;
    bodyContent: string;
  }
  export interface FileSampleMailDto {
    fileID?: number | null,
    fileName: string,
    fileType: string,
    fileUrl: string,
    loai?: number
  }
  export interface CreateSampleEmailDto {
    sampleEmailDto: SampleEmail
    fileSampleEmailDto: FileSampleMailDto[]
}