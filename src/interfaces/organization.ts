import { FileSendMailDto } from "./contractors";
import { District } from "./district";
import { Officers } from "./officers";
import { Province } from "./province";

export interface Organization {
    coQuanID?: number,
    tenCoQuan: string,
    maSoThue: string,
    email?: string,
    loaiCoQuanID?: number,
    soDienThoai?: string,
    huyenID: number,
    tinhID: number,
    diaChi?: string,
    xaID: number,
    nhanVienID?: number,
    huyen?: District,
    tinh?: Province,
    nsCanBo?: Officers[]
}
export interface SendMailOrganization {
    provinceID: number;
    districtID: number;
    loaiCoQuanID: number;
    subject: string;
    bodyContent: string;
    fileSendMailDtos: FileSendMailDto[]
}