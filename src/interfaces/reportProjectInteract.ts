import { District } from "./district"
import { Interaction } from "./interaction"
import { Officers } from "./officers"
import { ProductTypes } from "./productTypes"
import { ProjectEstimate } from "./projectEstimate"
import { Province } from "./province"
import { Step } from "./step"
import { Staff } from "./user"

export interface ReportProjectInteract {
    coQuanID: number,
    tenCoQuan: string,
    thoiGian: string,
    doanhThuThucTe: number,
    doanhThuDuKien: number,
    ketQua: boolean,
    buocThiTruong: Step,
    thoiGianKetThucDuKien: string,
    loaiSanPhamDTO?: ProductTypes[];
    loaiSanPham?: ProductTypes[];
    nhanVien: {
        nhanVienID: number,
        tenNhanVien: string,
        lstChucVuView?: [
            {
                lstChucVu: {
                    chucVuID: number,
                    tenChucVu: string,
                    chiTiet: string,
                    phongBanID: number
                },
                lstPhongBan: {
                    phongBanID: number,
                    tenPhongBan: string,
                    chiTiet: string,
                    congTyID: number
                },
                lstCongTy: {
                    congTyID: number,
                    tenCongTy: string,
                    maSoThue: string,
                    tinhID: number,
                    diaChi: string
                }
            }
        ]
    };
    nguoiTao: {
        nhanVienID: number,
        tenNhanVien: string,
    };
    nguoiVanHanh: {
        nhanVienID: number,
        tenNhanVien: string,

    };
}