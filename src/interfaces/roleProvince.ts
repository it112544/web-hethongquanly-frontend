export interface RoleProvince {
    id: number,
    nhanVienID: number,
    tinhID: number
}
export interface RoleProvinceByUser{
    nhanVienID: any,
    tinhID: number[]
}