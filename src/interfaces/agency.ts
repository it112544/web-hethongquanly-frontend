export interface Agency {
    daiLyID: number,
    tenDaiLy: string,
    maSoThue: string,
    diaChi: string,
    nguoiDaiDien: string,
    ghiChu: string,
    tinhID: number
    nhanVienPhuTrach: string,
    chucVu: string,
    soDienThoai: string,
    loaiDLID: number,
    nddChucVu: string,
    nddSoDienThoai: string,
    nhanVienID:number
}
export interface AgencyType {
    loaiDLID: number,
    tenLoai: string
}