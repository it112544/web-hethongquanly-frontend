import { Capital } from "./capital"
import { Companys } from "./companys"
import { DerparmentOfCompany } from "./derparmentOfCompany"
import { Organization } from "./organization"
import { Position } from "./position"
import { ProductTypes } from "./productTypes"
import { Step } from "./step"
import { Staff } from "./user"

export interface Interaction {
  tuongTacID: number,
  coQuanID: number,
  tenCoQuan: string,
  nsCoQuan: Organization,
  thoiGian: string,
  thongTinLienHe: string,
  thongTinTiepXuc: string,
  canBoTiepXuc: string,
  nhomHangQuanTam: string,
  ghiChu: string,
  active: boolean,
  doanhThuDuKien: number,
  buocThiTruongID: number,
  buocThiTruong: Step,
  nhanVienID: number,
  nhanVien: Staff,
  tenNhanVien: string;
  nguonVonID: number,
  nsNguonVon: Capital,
  thoiGianKetThucDuKien?: string,
  createDate: string,
  createBy: string,
  updateDate: string,
  loaiDuAnID?: number | null;
  updateBy: string,
  activeDate: string,
  activeBy: string,
  loaiSanPhamDTO?: ProductTypes[];
  loaiSanPham?: ProductTypes[];

}
export interface DetailInteraction {
  tuongTacID: number,
  nsCoQuan: Organization,
  thoiGian: string,
  thongTinLienHe: string,
  thongTinTiepXuc: string,
  canBoTiepXuc: string,
  nhomHangQuanTam: string,
  ghiChu: string,
  doanhThuDuKien: number,
  nhanVienID: number,
  buocThiTruong: Step
  nhanVien: Staff
  lstChucVuView: [
    {
      lstChucVu: Position,
      lstPhongBan: DerparmentOfCompany
      lstCongTy: Companys
    },
  ]
}