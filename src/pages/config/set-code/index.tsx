import SetCodePage from "@/views/config/set-code";

const IndexPage = () => {
    return <SetCodePage />;
};

export default IndexPage;