import ConfigNotificationPage from "@/views/config/notification";

const IndexPage = () => {
    return <ConfigNotificationPage />;
};

export default IndexPage;