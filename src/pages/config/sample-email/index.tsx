import SampleEmailPage from "@/views/config/sample-email";

const IndexPage = () => {
    return <SampleEmailPage />;
};

export default IndexPage;