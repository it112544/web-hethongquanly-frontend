import FieldTypePage from "@/views/bid-report-management/field-type/index";

const IndexPage = () => {
    return <FieldTypePage />;
};

export default IndexPage;