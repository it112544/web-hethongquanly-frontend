import BidReportPage from "@/views/bid-report-management/bid-report/index";

const IndexPage = () => {
    return <BidReportPage />;
};

export default IndexPage;