import axios from 'axios';
import { useState, useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../store/hook';
import { ADD_PLAN_QUARTER, DELETE_PLAN_QUARTER, GET_ALL, UPDATE_PLAN_QUARTER } from '@/store/planQuarter/action';
import { PlanQuarter } from '@/interfaces/plan';
import { addKeHoachQuy, confirmKeHoachQuy, deleteKeHoachQuy, getKeHoachQuy, updateKeHoachQuy } from '@/constant/api';

export default function usePlanQuarter() {
    const dataPlanQuarter = useAppSelector((state) => state.planQuarter)
    const [isLoadding, setIsLoading] = useState(true);
    const dispatch = useAppDispatch();

    const getAllPlanQuarter = async () => {
        try {
            const accessToken = window.localStorage.getItem('accessToken');
            const headers = { Authorization: `Bearer ${accessToken}` };
            const response = await axios.get(getKeHoachQuy, { headers });
            dispatch(GET_ALL({ plan: response.data }))
        } catch (e) {
            console.error("Error: ", e);
        } finally {
            setIsLoading(false)
        }
    }


    const addPlanQuarter = async (plan: PlanQuarter) => {
        try {
            const accessToken = window.localStorage.getItem('accessToken');
            const headers = { Authorization: `Bearer ${accessToken}`, 'Content-Type': 'application/json' };
            const response = await axios.post(addKeHoachQuy, plan, { headers });
            if (response.status === 200) {
                //dispatch(ADD_PLAN_QUARTER({ plan: response.data }))
                getAllPlanQuarter()
                return true
            }
        } catch (e) {
            console.error("Error: ", e);
            return false
        } finally {
            setIsLoading(false)
        }
    }

    const updatePlanQuarter = async (plan: PlanQuarter) => {
        try {
            const accessToken = window.localStorage.getItem('accessToken');
            const headers = { Authorization: `Bearer ${accessToken}`, 'Content-Type': 'application/json' };
            const response = await axios.put(updateKeHoachQuy, plan, { headers });
            if (response.status === 200) {
                getAllPlanQuarter()
                return true
            }
        } catch (e) {
            console.error("Error: ", e);
            return false
        } finally {
            setIsLoading(false)
        }
    }
    const confirmPlanQuarter = async (id: any, isApprove: boolean, planQuarter: PlanQuarter) => {
        try {
            const accessToken = window.localStorage.getItem('accessToken');
            const headers = { Authorization: `Bearer ${accessToken}`, 'Content-Type': 'application/json' };
            const response = await axios.post(`${confirmKeHoachQuy}/${id}?trangThai=${isApprove}`, planQuarter, { headers });
            if (response.status === 200) {
                getAllPlanQuarter()
                return true
            }
        } catch (e) {
            console.error("Error: ", e);
            return false
        } finally {
            setIsLoading(false)
        }
    }
    const deletePlanQuarter = async (id: number) => {
        try {
            const response = await axios.delete(deleteKeHoachQuy, { params: { id } });
            if (response.status === 200) {
                dispatch(DELETE_PLAN_QUARTER({ id }))
                return true
            }
        } catch (e) {
            console.error("Error: ", e);
            return false
        } finally {
            setIsLoading(false)
        }
    }
    const deleteMulPlanQuarter = async (ids: number[]) => {
        let flag = true
        ids.map(async (id) => {
            try {
                const response = await axios.delete(deleteKeHoachQuy, { params: { id } });
                if (response.status === 200) {
                    dispatch(DELETE_PLAN_QUARTER({ id }))
                    flag = true;
                }
            } catch (e) {
                console.error("Error: ", e);
                flag = false;
            } finally {
                setIsLoading(false)
            }
        })
        return flag;
    }
    return {
        isLoadding,
        dataPlanQuarter,
        getAllPlanQuarter,
        addPlanQuarter,
        updatePlanQuarter,
        deletePlanQuarter,
        deleteMulPlanQuarter,
        confirmPlanQuarter
    };
}
