/* eslint-disable react-hooks/exhaustive-deps */
import MainCard from "@/components/card/MainCard"
import BasicComposition from "@/components/chart/ChartResponsive"
import ColumnBarChart from "@/components/chart/ColumnChart"
import MUIPieActiveArc from "@/components/chart/MUIPieActiveArc"
import OpportunityValueProjectEstimateChart from "@/components/chart/OpportunityValueProjectEstimateChart"
import ProjectEstimateChart from "@/components/chart/OpportunityValueProjectEstimateChart"
import ProjectRevenueEstimateChart from "@/components/chart/ProjectRevenueEstimateChart"
import useCompanys from "@/hooks/useCompanys"
import useInteraction from "@/hooks/useInteraction"
import useOrganization from "@/hooks/useOrganization"
import useStaff from "@/hooks/useStaff"
import { Companys } from "@/interfaces/companys"
import { DetailInteraction, Interaction } from "@/interfaces/interaction"
import { ReportProjectInteract } from "@/interfaces/reportProjectInteract"
import { Step } from "@/interfaces/step"
import { Staff, StaffDetail } from "@/interfaces/user"
import { formatCurrency } from "@/utils/formatCurrency"
import { Box, Grid, Typography, useTheme } from "@mui/material"
import { useRouter } from "next/router"
import { useEffect, useMemo, useRef, useState } from "react"
interface Props {

    dataReportProjectInteract: ReportProjectInteract[],
    dataAllStaff: Staff[],
    filterStaff: number,
    filterResult: string | number | null
}
const ChartOverviewAllStaff = ({ dataAllStaff, dataReportProjectInteract, filterResult, filterStaff }: Props) => {
    const theme = useTheme()
    const router = useRouter()
    const elementRef = useRef<any>(null);
    const [dataLabel, setDataLabel] = useState<string[]>([])
    const [dataLabelExpected, setDataLabelExpected] = useState<string[]>([])
    const [dataLabelActual, setDataLabelActual] = useState<string[]>([])

    useEffect(() => {
        dataAllStaff?.map(item => {
            setDataLabel((prevState) => {
                return [...prevState, item?.tenNhanVien]
            })
        })

    }, [dataAllStaff, dataReportProjectInteract])
    console.log("dataStaffDepartment", dataLabel);

    const countExpectedValueByStaff = (nhanVienID: any) => {
        let count = 0;

        // Kiểm tra nếu filterResult === 0 thì không áp dụng bất kỳ bộ lọc nào
        if (filterResult === 0) {
            dataReportProjectInteract?.map((item) => {
                if (item?.nhanVien?.nhanVienID === nhanVienID) {
                    count += item.doanhThuDuKien; // Cộng doanh thu dự kiến vào count
                }
            });
        } else {
            // Lọc theo filterResult như bình thường
            dataReportProjectInteract?.map((item) => {
                // Nếu filterResult là null, chỉ lọc các item có ketQua === null
                if (filterResult === 'null' && item.ketQua === null) {
                    if (item?.nhanVien?.nhanVienID === nhanVienID) {
                        count += item.doanhThuDuKien;
                    }
                }
                // Nếu filterResult là "true", chỉ lọc các item có ketQua === true
                else if (filterResult === "true" && item.ketQua === true) {
                    if (item?.nhanVien?.nhanVienID === nhanVienID) {
                        count += item.doanhThuDuKien;
                    }
                }
                // Nếu filterResult là "false", chỉ lọc các item có ketQua === false
                else if (filterResult === "false" && item.ketQua === false) {
                    if (item?.nhanVien?.nhanVienID === nhanVienID) {
                        count += item.doanhThuDuKien;
                    }
                }
            });
        }
        return count;
    };
    console.log("dsdsadafgdwesrqwds", filterResult);

    const convertToAbbreviation = (fullName: string) => {
        const nameParts = fullName.split(' '); // Tách tên thành mảng các phần
        const lastName = nameParts[0]; // Họ
        const firstName = nameParts[nameParts.length - 1]; // Tên cuối
        const middleName = nameParts.slice(1, -1).join(' '); // Tên đệm (nếu có)

        // Lấy chữ cái đầu tiên của họ và tên đệm, nếu có
        const abbreviatedMiddleName = middleName ? middleName.split(' ').map(word => word.charAt(0).toUpperCase()).join('.') + '.' : '';

        // Ghép lại họ + tên viết tắt + tên cuối
        return `${lastName.charAt(0).toUpperCase()}.${abbreviatedMiddleName}${firstName}`;
    };
    const countActualValueByStaff = (nhanVienID: any) => {
        let count = 0;

        // Kiểm tra nếu filterResult === 0 thì không áp dụng bất kỳ bộ lọc nào
        if (filterResult === 0) {
            dataReportProjectInteract?.map((item) => {
                if (item?.nhanVien?.nhanVienID === nhanVienID) {
                    count += item.doanhThuThucTe; // Cộng doanh thu thực tế vào count
                }
            });
        } else {
            // Lọc theo filterResult như bình thường
            dataReportProjectInteract?.map((item) => {
                // Nếu filterResult là null, chỉ lọc các item có ketQua === null
                if (filterResult === 'null' && item.ketQua === null) {
                    if (item?.nhanVien?.nhanVienID === nhanVienID) {
                        count += item.doanhThuThucTe;
                    }
                }
                // Nếu filterResult là "true", chỉ lọc các item có ketQua === true
                else if (filterResult === "true" && item.ketQua === true) {
                    if (item?.nhanVien?.nhanVienID === nhanVienID) {
                        count += item.doanhThuThucTe;
                    }
                }
                // Nếu filterResult là "false", chỉ lọc các item có ketQua === false
                else if (filterResult === "false" && item.ketQua === false) {
                    if (item?.nhanVien?.nhanVienID === nhanVienID) {
                        count += item.doanhThuThucTe;
                    }
                }
            });
        }
        return count;
    };


    const dataExpectedValuesByStaffOfCompany = useMemo(() => {
        let data: number[] = [];
        let labels: string[] = [];

        dataAllStaff?.forEach(item => {
            const expectedValue = countExpectedValueByStaff(item?.nhanVienID);

            // Kiểm tra nếu giá trị lớn hơn 0 thì mới thêm vào data và labels
            if (expectedValue > 0) {
                data.push(expectedValue);
                const abbreviatedName = convertToAbbreviation(item?.tenNhanVien);
                labels.push(abbreviatedName); // Thêm tên viết tắt vào labels
            }
        });

        // Kết hợp data và labels thành mảng các đối tượng để sắp xếp
        const combined = data
            .map((value, index) => ({
                value,
                label: labels[index], // Lưu tên tương ứng với mỗi giá trị
            }))
            .sort((a, b) => b.value - a.value); // Sắp xếp theo giá trị giảm dần

        // Tách lại mảng data và labels sau khi sắp xếp
        const sortedData = combined.map(item => item.value); // Mảng giá trị đã sắp xếp
        const sortedLabels = combined.map(item => item.label); // Mảng tên đã sắp xếp theo giá trị

        // Cập nhật state dataLabelExpected nếu cần thiết
        setDataLabelExpected(sortedLabels);

        return sortedData; // Trả về mảng giá trị mong đợi đã sắp xếp
    }, [dataAllStaff, filterResult, filterStaff]);

    const dataActualValuesByStaffOfCompany = useMemo(() => {
        let data: number[] = [];
        let labels: string[] = [];

        dataAllStaff?.forEach(item => {
            const expectedValue = countActualValueByStaff(item?.nhanVienID);

            // Kiểm tra nếu giá trị lớn hơn 0 thì mới thêm vào data và labels
            if (expectedValue > 0) {
                data.push(expectedValue);
                const abbreviatedName = convertToAbbreviation(item?.tenNhanVien);
                labels.push(abbreviatedName); // Thêm tên viết tắt vào labels
            }
        });

        // Kết hợp data và labels thành mảng các đối tượng để sắp xếp
        const combined = data
            .map((value, index) => ({
                value,
                label: labels[index], // Lưu tên tương ứng với mỗi giá trị
            }))
            .sort((a, b) => b.value - a.value); // Sắp xếp theo giá trị giảm dần

        // Tách lại mảng data và labels sau khi sắp xếp
        const sortedData = combined.map(item => item.value); // Mảng giá trị đã sắp xếp
        const sortedLabels = combined.map(item => item.label); // Mảng tên đã sắp xếp theo giá trị

        // Cập nhật state dataLabelExpected nếu cần thiết
        setDataLabelActual(sortedLabels);

        return sortedData; // Trả về mảng giá trị mong đợi đã sắp xếp
    }, [dataReportProjectInteract, dataAllStaff, filterResult, filterStaff]);
    // const dataActualValuesByStaffOfCompany = useMemo(() => {
    //     let data: number[] = []
    //     dataAllStaff?.map(item => {
    //         data.push(countActualValueByStaff(item?.nhanVienID))

    //     })
    //     return data
    // }, [countActualValueByStaff, dataReportProjectInteract])


    // const dataLabelsByStaffOfCompany =() => {
    //     console.log("dataStaffDepartment",dataAllStaff);
    //     let data: string[] = []
    //     dataAllStaff?.map(item => {
    //         data.push(item?.tenNhanVien)
    //     })
    //     return data
    // }

    const totalActualValueByCompany = () => {
        let count = 0
        dataActualValuesByStaffOfCompany?.map((item) => {
            count += item
        })
        return count
    }
    const totalExpectedValueByCompany = () => {
        let count = 0
        dataExpectedValuesByStaffOfCompany?.map((item) => {
            count += item
        })
        return count
    }
    const briefCompanyName = (companyName: string) => {
        if (companyName?.toLowerCase().includes("công ty cổ phần giáo dục")) {
            return companyName?.toLowerCase()?.replace("công ty cổ phần giáo dục", "").trim().toUpperCase()
        }
        if (companyName?.toLowerCase().includes("công ty tnhh công nghệ - thiết bị - giáo dục")) {
            return companyName?.toLowerCase()?.replace("công ty tnhh công nghệ - thiết bị - giáo dục", "").trim().toUpperCase()
        }
        if (companyName?.toLowerCase().includes("công ty cổ phần phát triển")) {
            return companyName?.toLowerCase()?.replace("công ty cổ phần phát triển", "").trim().toUpperCase()
        }
        else if (companyName?.includes("Công ty Cổ phần Giải pháp Công nghệ")) {
            return companyName?.toLowerCase()?.replace("công ty cổ phần giải pháp công nghệ", "").trim().toUpperCase()
        }
        return companyName?.toUpperCase()
    }



    return (
        <Grid container spacing={2}>
            <Grid item xs={12} xl={12}>
                <Box sx={{ border: 1, borderColor: theme.palette.grey[400], borderRadius: '8px', p: 3 }}>
                    <Typography variant="h6">Giá trị cơ hội của nhân viên</Typography>
                    {/* <BasicComposition
                        data={dataExpectedValuesByStaffOfCompany.slice(0, 8)}
                        labels={dataLabelExpected.slice(0, 8)}
                    /> */}
                    <OpportunityValueProjectEstimateChart
                        dataReportProjectInteract={dataReportProjectInteract}
                        dataStaffs={dataAllStaff}
                        filterResult={filterResult}
                    />
                    <Typography py={1} align="center">Tống giá trị cơ hội: <span style={{ fontWeight: "bold" }}>{formatCurrency(totalExpectedValueByCompany())}</span></Typography>
                </Box>
            </Grid>
            <Grid item xs={12} xl={12}>
                <Box sx={{ border: 1, borderColor: theme.palette.grey[400], borderRadius: '8px', p: 3 }}>
                    <Typography variant="h6">Doanh thu theo nhân viên</Typography>

                    {/* <BasicComposition
                        data={dataActualValuesByStaffOfCompany.slice(0.8)}
                        labels={dataLabelActual!.slice(0, 8)}
                    /> */}
                    <ProjectRevenueEstimateChart
                        dataReportProjectInteract={dataReportProjectInteract}
                        dataStaffs={dataAllStaff}
                        filterResult={filterResult}
                    />
                    <Typography py={1} align="center">Tống doanh thu: <span style={{ fontWeight: "bold" }}>{formatCurrency(totalActualValueByCompany())}</span></Typography>
                </Box>
            </Grid>
        </Grid>

    )
}
export default ChartOverviewAllStaff