import { CustomInput } from "@/components/input";
import SearchNoButtonSection from "@/components/search/SearchNoButton";
import { BAN_THI_TRUONG_GIAM_DOC_CHI_NHANH, BAN_THI_TRUONG_GIAM_DOC_DU_AN, BAN_THI_TRUONG_NHAN_VIEN_KINH_DOANH, PHO_TONG_GIAM_DOC, QUAN_TRI, BAN_THI_TRUONG_GIAM_DOC_KINH_DOANH, TONG_GIAM_DOC } from "@/constant/role";
import useRole from "@/hooks/useRole";
import { Box, Card, FormControl, Grid, InputLabel, MenuItem, Select, useTheme } from "@mui/material";
import { useRouter } from "next/router";
import { useEffect, useMemo, useState } from "react";
import { StyledButton } from "@/components/styled-button";
import CustomDialog from "@/components/dialog/CustomDialog";
import FormPlanMonth from "@/components/form/FormPlanMonth";
import { toast } from "react-toastify";
import usePlanMonth from "@/hooks/usePlanMonth";
import { HeadCell } from "@/components/table/table-custom/type";
import useStaff from "@/hooks/useStaff";
import { IconPlus } from "@tabler/icons-react";
import TableAccordion from "@/components/table/table-accordion";
import TablePlanMonth from "./TablePlanMonth";

const headCells: HeadCell[] = [
    {
        id: 1,
        numeric: false,
        disablePadding: false,
        label: 'Tháng',
    },
    {
        id: 2,
        numeric: false,
        disablePadding: false,
        label: 'Năm',
    },
    {
        id: 3,
        numeric: false,
        disablePadding: false,
        label: 'Tiêu đề',
    },

    {
        id: 4,
        numeric: false,
        disablePadding: false,
        label: 'Ngày lập kế hoạch',
    },
    {
        id: 5,
        numeric: false,
        disablePadding: false,
        label: 'Người tạo',
    },
    {
        id: 6,
        numeric: false,
        disablePadding: false,
        label: 'Trạng thái',
    },
    {
        id: 7,
        numeric: false,
        disablePadding: false,
        label: 'Người duyệt',
    },
    {
        id: 8,
        numeric: true,
        disablePadding: false,
        label: 'Chi tiết',
    },
];
const TabPlanMonth = () => {
    /* Library Hook */
    const router = useRouter()
    const theme = useTheme()

    /* Custom Hook */
    const { dataStaff } = useStaff()
    const { getAllPlanMonth, updatePlanMonth, addPlanMonth, dataPlanMonth, deleteMulPlanMonth } = usePlanMonth()

    /* State */
    const [dataFilter, setDataFilter] = useState<any>();
    const [viewId, setViewId] = useState<number>(0);
    const [editId, setEditId] = useState<number>(0);
    const [openAddDialog, setOpenAddDialog] = useState(false);
    const [isOpenEditCard, setOpenEditCard] = useState<boolean>(false);
    const [isOpenViewCard, setOpenViewCard] = useState<boolean>(false);
    const [selected, setSelected] = useState<number[]>([]);
    const [contentSearch, setContentSearch] = useState<string>('');
    const [filterModify, setFilterModify] = useState<string>("0");
    const [filterStatus, setFilterStatus] = useState<number>(0);
    const [openEditDialog, setOpenEditDialog] = useState(false);
    const [filterMonth, setFilterMonth] = useState<number>(0);
    const [filterYear, setFilterYear] = useState<number>(2025);
    const [filterModifyApproved, setFilterModifyApproved] = useState<string>("0");

    useEffect(() => {
        getAllPlanMonth()
    }, [])

    /* Filter*/
    const handleChangeFilter = (e: any, setter: Function) => {
        setter(e.target.value);
    };
    const infoByEditID = dataPlanMonth.find((item) => item.thangID === editId)

    const handleDelete = async (ids: number[]) => {
        console.log("delete", ids);
        const rs = await deleteMulPlanMonth(ids)
        if (rs) toast.success("Xoá thành công")
        else toast.error("Xoá thất bại")
        setSelected([])
        setOpenViewCard(false)
    }

    const handleEdit = async (id: number) => {
        setEditId(id)
        setOpenEditDialog(true)
        setOpenEditCard(id !== 0)
    }
    const infoByID = dataPlanMonth.find((item) => item.thangID === viewId)

    const filterDataModify = useMemo(() => {
        // Mảng để lưu các người nhập không trùng lặp
        const uniquePersonModify: any[] = [];

        // Lặp qua mảng đối tượng
        dataPlanMonth?.forEach(plan => {
            const name = dataStaff.find((item, index) => item.tenNhanVien === plan.createBy)?.tenNhanVien

            // Kiểm tra xem người nhập đã tồn tại trong mảng chưa
            if (!uniquePersonModify.find(unique => unique.tenNhanVien === plan.createBy)) {
                if (name) {
                    uniquePersonModify.push({ nhanVienID: plan.createBy, tenNhanVien: name });
                }
                // Nếu chưa tồn tại, thêm người nhập vào mảng 
            }
        });

        return uniquePersonModify
    }, [dataPlanMonth, dataStaff])
    const filterDataModifyApproved = useMemo(() => {
        // Mảng để lưu các người nhập không trùng lặp
        const uniquePersonModify: any[] = [];

        // Lặp qua mảng đối tượng
        dataPlanMonth?.forEach(plan => {
            const name = dataStaff.find((item, index) => item.tenNhanVien === plan.nguoiDuyet)?.tenNhanVien

            // Kiểm tra xem người nhập đã tồn tại trong mảng chưa
            if (!uniquePersonModify.find(unique => unique.tenNhanVien === plan.nguoiDuyet)) {
                if (name) {
                    // Nếu chưa tồn tại, thêm người nhập vào mảng 
                    uniquePersonModify.push({ nhanVienID: plan.nguoiDuyet, tenNhanVien: name });
                }
            }
        });

        return uniquePersonModify
    }, [dataPlanMonth, dataStaff])
    const filterData = useMemo(() => {
        if (!dataPlanMonth || dataPlanMonth.length === 0) {
            return [];
        }

        return dataPlanMonth.filter((item) => {
            const matchesSearch = !contentSearch || (item.tieuDe && item.tieuDe.includes(contentSearch));
            // Xác định trạng thái của item dựa vào điều kiện đánh giá
            let itemStatus = 1; // Mặc định là "Chưa duyệt"

            if (item.isApprove === true) {
                if (
                    !item.khcT_DanhGias.some(dg => dg.trangThai === null) &&
                    !item.khcT_CongViecPhatSinhs.some(cv => cv.trangThai === null)
                ) {
                    if (
                        item.khcT_DanhGias.some(dg => dg.trangThaiQuanLy === null) ||
                        item.khcT_CongViecPhatSinhs.some(cv => cv.trangThaiQuanLy === null)
                    ) {
                        itemStatus = 3; // NS đã ĐG
                    } else {
                        itemStatus = 4; // QL đã ĐG
                    }
                } else {
                    itemStatus = 2; // Đã duyệt
                }
            }
            if (item.isApprove === false) {
                itemStatus = 5;
            }
            // Kiểm tra xem item có khớp với bộ lọc trạng thái không
            const matchesStatus = filterStatus === 0 || itemStatus === filterStatus;
            const matchesModify = filterModify === "0" || item.createBy === filterModify
            const matchesModifyApproved = filterModifyApproved === "0" || item.nguoiDuyet === filterModifyApproved
            // Lọc theo quý, nhưng nếu filterQuarter === 0 thì không lọc
            const matchesMonth = filterMonth === 0 || item.thang === filterMonth;

            // Lọc theo năm, nhưng nếu filterYear === 0 thì không lọc
            const matchesYear = filterYear === 0 || item.nam === filterYear;
            if (filterStatus === 0 && filterModify === "0" && filterModifyApproved === "0" && filterMonth === 0 && filterYear === 0) {
                return matchesSearch
            }

            return matchesSearch && matchesModify && matchesStatus && matchesMonth && matchesYear && matchesModifyApproved;
        });
    }, [contentSearch, dataPlanMonth, filterModify, filterStatus, filterMonth, filterYear, filterModifyApproved]);

    return (
        <Grid container>
            {/* Filter và các hành động */}
            <Grid item xs={12}>
                <Box width='100%' bgcolor={theme.palette.background.paper} p={3}>
                    <Grid container spacing={1}>
                        <Grid item xs={12} md={4} lg={4}>
                            <SearchNoButtonSection fullwidth handleContentSearch={setContentSearch} contentSearch={contentSearch} />
                        </Grid>
                        {/* <Grid item xs={12} md={8} lg={8}>
                            <Box width="100%" display="flex" justifyContent="flex-end" gap={1}>
                                <FormControl variant="outlined" sx={{ minWidth: 140 }}>
                                    <InputLabel id="demo-simple-select-label-status" sx={{ color: theme.palette.text.primary }}>Trạng thái</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label-status"
                                        label="Trạng thái"
                                        name="status"
                                        value={filterStatus}
                                        onChange={(e) => handleChangeFilter(e, setFilterStatus)}
                                        input={<CustomInput size="small" label="Trạng thái" />}
                                    >
                                        <MenuItem value={0}>Tất cả</MenuItem>
                                        {[{ value: 1, name: "Chưa duyệt" }, { value: 2, name: "Đã duyệt" }].map((item: any, index) => (
                                            <MenuItem key={index} value={item.value}>{item.name}</MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                                <FormControl variant="outlined" sx={{ minWidth: 140 }}>
                                    <InputLabel id="demo-simple-select-label-modify" sx={{ color: theme.palette.text.primary }}>Người tạo</InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label-modify"
                                        label="Người tạo"
                                        name="created"
                                        value={filterModify}
                                        onChange={(e) => handleChangeFilter(e, setFilterModify)}
                                        input={<CustomInput size="small" label="Người tạo" />}
                                    >
                                        <MenuItem value='0'>Tất cả</MenuItem>
                                        {filterDataModify.map((item, index) => (
                                            <MenuItem key={index} value={item.nhanVienID}>{item.tenNhanVien}</MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </Box>
                        </Grid> */}
                    </Grid>
                </Box>
            </Grid>
            <Grid item xs={12}>
                <Box px={3} pb={3}
                    sx={{
                        maxHeight: 'calc(100vh - 200px)', // Set maximum height to prevent infinite scrolling
                    }} bgcolor={theme.palette.background.paper}>
                    <Grid container spacing={2}>
                        <Grid item xs={3}>
                            <FormControl variant="outlined" sx={{ width: "100%" }}>
                                <InputLabel id="demo-simple-select-label-status" sx={{ color: theme.palette.text.primary }}>Năm</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label-status"
                                    label="Năm"
                                    name="filterYear"
                                    value={filterYear}
                                    onChange={(e) => handleChangeFilter(e, setFilterYear)}
                                    input={<CustomInput size="small" label="Năm" />}
                                >
                                    <MenuItem value={0}>Tất cả</MenuItem>
                                    {[2025, 2026, 2027, 2028, 2029, 2030].map((item: any, index) => (
                                        <MenuItem key={index} value={item}>{'Năm ' + item}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={3}>
                            <FormControl variant="outlined" sx={{ width: "100%" }}>
                                <InputLabel id="demo-simple-select-label-status" sx={{ color: theme.palette.text.primary }}>Tháng</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label-status"
                                    label="Tháng"
                                    name="filterMonth"
                                    value={filterMonth}
                                    onChange={(e) => handleChangeFilter(e, setFilterMonth)}
                                    input={<CustomInput size="small" label="Tháng" />}
                                >
                                    <MenuItem value={0}>Tất cả</MenuItem>
                                    {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map((item: any, index) => (
                                        <MenuItem key={index} value={item}>{'Tháng ' + item}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={3}>
                            <FormControl variant="outlined" sx={{ width: "100%" }}>
                                <InputLabel id="demo-simple-select-label-status" sx={{ color: theme.palette.text.primary }}>Trạng thái</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label-status"
                                    label="Trạng thái"
                                    name="status"
                                    value={filterStatus}
                                    onChange={(e) => handleChangeFilter(e, setFilterStatus)}
                                    input={<CustomInput size="small" label="Trạng thái" />}
                                >
                                    <MenuItem value={0}>Tất cả</MenuItem>
                                    {[{ value: 1, name: "Chờ duyệt" }, { value: 2, name: "Đã duyệt" }, { value: 3, name: "NS đã ĐG", },
                                    { value: 4, name: "QL đã ĐG", }, { value: 5, name: "Từ chối", }
                                    ].map((item: any, index) => (
                                        <MenuItem key={index} value={item.value}>{item.name}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={3}>
                            <FormControl variant="outlined" sx={{ width: "100%" }}>
                                <InputLabel id="demo-simple-select-label-modify" sx={{ color: theme.palette.text.primary }}>Người tạo</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label-modify"
                                    label="Người tạo"
                                    name="created"
                                    value={filterModify}
                                    onChange={(e) => handleChangeFilter(e, setFilterModify)}
                                    input={<CustomInput size="small" label="Người tạo" />}
                                >
                                    <MenuItem value='0'>Tất cả</MenuItem>
                                    {filterDataModify.map((item, index) => (
                                        <MenuItem key={index} value={item.nhanVienID}>{item.tenNhanVien}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={3}>
                            <FormControl variant="outlined" sx={{ width: "100%" }}>
                                <InputLabel id="demo-simple-select-label-modify" sx={{ color: theme.palette.text.primary }}>Người duyệt</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label-modify"
                                    label="Người duyệt"
                                    name="filterModifyApproved"
                                    value={filterModifyApproved}
                                    onChange={(e) => handleChangeFilter(e, setFilterModifyApproved)}
                                    input={<CustomInput size="small" label="Người duyệt" />}
                                >
                                    <MenuItem value='0'>Tất cả</MenuItem>
                                    {filterDataModifyApproved.map((item, index) => (
                                        <MenuItem key={index} value={item.nhanVienID}>{item.tenNhanVien}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>
                </Box>
            </Grid>
            {/* Data */}
            <Grid item xs={12}>
                <Box px={3} py={1} display="flex" gap={2}>
                    <TablePlanMonth
                        title={"Lập kế hoạch"}
                        handleOpenCard={setOpenViewCard}
                        handleViewId={setViewId}
                        handleSelected={setSelected}
                        handleDelete={handleDelete}
                        handleEditId={handleEdit}
                        selected={selected}
                        rows={filterData}
                        isButtonEdit={true}
                        head={headCells}
                        orderByKey={""} contentSearch={""} />
                    {/* Dialog */}
                    {
                        openAddDialog === true && <CustomDialog
                            title={"Tạo kế hoạch tháng"}
                            open={openAddDialog}
                            handleOpen={setOpenAddDialog}
                            content={<FormPlanMonth buttonActionText="Trình kế hoạch" />}
                        />
                    }
                    {
                        infoByEditID && openEditDialog === true &&
                        <CustomDialog
                            title={"Cập nhật kế hoạch"}
                            open={openEditDialog}
                            handleOpen={setOpenEditDialog}
                            content={<FormPlanMonth isEdit={true} defaulValue={infoByEditID} handleOpen={setOpenEditDialog} buttonActionText="Chỉnh sửa" />}
                        />
                    }
                </Box>
            </Grid>
        </Grid>

    )
}
export default TabPlanMonth