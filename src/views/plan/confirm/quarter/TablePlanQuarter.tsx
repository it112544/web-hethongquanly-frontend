import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import { useEffect, useMemo, useRef, useState } from 'react';
import { Avatar, Button, Checkbox, Chip, ClickAwayListener, Fade, Grid, IconButton, Paper, Popper, PopperPlacementType, Switch, TableHead, TableRow, Typography, styled } from '@mui/material';
import { IconChevronRight, IconDotsVertical, IconEye, IconInfoSquare, IconPencil, IconPrinter } from '@tabler/icons-react';
import { useRouter } from 'next/router';
import EnhancedTableToolbar from '@/components/table/table-custom/TableTool';
import { StyledTableCell } from '@/components/table/table-custom/TableCell';
import TableCustomizePagination from '@/components/table/TablePagination';
import { Order, PropsTable } from '@/components/table/table-custom/type';
import { PlanQuarter } from '@/interfaces/plan';
import { StyledButton } from '@/components/styled-button';
import CustomDialog from '@/components/dialog/CustomDialog';
import usePlanQuarter from '@/hooks/usePlanQuarter';
import { toast } from 'react-toastify';
import useRoleLocalStorage from '@/hooks/useRoleLocalStorage';
import ReasonForRejectionDialog from '@/components/dialog/ReasonForRejectionDialog';
import AlertConfirmDialog from '@/components/alert/confirmAlertDialog';
import WorkAssessmentTable from '@/components/ui-components/WorkAssessmentTable';
import ReactToPrint from 'react-to-print';
import useStaff from '@/hooks/useStaff';
import { Staff } from '@/interfaces/user';
import PrintPlan from '@/components/ui-components/print/PrintPlan';
import PrintWorkAssessmentTable from '@/components/ui-components/print/PrintWorkAssessmentTable';
import PrintWorkArisesTable from '@/components/ui-components/print/PrintWorkArisesTable';
import WorkArisesTable from '@/components/ui-components/WorkArisesTable';
import { IconClipboardCheck } from '@tabler/icons-react';
const TypographyPrint = styled(Typography)(() => ({
    fontFamily: "Times New Roman",
    fontSize: "17px"
}))
export interface EnhancedTableProps {
    headCells: any;
}

const EnhancedTableHead = (props: EnhancedTableProps) => {
    return (
        <TableHead>
            <TableRow>
                {props?.headCells.map((headCell: any, idx: any) => (
                    <StyledTableCell
                        key={idx}
                        align={headCell.numeric ? 'center' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'normal'}
                    >
                        {headCell.label}
                    </StyledTableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

export default function TablePlanQuarter(props: PropsTable) {
    const { title, handleOpenCard, handleViewId, rows, orderByKey, head, handleSelected, selected, handleDelete, isButtonEdit, handleEditId } = props
    const theme = useTheme()
    const router = useRouter()

    const { confirmPlanQuarter } = usePlanQuarter()
    const { dataStaff } = useStaff()

    const { userID, roleName } = useRoleLocalStorage()
    const componentRef = useRef(null);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
    const [open, setOpen] = useState<number>(0);
    const [openDialog, setOpenDialog] = useState(false);
    const [placement, setPlacement] = useState<PopperPlacementType>();
    const [viewID, setViewID] = useState(0)
    const [openReasonForRejectionDialog, setReasonForRejectionDialog] = useState<boolean>(false);
    const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
    const [staff, setStaff] = useState<Staff>();
    const visibleRows = useMemo(() => {
        // Đảo ngược danh sách rows trước khi cắt các hàng hiện tại
        const reversedRows = [...rows].reverse();

        return reversedRows.slice(
            page * rowsPerPage,
            page * rowsPerPage + rowsPerPage
        );
    }, [page, rows, rowsPerPage]);
    useEffect(() => {
        setPage(0)
    }, [rows])
    const infoByID: PlanQuarter = visibleRows.find(item => item.quyID === viewID)

    useEffect(() => {
        if (infoByID && dataStaff) {
            let staff = dataStaff.find(x => x.nhanVienID === infoByID.nguoiTaoID)
            if (staff) {
                setStaff(staff);
            }
        }
    }, [infoByID, dataStaff])
    const content = (
        <>
            <Grid item sm={12}>
                <TypographyPrint variant='h6'>I. THÔNG TIN CHUNG</TypographyPrint>
            </Grid>
            <Grid item sm={12}>
                <TypographyPrint><span style={{ fontWeight: "bolder" }}>Quý:</span> {infoByID?.quy}/{infoByID?.nam}</TypographyPrint>
            </Grid>
            <Grid item sm={12}>
                <TypographyPrint><span style={{ fontWeight: "bolder" }}>Thời gian thực hiện:</span> {infoByID?.tuNgay?.slice(0, 16)} - {infoByID?.denNgay?.slice(0, 16)}</TypographyPrint>
            </Grid>
            <Grid item sm={12}>
                <TypographyPrint><span style={{ fontWeight: "bolder" }}>Người thực hiện:</span> {infoByID?.createBy}</TypographyPrint>
            </Grid>
            <Grid item sm={12}>
                <TypographyPrint><span style={{ fontWeight: "bolder" }}>Ngày tạo:</span> {infoByID?.tuNgay?.slice(0, 10)}</TypographyPrint>
            </Grid>
            <Grid item sm={12} sx={{ mb: 0 }}>
                <TypographyPrint><span style={{ fontWeight: "bolder" }}>Nội dung chính:</span>   <Box
                    sx={{
                        mb: 0,   // Adjust this if you need left margin as well\
                        lineHeight: 1, // Chỉnh độ cao dòng ở đây
                        textIndent: '20px',
                    }}
                    dangerouslySetInnerHTML={{ __html: ('    ' + infoByID?.noiDung!) }}
                />
                </TypographyPrint>
            </Grid>
            <Grid item sm={12} sx={{ mt: -2 }}>
                {
                    infoByID?.khcT_DanhGias?.length > 0 &&
                    <>
                        <TypographyPrint variant="h5" >Danh sách công việc:
                        </TypographyPrint>
                        <PrintWorkAssessmentTable defaulValue={infoByID} />
                    </>
                }
                {
                    infoByID?.khcT_CongViecPhatSinhs?.length > 0 &&
                    <>
                        <TypographyPrint variant="h5" sx={{ mt: 3 }} >Danh sách công việc phát sinh:
                        </TypographyPrint>
                        <PrintWorkArisesTable defaulValue={infoByID} />
                    </>
                }
            </Grid>
            <Grid item sm={12} className="no-break">
                <Grid container spacing={1}>
                    <Grid item sm={3}>
                        <Box sx={{ display: "flex", flexDirection: "column", alignContent: "center", justifyContent: "center", pt: 4 }}>
                            <TypographyPrint textAlign="center" variant='h6' height={100} textTransform="uppercase">NGƯỜI DUYỆT</TypographyPrint>
                            <TypographyPrint textAlign="center" variant='h6'>
                                {infoByID?.nguoiDuyet}
                            </TypographyPrint>
                        </Box>
                    </Grid>
                    <Grid item sm={5}>

                    </Grid>
                    <Grid item sm={4}>
                        <Box sx={{ display: "flex", flexDirection: "column", alignContent: "center", justifyContent: "center", pt: 4 }}>
                            <TypographyPrint textAlign="center" variant='h6' height={100} textTransform="uppercase">NGƯỜI THỰC HIỆN</TypographyPrint>
                            <TypographyPrint textAlign="center" variant='h6'>
                                {infoByID?.createBy}
                            </TypographyPrint>
                        </Box>
                    </Grid>
                </Grid>
            </Grid>

        </>
    );

    const handleConfirm = async (thangID: number, isApprove: boolean, lyDoTuChoi: string | null) => {
        let updateData = { ...infoByID };
        updateData.lyDoTuChoi = lyDoTuChoi;
        const rs = await confirmPlanQuarter(thangID, isApprove, updateData)
        if (rs) {
            toast.success("Thành công")
            setReasonForRejectionDialog(false)
        }
        else toast.error("Thất bại")
    };
    const handleReasonForRejectionDialog = async () => {
        setReasonForRejectionDialog(true)
    };
    const handleReasonForRejection = (lyDoTuChoi: string) => {
        handleConfirm(infoByID.quyID, false, lyDoTuChoi)
    }
    const handleApprovePlan = (
    ) => {
        setOpenConfirmDialog(true);
    };
    const handleConfirmApprovePlan = (
    ) => {
        setOpenConfirmDialog(false);
        handleConfirm(infoByID.quyID, true, null)
    };

    const handleViewItem = (e: any, id: any) => {
        setViewID(id)
        setOpenDialog(true)
        setOpen(0)
    }

    const handleClickShow = (e: any, newPlacement: PopperPlacementType, id: number) => {
        setOpen(id);
        setAnchorEl(e.currentTarget);
        setPlacement(newPlacement);
    };

    const isDisabled = infoByID?.nguoiDuyetID !== userID || infoByID?.isApprove

    const handleEditItem = (id: any) => {
        if (handleEditId) {
            handleEditId(id)
        }
    }
    console.log('nice', visibleRows);


    return (
        <Box
            display='flex'
            width='100%'
            bgcolor={theme.palette.background.paper}
        >
            <Box sx={{ overflow: "auto", width: '100%' }}>
                <Box sx={{ borderRadius: '6px', width: '100%', display: "table", tableLayout: "fixed", backgroundColor: theme.palette.background.paper }}>
                    {selected.length > 0 &&
                        <EnhancedTableToolbar
                            title={title}
                            numSelected={selected.length}
                            handleSelected={handleSelected}
                            handleDelete={() => handleDelete(selected)}
                            selected={selected}
                        />
                    }
                    <TableContainer>
                        <Table
                            aria-labelledby="tableTitle"
                            sx={{ minWidth: 750, border: 0 }}
                            size='medium'
                        >
                            <EnhancedTableHead headCells={head} />
                            <TableBody>
                                {visibleRows.map((row: PlanQuarter, index) => {
                                    return (
                                        <TableRow
                                            hover
                                            role="checkbox"
                                            tabIndex={-1}
                                            key={Number(row.quyID)}
                                            sx={{ cursor: 'pointer' }}
                                        >
                                            <StyledTableCell align="left">
                                                {row.khcT_DanhGias?.some(item => item.trangThai === false || item.trangThaiQuanLy === false) &&
                                                    row.khcT_DanhGias?.filter(item => item.trangThai === false || item.trangThaiQuanLy === false)
                                                        .some(item => !item.nguyenNhanThatBai) ? (
                                                    <span
                                                        className="required_text"
                                                        style={{
                                                            fontWeight: 'bold',
                                                            color: '#fff',
                                                            backgroundColor: '#f44336', // Màu đỏ
                                                            padding: '4px 8px',
                                                            borderRadius: '50%',
                                                            display: 'inline-block'
                                                        }}
                                                    >
                                                        !  {('Quý ' + row?.quy) || 'Chưa có dữ liệu'}
                                                    </span>
                                                ) : (
                                                    ('Quý ' + row?.quy) || 'Chưa có dữ liệu'
                                                )}
                                            </StyledTableCell>
                                            <StyledTableCell align="left">{row.nam ? 'Năm ' + row.nam : 'Chưa có dữ liệu'}</StyledTableCell>
                                            <StyledTableCell align="left">{row.tieuDe ? row.tieuDe : "Chưa có dữ liệu"}</StyledTableCell>
                                            <StyledTableCell align="left">{row.createDate ? row.createDate : "Chưa có dữ liệu"}</StyledTableCell>
                                            <StyledTableCell align="left">{row.createBy ? row.createBy : "Chưa có dữ liệu"}</StyledTableCell>
                                            <StyledTableCell align="left">
                                                <Chip
                                                    label={(() => {
                                                        if (
                                                            row?.isApprove === true &&
                                                            (row.khcT_DanhGias.some(item => item.trangThai === null) ||
                                                                row.khcT_CongViecPhatSinhs.some(item => item.trangThai === null))
                                                        ) {
                                                            return "Đã duyệt";
                                                        }
                                                        if (
                                                            row?.isApprove === true && !row.khcT_DanhGias.some(item => item.trangThai === null) &&
                                                            !row.khcT_CongViecPhatSinhs.some(item => item.trangThai === null) &&
                                                            (row.khcT_DanhGias.some(item => item.trangThaiQuanLy === null) ||
                                                                row.khcT_CongViecPhatSinhs.some(item => item.trangThaiQuanLy === null))
                                                        ) {
                                                            return "NS đã ĐG";
                                                        }
                                                        if (
                                                            row?.isApprove === true && !row.khcT_DanhGias.some(item => item.trangThai === null) &&
                                                            !row.khcT_CongViecPhatSinhs.some(item => item.trangThai === null) &&
                                                            !row.khcT_DanhGias.some(item => item.trangThaiQuanLy === null) &&
                                                            !row.khcT_CongViecPhatSinhs.some(item => item.trangThaiQuanLy === null)
                                                        ) {
                                                            return "QL đã ĐG";
                                                        }
                                                        if (
                                                            row?.isApprove === false
                                                        ) {
                                                            return "Từ chối";
                                                        }
                                                        return "Chờ duyệt";
                                                    })()} // <== GỌI HÀM NGAY LẬP TỨC
                                                    sx={{
                                                        backgroundColor:
                                                            row?.isApprove === true
                                                                ? "green"
                                                                : row?.isApprove === false
                                                                    ? "red"
                                                                    : theme.palette.primary.main,
                                                        color: theme.palette.primary.contrastText,
                                                    }}
                                                />
                                            </StyledTableCell>
                                            <StyledTableCell align="left">{row.nguoiDuyet ? row.nguoiDuyet : "Chưa có dữ liệu"}</StyledTableCell>
                                            <StyledTableCell align="center">
                                                <Box display='flex' gap={2} alignItems='center' justifyContent='center'>
                                                    {/* <IconEye
                                                        size='large'
                                                        onClick={(e) => handleViewItem(e, row.thangID)} >
                                                    </IconEye> */}
                                                    <Avatar
                                                        variant='rounded'
                                                        sx={{
                                                            backgroundColor: theme.palette.primary.main
                                                        }}
                                                    >
                                                        <IconEye onClick={(e) => handleViewItem(e, row.quyID)} stroke={1.5} />

                                                    </Avatar>
                                                    {isButtonEdit &&
                                                        (
                                                            row.isApprove ? (
                                                                <Avatar variant="rounded" sx={{ bgcolor: "green" }}>
                                                                    <IconClipboardCheck onClick={() => handleEditItem(row.quyID)} stroke={1.5} />
                                                                </Avatar>
                                                            ) : (
                                                                <Avatar variant="rounded" sx={{ bgcolor: "green" }}>
                                                                    <IconPencil onClick={() => handleEditItem(row.quyID)} stroke={1.5} />
                                                                </Avatar>
                                                            )
                                                        )
                                                    }
                                                </Box>
                                            </StyledTableCell>
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                            <CustomDialog
                                size='xl'
                                title={'Chi tiết'}
                                open={openDialog}
                                handleOpen={setOpenDialog}
                                content={
                                    <Grid container spacing={2}>
                                        <Grid item sm={12}>
                                            <Typography><span style={{ fontWeight: "bolder" }}>Tiêu đề:</span> {infoByID?.tieuDe}</Typography>
                                        </Grid>
                                        <Grid item sm={6}>
                                            <Typography><span style={{ fontWeight: "bolder" }}>Quý:</span> {infoByID?.quy}</Typography>
                                        </Grid>
                                        <Grid item sm={6}>
                                            <Typography><span style={{ fontWeight: "bolder" }}>Năm:</span> {infoByID?.nam}</Typography>
                                        </Grid>

                                        <Grid item sm={6}>
                                            <Typography><span style={{ fontWeight: "bolder" }}>Từ ngày:</span> {infoByID?.tuNgay?.slice(0, 10)}</Typography>
                                        </Grid>
                                        <Grid item sm={6}>
                                            <Typography><span style={{ fontWeight: "bolder" }}>Đến ngày:</span> {infoByID?.denNgay?.slice(0, 10)}</Typography>
                                        </Grid>
                                        <Grid item sm={6}>
                                            <Typography><span style={{ fontWeight: "bolder" }}>Tạo bởi:</span> {infoByID?.createBy}</Typography>
                                        </Grid>
                                        <Grid item sm={6}>
                                            <Typography><span style={{ fontWeight: "bolder" }}>Ngày tạo:</span> {infoByID?.createDate?.slice(0, 10)}</Typography>
                                        </Grid>
                                        <Grid item sm={12}>
                                            <Typography>
                                                <span style={{ fontWeight: "bolder" }}>Trạng thái duyệt:</span>{" "}
                                                {infoByID?.isApprove === true
                                                    ? "Đã duyệt"
                                                    : infoByID?.isApprove === false
                                                        ? "Không duyệt"
                                                        : "Chờ duyệt"}
                                            </Typography>
                                        </Grid>
                                        <Grid item sm={12}>
                                            <Typography><span style={{ fontWeight: "bolder" }}>Người duyệt:</span> {infoByID?.nguoiDuyet}</Typography>
                                        </Grid>
                                        <Grid item sm={12}>
                                            <Typography><span style={{ fontWeight: "bolder" }}>Nội dung:</span></Typography>
                                            <Box bgcolor={theme.palette.grey[100]} mt={1} borderRadius={"8px"} width={"100%"} p={2} border={1} borderColor={theme.palette.grey[400]}><div dangerouslySetInnerHTML={{ __html: infoByID?.noiDung! }}></div></Box>
                                        </Grid>
                                        {
                                            infoByID?.lyDoTuChoi &&
                                            <Grid item sm={12}>
                                                <Typography><span style={{ fontWeight: "bolder" }}>Lý do từ chối:</span> {infoByID.lyDoTuChoi}</Typography>
                                            </Grid>
                                        }
                                        {
                                            infoByID?.khcT_DanhGias?.length > 0 &&
                                            <>
                                                <Typography variant="h5" sx={{ mt: 2 }}>Danh sách công việc
                                                </Typography>
                                                <WorkAssessmentTable defaulValue={infoByID} />
                                            </>
                                        }
                                        {
                                            infoByID?.khcT_CongViecPhatSinhs?.length > 0 &&
                                            <>
                                                <Typography variant="h5" sx={{ mt: 2 }}>Danh sách công việc phát sinh
                                                </Typography>
                                                <WorkArisesTable defaulValue={infoByID} />
                                            </>
                                        }
                                        <Grid item xs={12}>
                                            {isDisabled === null &&
                                                <Box sx={{
                                                    display: "flex",
                                                    gap: 1,
                                                    justifyContent: "flex-end"
                                                }}>
                                                    <Box>
                                                        <Button
                                                            variant='contained'
                                                            sx={{
                                                                backgroundColor: theme.palette.primary.main,
                                                                boxShadow: "none",
                                                            }}
                                                            onClick={() => handleApprovePlan()}
                                                        >
                                                            Duyệt
                                                        </Button>
                                                    </Box>
                                                    <Box>
                                                        <Button
                                                            variant='contained'
                                                            onClick={() => handleReasonForRejectionDialog()}
                                                            sx={{
                                                                backgroundColor: "red",
                                                                boxShadow: "none",
                                                                '&:hover': {
                                                                    backgroundColor: "red"
                                                                },
                                                            }}>
                                                            Từ chối
                                                        </Button>
                                                    </Box>
                                                </Box>
                                            }

                                            <Box sx={{
                                                display: "flex",
                                                gap: 1,
                                                justifyContent: "flex-end"
                                            }}>
                                                <ReactToPrint
                                                    trigger={() => <StyledButton variant="contained" startIcon={<IconPrinter stroke={1.5} />} size="large">In kế hoạch</StyledButton>}
                                                    content={() => componentRef.current
                                                    }
                                                    pageStyle={`
                                                            @page {
                                                                margin: 5mm;
                                                            }
                                                            `}
                                                />
                                            </Box>
                                        </Grid>
                                    </Grid>
                                }

                            />
                            <Box sx={{
                                position: 'absolute',
                                top: 0,
                                left: 0,
                                width: 0,
                                height: 0,
                                overflow: 'hidden',
                                visibility: 'hidden'
                            }} >
                                {
                                    staff && infoByID &&
                                    <PrintPlan staff={staff} ref={componentRef} title={'Kế hoạch quý ' + infoByID?.quy + '/' + infoByID?.nam} content={content} />
                                }
                            </Box>
                            {
                                openReasonForRejectionDialog &&
                                <ReasonForRejectionDialog isInsert={true} open={openReasonForRejectionDialog} handleOpen={setReasonForRejectionDialog} title="Nhập lý do từ chối" handleSubmit={handleReasonForRejection} />
                            }
                            {openConfirmDialog && (
                                <AlertConfirmDialog
                                    title="Xác nhận duyệt kế hoạch?"
                                    message="Vui lòng nhấn xác nhận"
                                    onHandleConfirm={handleConfirmApprovePlan}
                                    openConfirm={openConfirmDialog}
                                    handleOpenConfirmDialog={setOpenConfirmDialog}
                                />
                            )}
                        </Table>
                    </TableContainer>
                    <TableCustomizePagination
                        handlePage={setPage}
                        handleRowsPerPage={setRowsPerPage}
                        page={page}
                        rowsPerPage={rowsPerPage}
                        rows={rows}
                    />
                </Box>
            </Box>
        </Box >
    );
}
