import UploadFileDialog from "@/components/dialog/UploadFileDialog";
import { AdminLayout } from "@/components/layout";
import SearchNoButtonSection from "@/components/search/SearchNoButton";
import { StyledButton } from "@/components/styled-button";
import useImportFile from "@/hooks/useImportFile";
import {
    Box,
    Button,
    CircularProgress,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    Typography,
    useTheme,
} from "@mui/material";
import React, { ChangeEvent, useEffect, useMemo, useState } from "react";
import useRole from "@/hooks/useRole";
import {
    BAN_THI_TRUONG_NHAN_VIEN_KINH_DOANH,
    QUAN_TRI,
    BAN_THI_TRUONG_GIAM_DOC_KINH_DOANH,
} from "@/constant/role";
import useProducts from "@/hooks/useProducts";
import TableProducts from "@/components/table/table-products/TableProducts";
import ProductsDialog from "@/components/dialog/ProductsDialog";
import useProductTypes from "@/hooks/useProductTypes";
import useSubjects from "@/hooks/useSubjects";
import useGrades from "@/hooks/useGrades";
import useCirculars from "@/hooks/useCirculars";
import SearchSectionTextField from "@/components/search/SearchSectionTextField";
import { CustomInput } from "@/components/input";
import { toast } from "react-toastify";
import axios, { AxiosResponse } from "axios";
import { getAllBidReport, getAllBidReportLevel, getAllBidReportType, getAllFieldType, getHuyenByTinhID, getTinh, importBaoCaoThau, importKetQuaThau } from "@/constant/api";
import { ExportExcelProduct, FindProductInExcel, ProductInEstimate, Products } from "@/interfaces/products";
import { Grades } from "@/interfaces/grades";
import { Subjects } from "@/interfaces/subjects";
import XLSX from "sheetjs-style";
import ExportExcelProductDialog from "@/components/dialog/ExportExcelProductDialog";
const MYdata = [
    { title: "21", website: "Foo" },
    { title: "21", website: "Bar" },
];
import * as htmlToText from "html-to-text";
import * as ExcelJS from 'exceljs';
import useCompanyEstimate from "@/hooks/useCompanyEstimate";
import QuoteUploadFileDialog from "@/components/dialog/QuoteUploadFileDialog";
import FindProductByNameDialog from "@/components/dialog/FindProductByNameDialog";
import useRoleLocalStorage from "@/hooks/useRoleLocalStorage";
import TableBidReports from "@/components/table/table-bid-report/TableBidReport";
import { BidReport } from "@/interfaces/bidReport";
import { stringToSlug } from "@/utils/stringToSlug";
import BidReportDialog from "@/components/dialog/BidReportDialog";
import { FieldType } from "@/interfaces/fieldType";
import { Province } from "@/interfaces/province";
import { District } from "@/interfaces/district";
import { ImportResult } from "@/interfaces/importError";
import { BidReportLevel, ChuDauTu } from "@/interfaces/bidReportLevel";
import { BidReportType } from "@/interfaces/bidReportType";
import DatePicker from "react-datepicker";
import { vi } from "date-fns/locale";
import { IconCalendar, IconX } from "@tabler/icons-react";
interface CustomInputProps {
    value: string;
    onClick: () => void;
}

export function CustomInputDatePicker({ value, onClick }: CustomInputProps) {
    return (
        <div className="input-group position-relative">
            <input
                type="text"
                className="form-control custom-input"
                value={value}
                onClick={onClick}
                placeholder="Lọc theo tháng/năm"
                readOnly
                style={{ width: '100%', height: '20px !important' }} // Giảm chiều rộng để có chỗ cho biểu tượng "X"
            />
            <span className="icon-background" onClick={onClick}>
                <IconCalendar color="white" />
            </span>

        </div>
    );
}
interface FilterCapBaoCaoThau {
    capBaoCaoThauID: number;
    chuDauTu: string; // String cho chuDauTu
}
const BidReportPage = () => {
    const theme = useTheme();
    const { getAllProducts, addProducts, dataProducts, isLoadding } =
        useProducts();
    const [openAdd, setOpenAdd] = useState(false);
    const [openUploadBaoCaoThau, setOpenUploadBaoCaoThau] = useState(false);
    const [openUploadKetQuaThau, setOpenUploadKetQuaThau] = useState(false);
    const [contentSearch, setContentSearch] = useState<string>("");
    const [filterCapBaoCaoThaus, setFilterCapBaoCaoThaus] = useState<FilterCapBaoCaoThau[]>([]);
    const { getAllRoleOfUser, dataRoleByUser } = useRole();
    const {
        isAdmin,
        isGeneralDirector,
        isDeputyGeneralDirector,
        isLoadingRole
    } = useRoleLocalStorage();

    const [dataBidReports, setDataBidReports] = useState<BidReport[]>([]);
    const [dataFieldTypes, setDataFieldTypes] = useState<FieldType[]>([]);
    const [dataBidReportLevels, setDataBidReportLevel] = useState<BidReportLevel[]>([]);
    const [dataBidReportTypes, setDataBidReportTypes] = useState<BidReportType[]>([]);
    const [filterTinhID, setFilterTinhID] = useState<number>(0);
    const [filterHuyenID, setFilterHuyenID] = useState<number>(0);
    const [filterLoaiLinhVucID, setFilterLoaiLinhVucID] = useState<number>(0);
    const [filterLoaiBaoCaoThauID, setFilterLoaiBaoCaoThauID] = useState<number>(0);
    const [dataProvinces, setDataProvinces] = useState<Province[]>([]);
    const [dataDistricts, setDataDistricts] = useState<District[]>([]);
    const [importResults, setImportResults] = useState<ImportResult>();
    const [thoiGianDongMo, setThoiGianDongMo] = useState<Date | null>(null);
    const [filterStatus, setFilterStatus] = useState<string>("0");
    const handleChangeFilter = (e: any, setter: Function) => {
        setter(e.target.value);
    };
    const handleChangeFilterBidReportLevel = (e: any, level: BidReportLevel) => {
        const { name, value } = e.target;

        // Kiểm tra xem filter có tồn tại cho level này không
        const existingFilterIndex = filterCapBaoCaoThaus.findIndex(filter => filter.capBaoCaoThauID === level.capBaoCaoThauID);

        if (existingFilterIndex > -1) {
            // Nếu đã tồn tại, cập nhật giá trị chuDauTu
            const updatedFilterCapBaoCaoThaus = [...filterCapBaoCaoThaus];
            updatedFilterCapBaoCaoThaus[existingFilterIndex] = {
                ...updatedFilterCapBaoCaoThaus[existingFilterIndex],
                chuDauTu: value // Cập nhật chuDauTu
            };
            setFilterCapBaoCaoThaus(updatedFilterCapBaoCaoThaus);
        } else {
            // Nếu chưa tồn tại, thêm mới
            setFilterCapBaoCaoThaus(prev => [...prev, { capBaoCaoThauID: level.capBaoCaoThauID ?? 0, chuDauTu: value }]);
        }
    };
    const handleSaveFileImportBaoCaoThau = async (file: File | null) => {
        if (file) {
            const accessToken = window.localStorage.getItem("accessToken");
            if (!accessToken) {
                throw new Error("No access token found");
            }

            const formData = new FormData();
            formData.append("formFile", file); // Thay "file" bằng tên trường mong muốn của bạn

            const headers = {
                Authorization: `Bearer ${accessToken}`,
                "Content-Type": "multipart/form-data",
            };

            try {
                setImportResults(undefined);
                const response = await axios.post(importBaoCaoThau, formData, { headers });
                setImportResults(response.data);
                fetchData();
            } catch (error) {
                console.error("Error uploading file:", error);
            }
        }
        setOpenUploadBaoCaoThau(false);
    }
    const handleSaveFileImportKetQuaThau = async (file: File | null) => {
        if (file) {
            const accessToken = window.localStorage.getItem("accessToken");
            if (!accessToken) {
                throw new Error("No access token found");
            }

            const formData = new FormData();
            formData.append("formFile", file); // Thay "file" bằng tên trường mong muốn của bạn

            const headers = {
                Authorization: `Bearer ${accessToken}`,
                "Content-Type": "multipart/form-data",
            };

            try {
                setImportResults(undefined);
                const response = await axios.post(importKetQuaThau, formData, { headers });
                fetchData();
                console.log("File uploaded successfully:", response.data);
                setImportResults(response.data);
            } catch (error) {
                console.error("Error uploading file:", error);
            }
        }
        setOpenUploadKetQuaThau(false);
    }
    const handleDownLoadFileImportBaoCaoThau = async () => {
        const filePath = '/data/BaoCaoThau.xlsx';
        const a = document.createElement('a');
        a.href = filePath;
        a.download = 'BaoCaoThau.xlsx'; // Tên của tệp tin khi được tải xuống
        document.body.appendChild(a); // Thêm liên kết vào body
        a.click(); // Kích hoạt sự kiện click trên liên kết ẩn
        document.body.removeChild(a); // Sau khi click, xóa liên kết ra khỏi body

    }
    const handleDownLoadFileImportKetQuaThau = async () => {
        const filePath = '/data/KetQuaThau.xlsx';
        const a = document.createElement('a');
        a.href = filePath;
        a.download = 'KetQuaThau.xlsx'; // Tên của tệp tin khi được tải xuống
        document.body.appendChild(a); // Thêm liên kết vào body
        a.click(); // Kích hoạt sự kiện click trên liên kết ẩn
        document.body.removeChild(a); // Sau khi click, xóa liên kết ra khỏi body

    }
    const fetchData = async () => {
        try {
            const accessToken = window.localStorage.getItem("accessToken");
            if (!accessToken) {
                throw new Error("No access token found");
            }
            const headers = { Authorization: `Bearer ${accessToken}` };
            const response = await axios.get(getAllBidReport, { headers });
            setDataBidReports(response.data)

        } catch (error) {
            console.log(error);
        } finally {
        }
    };
    const fetchFieldTypes = async () => {
        try {
            const accessToken = window.localStorage.getItem("accessToken");
            if (!accessToken) {
                throw new Error("No access token found");
            }
            const headers = { Authorization: `Bearer ${accessToken}` };
            const response = await axios.get(getAllFieldType, { headers });
            setDataFieldTypes(response.data)

        } catch (error) {
            console.log(error);
        } finally {
        }
    };
    const fetchBidReportTypes = async () => {
        try {
            const accessToken = window.localStorage.getItem("accessToken");
            if (!accessToken) {
                throw new Error("No access token found");
            }
            const headers = { Authorization: `Bearer ${accessToken}` };
            const response = await axios.get(getAllBidReportType, { headers });
            setDataBidReportTypes(response.data)

        } catch (error) {
            console.log(error);
        } finally {
        }
    };
    const fetchBidReportLevels = async () => {
        try {
            const accessToken = window.localStorage.getItem("accessToken");
            if (!accessToken) {
                throw new Error("No access token found");
            }
            const headers = { Authorization: `Bearer ${accessToken}` };
            const response = await axios.get(getAllBidReportLevel, { headers });
            response.data.forEach((level: BidReportLevel) => {
                // Lọc các item trong dataBidReports dựa trên capBaoCaoThauID
                const relatedItems = dataBidReports.filter(item => item.capBaoCaoThauID === level.capBaoCaoThauID);
                // Gán NhaDauTu cho level, chỉ lấy baoCaoThauID và chuDauTu
                // Sử dụng Set để lọc các item trùng
                const seen = new Set();
                level.chuDauTu = relatedItems
                    .filter(item => {
                        if (seen.has(item.chuDauTu)) {
                            return false; // Bỏ qua nếu đã thấy baoCaoThauID này
                        }
                        seen.add(item.chuDauTu); // Thêm baoCaoThauID vào Set
                        return true; // Giữ lại item này
                    })
                    .map(item => ({
                        baoCaoThauID: item.baoCaoThauID,
                        chuDauTu: item.chuDauTu,
                        tinhID: item.tinhID,
                        huyenID: item.huyenID,
                        // Các thuộc tính khác cần thiết cho kiểu NhaDauTu
                    })) as ChuDauTu[]; // Ép kiểu nếu cần thiết
            });
            setDataBidReportLevel(response.data);
        } catch (error) {
            console.log(error);
        } finally {
        }
    };

    useEffect(() => {
        fetchData();
    }, []);
    useEffect(() => {
        fetchFieldTypes();
        fetchBidReportTypes();
    }, []);

    useEffect(() => {
        if (dataBidReports) {
            fetchBidReportLevels();
        }
    }, [dataBidReports]);
    useEffect(() => {
        const account = JSON.parse(localStorage.getItem('account')!)
        getAllRoleOfUser(account?.userID)
    }, [])
    const viewRole = isAdmin
        || isGeneralDirector
        || isDeputyGeneralDirector

    const filterDataBidReports = useMemo(() => {
        let filteredReports = dataBidReports;
        if (contentSearch) {
            // First, filter by tbmt
            const tbmtFilteredReports = filteredReports.filter((item) =>
                stringToSlug(item.tbmt).includes(stringToSlug(contentSearch))
            );

            // If matches found in tbmt, keep those; otherwise, filter by tenGoiThau or chuDauTu if tenGoiThau is empty
            if (tbmtFilteredReports.length > 0) {
                filteredReports = tbmtFilteredReports;
            } else {
                // Filter by tenGoiThau first
                const tenGoiThauFilteredReports = filteredReports.filter((item) =>
                    stringToSlug(item.tenGoiThau).includes(stringToSlug(contentSearch))
                );

                // If no matches found in tenGoiThau, filter by chuDauTu
                if (tenGoiThauFilteredReports.length > 0) {
                    filteredReports = tenGoiThauFilteredReports;
                } else {
                    filteredReports = filteredReports.filter((item) =>
                        stringToSlug(item.chuDauTu).includes(stringToSlug(contentSearch))
                    );
                }
            }
        }

        if (filterTinhID > 0) {
            filteredReports = filteredReports.filter((item) => item?.tinhID === filterTinhID);
        }
        if (filterLoaiBaoCaoThauID > 0) {
            filteredReports = filteredReports.filter((item) => item?.loaiBaoCaoThauID === filterLoaiBaoCaoThauID);
        }
        if (filterHuyenID > 0) {

            filteredReports = filteredReports.filter((item) => item.huyenID === filterHuyenID);
        }

        if (filterLoaiLinhVucID > 0) {
            filteredReports = filteredReports.filter((item) =>
                item.loaiLinhVucs?.some(linhVuc => linhVuc.loaiLinhVucID === filterLoaiLinhVucID)
            );
        }
        // New status filtering
        if (filterStatus === 'closed') {
            filteredReports = filteredReports.filter(item => {
                const currentTime = new Date();
                // Create a new Date object directly
                const [day, month, year, hours, minutes, seconds] = item.thoiGianDongMo.split(/[/ :]/).map(Number);
                const newDate = new Date(year, month - 1, day, hours, minutes, seconds);
                return newDate <= currentTime && item.nhaThauID === null;
            });
        } else if (filterStatus === 'open') {
            filteredReports = filteredReports.filter(item => {
                const currentTime = new Date();
                const [day, month, year, hours, minutes, seconds] = item.thoiGianDongMo.split(/[/ :]/).map(Number);
                const newDate = new Date(year, month - 1, day, hours, minutes, seconds);
                return newDate > currentTime;
            });
        }
        else if (filterStatus === 'result') {
            filteredReports = filteredReports.filter((item) =>
                item.nhaThauID != null
            );
        }
        const validFilters = filterCapBaoCaoThaus.filter(filter => filter.chuDauTu.toString() !== '0');
        // Thêm điều kiện lọc dựa trên filters
        if (validFilters.length > 0) {
            filteredReports = filteredReports.filter(item =>
                validFilters.some(filter => item.chuDauTu === filter.chuDauTu)
            );
        }
        // Lọc theo thoiGianDongMo
        if (thoiGianDongMo) {
            const thoiGianDongMoMonth = thoiGianDongMo.getMonth();
            const thoiGianDongMoYear = thoiGianDongMo.getFullYear();

            filteredReports = filteredReports.filter(item => {
                const [day, month, year] = item.thoiGianDongMo.split(/[/ :]/).map(Number);
                return month - 1 === thoiGianDongMoMonth && year === thoiGianDongMoYear;
            });
        }
        return filteredReports.length > 0 ? filteredReports : false;
    }, [contentSearch, dataBidReports, filterTinhID, filterHuyenID, filterLoaiLinhVucID, filterCapBaoCaoThaus, filterStatus, filterLoaiBaoCaoThauID, thoiGianDongMo]);


    const fetchProvinces = async () => {
        try {
            const accessToken = window.localStorage.getItem("accessToken");
            if (!accessToken) {
                throw new Error("No access token found");
            }
            const headers = { Authorization: `Bearer ${accessToken}` };
            const response = await axios.get(getTinh, { headers });
            setDataProvinces(response.data)

        } catch (error) {
            console.log(error);
        } finally {
        }
    };
    const fetchDistricts = async () => {
        try {

            const accessToken = window.localStorage.getItem("accessToken");
            if (!accessToken) {
                throw new Error("No access token found");
            }
            const headers = { Authorization: `Bearer ${accessToken}` };
            const response = await axios.get(`${getHuyenByTinhID}/${filterTinhID}`, { headers });
            setDataDistricts(response.data)

        } catch (error) {
            console.log(error);
        } finally {
        }
    };
    useEffect(() => {
        fetchProvinces();
    }, []);
    useEffect(() => {
        if (filterTinhID) {
            fetchDistricts()
            setFilterHuyenID(0);
        }
    }, [filterTinhID]);
    // const filteredDataReportLevels = useMemo(() => {
    //     // Nếu cả filterTinhID và filterHuyenID đều bằng 0, trả về toàn bộ dataBidReportLevels
    //     if (filterTinhID === 0 && filterHuyenID === 0) {
    //         return dataBidReportLevels;
    //     }

    //     // Nếu có dữ liệu, thực hiện lọc
    //     return dataBidReportLevels.map(item => {
    //         // Kiểm tra xem item có chuDauTu không và chuDauTu có phải là mảng
    //         if (item.chuDauTu && Array.isArray(item.chuDauTu)) {
    //             // Lọc các chuDauTu theo tinhID và huyenID
    //             const filteredChuDauTu = item.chuDauTu.filter(chuDauTu => {
    //                 const matchesTinhID = item.tenCapBaoCaoThau === 'Cơ quan Tỉnh/Thành phố'
    //                     ? filterTinhID === 0 || chuDauTu.tinhID === filterTinhID
    //                     : true; // Không cần kiểm tra nếu không phải 'Cơ quan Tỉnh/Thành phố'

    //                 const matchesHuyenID = item.tenCapBaoCaoThau === 'Cơ quan Quận/Huyện'
    //                     ? filterHuyenID === 0 || chuDauTu.huyenID === filterHuyenID
    //                     : true; // Không cần kiểm tra nếu không phải 'Cơ quan quận/huyện'


    //                 const matchesXaID = item.tenCapBaoCaoThau === 'Cơ quan Phường/Xã'
    //                     ? filterHuyenID === 0 || chuDauTu.huyenID === filterHuyenID
    //                     : true; // Không cần kiểm tra nếu không phải 'Cơ quan quận/huyện'

    //                 return matchesTinhID && matchesHuyenID && matchesXaID;
    //             });

    //             // Trả về item với chuDauTu đã được lọc
    //             return {
    //                 ...item,
    //                 chuDauTu: filteredChuDauTu,
    //             };
    //         }
    //         // Trả về item gốc nếu không có chuDauTu
    //         return item;
    //     }).filter(item => item.chuDauTu && item.chuDauTu.length > 0); // Lọc ra các item không có chuDauTu
    // }, [dataBidReportLevels, filterTinhID, filterHuyenID]);
    const filteredDataReportLevels = useMemo(() => {
        // Nếu cả filterTinhID và filterHuyenID đều bằng 0, trả về toàn bộ dataBidReportLevels
        if (filterTinhID === 0 && filterHuyenID === 0) {
            return dataBidReportLevels;
        }

        // Nếu có dữ liệu, thực hiện lọc
        return dataBidReportLevels.map(item => {
            // Kiểm tra xem item có chuDauTu không và chuDauTu có phải là mảng
            if (item.chuDauTu && Array.isArray(item.chuDauTu)) {
                // Lọc các chuDauTu theo tinhID và huyenID
                const filteredChuDauTu = item.chuDauTu.filter(chuDauTu => {
                    const matchesTinhID = item.tenCapBaoCaoThau === 'Cơ quan Tỉnh/Thành phố'
                        ? filterTinhID === 0 || chuDauTu.tinhID === filterTinhID
                        : item.tenCapBaoCaoThau === 'Cơ quan Quận/Huyện'
                            ? (filterTinhID === 0 || chuDauTu.tinhID === filterTinhID) // Lọc theo filterTinhID
                            : item.tenCapBaoCaoThau === 'Cấp Phường/Xã'
                                ? filterTinhID === 0 || chuDauTu.tinhID === filterTinhID // Lọc theo filterTinhID
                                : true; // Không cần kiểm tra nếu không phải 'Cơ quan Tỉnh/Thành phố' hoặc 'Cấp Phường/Xã'

                    const matchesHuyenID = item.tenCapBaoCaoThau === 'Cơ quan Quận/Huyện'
                        ? (filterHuyenID === 0 || chuDauTu.huyenID === filterHuyenID) // Lọc theo filterHuyenID
                        : item.tenCapBaoCaoThau === 'Cấp Phường/Xã'
                            ? filterHuyenID === 0 || chuDauTu.huyenID === filterHuyenID // Lọc theo filterHuyenID
                            : true; // Không cần kiểm tra nếu không phải 'Cơ quan Quận/Huyện' hoặc 'Cấp Phường/Xã'

                    const matchesXaID = item.tenCapBaoCaoThau === 'Cơ quan Phường/Xã'
                        ? (filterHuyenID === 0 || chuDauTu.huyenID === filterHuyenID) &&
                        (filterTinhID === 0 || chuDauTu.tinhID === filterTinhID) // Lọc theo cả filterTinhID và filterHuyenID
                        : true; // Không cần kiểm tra nếu không phải 'Cơ quan Phường/Xã'

                    return matchesTinhID && matchesHuyenID && matchesXaID;
                });

                // Trả về item với chuDauTu đã được lọc, giữ nguyên item
                return {
                    ...item,
                    chuDauTu: filteredChuDauTu, // Cập nhật chỉ chuDauTu
                };
            }
            // Trả về item gốc nếu không có chuDauTu
            return item;
        });
    }, [dataBidReportLevels, filterTinhID, filterHuyenID]);
    console.log('dsds', filteredDataReportLevels);

    const handleClearThoiGianDongMo = () => {
        setThoiGianDongMo(null); // Đặt lại giá trị endDate về null
    };
    return (
        <AdminLayout>
            <></>
            {isLoadingRole ? (
                <Box
                    display="flex"
                    justifyContent="center"
                    alignItems="flex-start"
                    width="100%"
                    my={6}
                    gap={3}
                >
                    Đang tải ......
                </Box>
            ) : viewRole ? (
                <Box padding="24px">
                    <Box
                        display="flex"
                        alignItems="center"
                        justifyContent="space-between"
                    >
                        <Typography variant="h3" color={theme.palette.primary.main} pb={2}>
                            Quản lý báo cáo thầu
                        </Typography>
                    </Box>
                    <Box
                        display="flex"
                        flexDirection="column"
                        alignItems="flex-start"
                        width="100%"
                        bgcolor={theme.palette.background.paper}
                        px={3}
                        py={3}
                        sx={{
                            overflow: 'auto', // Enable scrolling for overflowed content
                            maxHeight: 'calc(100vh - 200px)', // Set maximum height to prevent infinite scrolling
                        }}
                    >
                        <Box
                            sx={{
                                display: "flex",
                                justifyContent: "space-between",
                                alignItems: "left",
                                gap: { xl: 1, xs: 2 },
                                width: "100%",
                                flexDirection: { xl: "row", xs: "column", lg: "column" },
                            }}
                        >
                            <Box sx={{ width: { xs: "100%", sm: "50%" } }}>
                                <SearchSectionTextField
                                    placeholderText="Nhập TBMT, tên gói thầu, chủ đầu tư"
                                    handleContentSearch={setContentSearch}
                                    contentSearch={contentSearch}
                                />
                            </Box>
                            <Box
                                sx={{
                                    display: "flex",
                                    alignItems: "center",
                                    gap: 1,
                                    width: "100%",
                                    flexDirection: "row",
                                    flexWrap: 'wrap',
                                    justifyContent: {
                                        xs: "flex-start", // Extra-small screens
                                        sm: "flex-start", // Small screens
                                        md: "flex-start",   // Medium screens
                                        lg: "flex-start",   // Large screens
                                        xl: "flex-end",   // Extra-large screens
                                    },

                                }}
                            >
                                <Button
                                    onClick={() => setOpenUploadBaoCaoThau(true)}
                                    variant="contained"
                                    size="large"
                                    disabled={!viewRole}
                                    sx={{
                                        // width: { xs: '24%', md: '20%', lg: "20%", xl: "20%" }, // Take up 100% width on xs and 29% width on md (max-width: 1200px)
                                        margin: '0.5rem',
                                        textTransform: 'none'
                                        // Adjust margin as needed
                                    }}
                                >
                                    Nhập Excel Báo cáo thầu
                                </Button>

                                <Button
                                    onClick={() => setOpenUploadKetQuaThau(true)}
                                    variant="contained"
                                    size="large"
                                    disabled={!viewRole}
                                    sx={{
                                        // width: { xs: '24%', md: '20%', lg: "20%", xl: "20%" }, // Take up 100% width on xs and 29% width on md (max-width: 1200px)
                                        margin: '0.5rem',
                                        textTransform: 'none'
                                        // Adjust margin as needed
                                    }}
                                >
                                    Nhập Excel kết quả thầu
                                </Button>
                                <Button
                                    onClick={() => setOpenAdd(true)}
                                    variant="contained"
                                    size="large"
                                    disabled={!viewRole}
                                    sx={{
                                        // width: { xs: '24%', md: '20%', lg: "20%", xl: "20%" }, // Take up 100% width on xs and 29% width on md (max-width: 1200px)
                                        margin: '0.5rem',
                                        textTransform: 'none'
                                        // Adjust margin as needed
                                    }}
                                >
                                    Thêm báo cáo thầu
                                </Button>
                            </Box>

                            {
                                openAdd === true ? (<>
                                    <BidReportDialog
                                        title="Thêm báo cáo thầu"
                                        fetchData={fetchData}
                                        dataFieldTypes={dataFieldTypes}
                                        dataBidReportLevels={dataBidReportLevels}
                                        dataBidReportTypes={dataBidReportTypes}
                                        defaulValue={null}
                                        isInsert
                                        handleOpen={setOpenAdd}
                                        open={openAdd}
                                    />

                                </>) : (<></>)
                            }

                        </Box>

                    </Box>
                    <Box px={3} pb={3}
                        sx={{
                            maxHeight: 'calc(100vh - 200px)', // Set maximum height to prevent infinite scrolling
                        }} bgcolor={theme.palette.background.paper}>
                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <FormControl variant="outlined" sx={{ width: '100%' }} >
                                    <InputLabel
                                        id="demo-simple-select-label-province"
                                        sx={{ color: theme.palette.text.primary }}
                                    >
                                        Tỉnh/Thành phố
                                    </InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label-province"
                                        label="Tỉnh/Thành phố"
                                        id="filterTinhID"
                                        name="filterTinhID"
                                        type="filterTinhID"
                                        value={filterTinhID}
                                        onChange={(e) => handleChangeFilter(e, setFilterTinhID)}
                                        input={<CustomInput size="small" label="Tỉnh/Thành phố" />}
                                    >
                                        <MenuItem value={0}>Tất cả</MenuItem>
                                        {dataProvinces.map((item: Province, index) => (
                                            <MenuItem key={index} value={item.tinhID}>
                                                {item.tenTinh}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={4}>
                                <FormControl variant="outlined" sx={{ width: '100%' }} >
                                    <InputLabel
                                        id="demo-simple-select-label-province"
                                        sx={{ color: theme.palette.text.primary }}
                                    >
                                        Quận/Huyện
                                    </InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label-province"
                                        label="Quận/Huyện"
                                        id="filterHuyenID"
                                        name="filterHuyenID"
                                        type="filterHuyenID"
                                        value={filterHuyenID}
                                        onChange={(e) => handleChangeFilter(e, setFilterHuyenID)}
                                        input={<CustomInput size="small" label="Quận/Huyện" />}
                                    >
                                        <MenuItem value={0}>Tất cả</MenuItem>
                                        {dataDistricts.map((item: District, index) => (
                                            <MenuItem key={index} value={item.huyenID}>
                                                {item.tenHuyen}
                                            </MenuItem>
                                        ))}
                                        {
                                            dataDistricts?.length <= 0 &&
                                            <MenuItem
                                                key="filterTInhID"
                                                disabled
                                            >
                                                Vui lòng chọn thành phố trước
                                            </MenuItem>
                                        }
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={4}>
                                <FormControl variant="outlined" sx={{ width: '100%' }} >
                                    <InputLabel
                                        id="demo-simple-select-label-province"
                                        sx={{ color: theme.palette.text.primary }}
                                    >
                                        Nhóm hàng hóa/dịch vụ
                                    </InputLabel>
                                    <Select
                                        labelId="demo-simple-select-label-province"
                                        label="Nhóm hàng hóa/lĩnh vực"
                                        id="filterLoaiLinhVucID"
                                        name="filterLoaiLinhVucID"
                                        type="filterLoaiLinhVucID"
                                        value={filterLoaiLinhVucID}
                                        onChange={(e) => handleChangeFilter(e, setFilterLoaiLinhVucID)}
                                        input={<CustomInput size="small" label="Nhóm hàng hóa/lĩnh vực" />}
                                    >
                                        <MenuItem value={0}>Tất cả</MenuItem>
                                        {dataFieldTypes.map((item: FieldType, index) => (
                                            <MenuItem key={index} value={item.loaiLinhVucID}>
                                                {item.tenLoaiLinhVuc}
                                            </MenuItem>
                                        ))}

                                    </Select>
                                </FormControl>
                            </Grid>
                            {
                                importResults && <>
                                    <Box sx={{ p: 3 }}>
                                        <Typography>Số dòng lỗi: {importResults?.error}</Typography>
                                        <Typography>Số dòng thành công: {importResults?.success}</Typography>
                                        {importResults?.importError?.length > 0 && (
                                            <div>
                                                <Typography variant="h6">Danh sách lỗi:</Typography>
                                                {importResults.importError.map((error, index) => (
                                                    <Typography key={index} color="error">
                                                        STT {error.stt}: {error.maLoi}
                                                    </Typography>
                                                ))}
                                            </div>
                                        )}
                                    </Box>

                                </>
                            }
                        </Grid>
                        <Grid container spacing={2} sx={{ mt: 1 }}>
                            {filteredDataReportLevels?.map((level: BidReportLevel, index) => (
                                <Grid item xs={4} key={index}>
                                    <FormControl variant="outlined" sx={{ width: '100%' }}>
                                        <InputLabel
                                            id={`demo-simple-select-label-province-${index}`}
                                            sx={{ color: theme.palette.text.primary }}
                                        >
                                            {level.tenCapBaoCaoThau} {/* Hoặc tên gì đó liên quan đến level */}
                                        </InputLabel>
                                        <Select
                                            labelId={`demo-simple-select-label-province-${index}`}
                                            label={level.tenCapBaoCaoThau}
                                            id={`capBaoCaoThauID-${index}`}
                                            name={`capBaoCaoThauID-${index}`}
                                            type="capBaoCaoThauID"
                                            value={filterCapBaoCaoThaus.find(filter => filter.capBaoCaoThauID === level.capBaoCaoThauID)?.chuDauTu || 0}
                                            onChange={(e) => handleChangeFilterBidReportLevel(e, level)}
                                            input={<CustomInput size="small" label={level.tenCapBaoCaoThau} />}
                                        >
                                            <MenuItem value={0}>Tất cả</MenuItem>
                                            {level?.chuDauTu?.map((chuDauTuItem, idx) => (
                                                <MenuItem key={idx} value={chuDauTuItem.chuDauTu}>
                                                    {chuDauTuItem.chuDauTu}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                </Grid>
                            ))}
                            <Grid item xs={4}>
                                <FormControl variant="outlined" sx={{ width: '100%' }}>
                                    <InputLabel
                                        id="filter-status-label"
                                        sx={{ color: theme.palette.text.primary }}
                                    >
                                        Trạng thái
                                    </InputLabel>
                                    <Select
                                        labelId="filter-status-label"
                                        label="Trạng thái"
                                        id="filterStatus"
                                        value={filterStatus}
                                        onChange={(e) => setFilterStatus(e.target.value)}
                                        input={<CustomInput size="small" label="Trạng thái" />}
                                    >
                                        <MenuItem value="0" >Tất cả</MenuItem>
                                        <MenuItem value="open">Đang diễn ra</MenuItem>
                                        <MenuItem value="closed">Đã đóng</MenuItem>
                                        <MenuItem value="result">Có kết quả</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={4}>
                                <FormControl variant="outlined" sx={{ width: '100%' }}>
                                    <InputLabel
                                        id="filter-status-label"
                                        sx={{ color: theme.palette.text.primary }}
                                    >
                                        Lĩnh vực gói thầu
                                    </InputLabel>
                                    <Select
                                        labelId="filter-status-label"
                                        label="Lĩnh vực gói thầu"
                                        id="filterLoaiBaoCaoThauID"
                                        value={filterLoaiBaoCaoThauID}
                                        onChange={(e) => handleChangeFilter(e, setFilterLoaiBaoCaoThauID)}
                                        input={<CustomInput size="small" label="Lĩnh vực gói thầu" />}
                                    >
                                        <MenuItem value={0}>Tất cả</MenuItem>
                                        {dataBidReportTypes?.map((item: BidReportType, index) => (
                                            <MenuItem key={index} value={item.loaiBaoCaoThauID}>
                                                {item.tenLoaiBaoCaoThau}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={4}>
                                <Box style={{ width: "100%" }}>
                                    {/* <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                        Ngày đăng tải  <span className="required_text">(*)</span>{" "}
                                    </Typography> */}
                                    <div className="date-picker-container">
                                        <DatePicker
                                            locale={vi}
                                            className="react-date-picker"
                                            selected={thoiGianDongMo}
                                            onChange={(date: Date) => {
                                                setThoiGianDongMo(date);
                                            }}
                                            onYearChange={(date: Date) => {
                                                setThoiGianDongMo(date);
                                            }}
                                            onMonthChange={(date: Date) => {
                                                setThoiGianDongMo(date);
                                            }}
                                            customInput={<CustomInputDatePicker value={thoiGianDongMo ? thoiGianDongMo.toLocaleDateString() : ''} onClick={() => { }} />}
                                            dateFormat="MM/yyyy" // Chỉ định định dạng tháng/năm
                                            showMonthYearPicker // Bật chọn tháng và năm
                                            popperPlacement="top-end"
                                            placeholderText="Lọc theo tháng/năm"
                                            showPopperArrow={true}
                                        />
                                        {
                                            thoiGianDongMo && <span className="icon-clear" onClick={handleClearThoiGianDongMo}>
                                                <IconX color="red" />
                                            </span>
                                        }
                                    </div>

                                </Box>
                            </Grid>
                        </Grid>
                    </Box>
                    {filterDataBidReports && filterDataBidReports.length > 0 ? (
                        <Box
                            display="flex"
                            justifyContent="center"
                            alignItems="flex-start"
                            width="100%"
                            my={3}
                            gap={3}
                        >
                            <TableBidReports
                                dataBidReportLevels={dataBidReportLevels}
                                dataBidReportTypes={dataBidReportTypes}
                                rows={filterDataBidReports}
                                fetchData={fetchData}
                                dataFieldTypes={dataFieldTypes}
                                isAdmin={true}
                            />
                        </Box>
                    ) : (
                        <Box
                            display="flex"
                            justifyContent="center"
                            alignItems="flex-start"
                            width="100%"
                            my={6}
                            gap={3}
                        >
                            Không có dữ liệu
                        </Box>
                    )}
                    {
                        openUploadBaoCaoThau && <UploadFileDialog
                            title="Tải lên file báo cáo thầu"
                            defaulValue={null}
                            isInsert
                            handleOpen={setOpenUploadBaoCaoThau}
                            open={openUploadBaoCaoThau}
                            handlSaveFile={handleSaveFileImportBaoCaoThau}
                            handleDownloadFile={handleDownLoadFileImportBaoCaoThau}
                        />
                    }
                    {
                        openUploadKetQuaThau && <UploadFileDialog
                            title="Tải lên file kết quả thầu"
                            defaulValue={null}
                            isInsert
                            handleOpen={setOpenUploadKetQuaThau}
                            open={openUploadKetQuaThau}
                            handlSaveFile={handleSaveFileImportKetQuaThau}
                            handleDownloadFile={handleDownLoadFileImportKetQuaThau}

                        />
                    }


                </Box>
            ) : (
                // <Box
                //     display='flex'
                //     justifyContent='center'
                //     alignItems='flex-start'
                //     width='100%'
                //     my={6}
                //     gap={3}
                // >
                //     Không có quyền truy cập
                // </Box>
                <Box
                    height="60vh"
                    display="flex"
                    flexDirection="column"
                    justifyContent="center"
                    alignItems="center"
                    gap={3}
                >
                    <Typography variant="button" component="h1" fontSize="50px">
                        COMING SOON
                    </Typography>
                    <Button size="large" variant="contained" href="/home">
                        Trở về trang chủ
                    </Button>
                </Box>
            )}
        </AdminLayout>
    );
};
export default BidReportPage;
