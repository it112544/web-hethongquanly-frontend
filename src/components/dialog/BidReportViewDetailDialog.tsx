import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import dynamic from "next/dynamic";
import "react-quill/dist/quill.snow.css";
const QuillEditor = dynamic(() => import("react-quill"), { ssr: false });
import {
    Alert,
    Button,
    Card,
    Chip,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FilledInput,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    IconButton,
    Input,
    InputAdornment,
    InputBase,
    LinearProgress,
    MenuItem,
    OutlinedInput,
    Radio,
    RadioGroup,
    Rating,
    Select,
    Snackbar,
    Step,
    StepLabel,
    Stepper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    TextareaAutosize,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import CloseIcon from "@mui/icons-material/Close";
import { PropsDialog } from "@/interfaces/dialog";
import { LoadingButton } from "@mui/lab";
import { ChangeEvent, useEffect, useMemo, useRef, useState } from "react";
import useProvince from "@/hooks/useProvince";
import { Contractors } from "@/interfaces/contractors";
import useContractors from "@/hooks/useContractors";
import useContractorsType from "@/hooks/useContractorsType";
import { Products } from "@/interfaces/products";
import { addProduct, apiPort } from "@/constant/api";
import useProducts from "@/hooks/useProducts";
import useProductTypes from "@/hooks/useProductTypes";
import useSubjects from "@/hooks/useSubjects";
import useGrades from "@/hooks/useGrades";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import dayjs from "dayjs"; // Import dayjs library
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import MultipleSelectCheckBoxUtilizedObject from "../multiple-select/MultipleSelectCheckBoxUtilizedObject";
import { UtilizedObject, utilizedObjects } from "@/interfaces/utilizedObject";
import { useFormik } from "formik";
import * as yup from "yup";
import Divider from "@mui/material/Divider";
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { FileBidReportDto } from "@/interfaces/bidReport";


export default function BidReportViewDetailDialog(props: PropsDialog) {
    const { title, defaulValue, isInsert, handleOpen, open, isUpdate } = props;
    const onHandleClose = () => {
        handleOpen(false);
    };
    const handleDownloadFile = (file: string) => {
        const downloadUrl = `${apiPort}${file}`;
        window.open(downloadUrl, "_blank");
    };
    console.log('kkk', defaulValue);

    return (
        <>
            <Dialog
                maxWidth="xl"
                fullWidth
                open={open}
                // onClose={() => handleOpen(false)}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <DialogTitle sx={{ m: 0, p: 3 }} id="customized-dialog-title">
                    <Typography variant="h3">
                        {title} - {defaulValue?.tbmt}
                    </Typography>
                </DialogTitle>
                <IconButton
                    aria-label="close"
                    onClick={() => onHandleClose()}
                    sx={{
                        position: "absolute",
                        right: 8,
                        top: 8,
                    }}
                >
                    <CloseIcon />
                </IconButton>
                <DialogContent sx={{ p: 3 }}>
                    <Card variant="outlined" sx={{ p: 4 }}>
                        <Grid
                            container
                            direction="row"
                            justifyContent="center"
                            alignItems="center"
                        >
                            <Grid item md={11}>
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Tên gói thầu
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.tenGoiThau}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        TBMT
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.tbmt}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Bên mời thầu
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.benMoiThau}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Chủ đầu tư
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.chuDauTu}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Tỉnh/Thành phố (CĐT)
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.tinh?.tenTinh}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Quận/Huyện (CĐT)
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.huyen?.tenHuyen}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Thời gian thực hiện hợp đồng
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.thoiGianThucHienHopDong} Ngày
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Ngày đăng tải
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.ngayDangTai}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Thời gian đóng/mở
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.thoiGianDongMo}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Giá gói thầu
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.giaGoiThau.toLocaleString()}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Tiền đảm báo dự thầu
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.tienDamBaoDuThau.toLocaleString()}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Nguồn vốn
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.nguonVon}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Nhà thầu trúng
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.nhaThau?.tenCongTy}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Mã định danh
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.maDinhDanh}
                                        </Typography>
                                    </Grid>
                                </Grid>

                                {/* <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Người đại diện
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.nguoiDaiDien}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Địa chỉ
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.diaChi}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Cấp báo cáo thầu
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.capBaoCaoThau?.tenCapBaoCaoThau}
                                        </Typography>
                                    </Grid>
                                </Grid> */}
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Lĩnh vực báo cáo thầu
                                    </Grid>
                                    <Grid item md={6}>
                                        <Typography sx={{ ml: 1 }}>
                                            {defaulValue?.loaiBaoCaoThau?.tenLoaiBaoCaoThau}
                                        </Typography>
                                    </Grid>
                                </Grid>
                                <Divider sx={{ mb: 2, mt: 2 }} />
                                <Grid container>
                                    <Grid item md={6} sx={{ fontWeight: "bold" }}>
                                        Tệp đính kèm
                                    </Grid>
                                    <Grid item md={6}>
                                        {defaulValue?.fileBaoCaoThaus?.length > 0 ? (
                                            defaulValue.fileBaoCaoThaus.map((item: FileBidReportDto, index: number) => (
                                                <Box key={index} sx={{ position: 'relative', mb: 1, ml: 1 }}>
                                                    <Typography variant="body1" component="span" sx={{ cursor: 'pointer' }}>
                                                        {item.fileName} -212121
                                                    </Typography>
                                                    <IconButton>
                                                        <FileDownloadIcon
                                                            sx={{ ml: 1, cursor: 'pointer' }}
                                                            onClick={() => handleDownloadFile(item.fileUrl!)}
                                                        />
                                                    </IconButton>
                                                </Box>
                                            ))
                                        ) : (
                                            <Box>Không có</Box>
                                        )}
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Card>
                </DialogContent>
                <DialogActions sx={{ p: 3 }}></DialogActions>
            </Dialog>
        </>
    );
}
