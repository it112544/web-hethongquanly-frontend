import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { Alert, Dialog, DialogActions, DialogContent, DialogTitle, FilledInput, FormControl, FormControlLabel, FormHelperText, Grid, IconButton, Input, InputBase, LinearProgress, MenuItem, OutlinedInput, Radio, RadioGroup, Rating, Select, Snackbar, Step, StepLabel, Stepper, TextField } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import CloseIcon from '@mui/icons-material/Close';
import { PropsDialog } from '@/interfaces/dialog';
import { LoadingButton } from '@mui/lab';
import { useEffect, useMemo, useRef, useState } from 'react';
import useDistrict from '@/hooks/useDistrict';
import useProvince from '@/hooks/useProvince';
import { Organization } from '@/interfaces/organization';
import useOrganization from '@/hooks/useOrganization';
import useCommune from '@/hooks/useCommune';
import { CustomInput } from '../input';
import * as Yup from 'yup';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import { useFormik } from 'formik';
import { OrganizationType } from '@/interfaces/organizationType';
import axios from 'axios';
import { getLoaiCoQuan } from '@/constant/api';
export const validationUtilizedObjectSchema = Yup.object({
    maSoThue: Yup.string().required('Yêu cầu nhập mã số thuế'),
    tenCoQuan: Yup.string().required('Yêu cầu nhập tên cơ quan'),
    email: Yup.string().email('Yêu cầu nhập đúng định dạng Email'),
    loaiCoQuanID: Yup
        .number()
        .positive("Vui lòng nhập loại cơ quan") // Validates against negative values
        .required("Vui lòng nhập loại cơ quan") // Sets it as a compulsory field
        .min(1, "Vui lòng nhập loại cơ quan"),
    tinhID: Yup.number().required('Yêu cầu nhập tỉnh'),
    huyenID: Yup.number().required('Yêu cầu nhập huyện'),
    // xaID: Yup.number().required('Yêu cầu nhập xã'),
});
export default function OrganizationDialog(props: PropsDialog) {
    const { title, defaulValue, isInsert, handleOpen, open, isUpdate, id } = props
    const theme = useTheme()
    const [formData, setFormData] = useState<Organization>(defaulValue);
    const { getAllProvince, dataProvince } = useProvince()

    const [entityError, setEntityError] = useState(null);
    const [loading, setLoading] = useState<boolean>(false);

    const router = useRouter()
    // Lấy đường dẫn URL hiện tại
    const currentPath = router.asPath;
    const { addOrganization, updateOrganization } = useOrganization(currentPath)

    const formik = useFormik({
        initialValues: {
            maSoThue: defaulValue?.maSoThue ?? "",
            tenCoQuan: defaulValue?.tenCoQuan ?? "",
            email: defaulValue?.email ?? "",
            soDienThoai: defaulValue?.soDienThoai ?? "",
            diaChi: defaulValue?.diaChi ?? "",
            tinhID: defaulValue?.tinhID ?? null,
            huyenID: defaulValue?.huyenID ?? null,
            xaID: defaulValue?.xaID ?? null,
            loaiCoQuanID: defaulValue?.loaiCoQuanID ?? 0
        },
        validationSchema: validationUtilizedObjectSchema,
        onSubmit: async (values) => {
            setEntityError(null);
            const data: Organization = {
                ...values,

            };
            try {
                if (isInsert) {
                    const response = await addOrganization(data);
                    handleOpen(false);
                    formik.resetForm();
                    setLoading(false);
                    toast.success("Thêm dữ liệu thành công", {});
                } else {
                    data.coQuanID = defaulValue.coQuanID;
                    const response = await updateOrganization(data);

                    setLoading(false);
                    toast.success("Cập nhật dữ liệu thành công", {});
                }
            } catch (error: any) {
                setEntityError(error.response.data);
            }
        },
    });

    const [loaiCoQuan, setLoaiCoQuan] = useState("")
    const [error, setError] = useState(false);
    const [touched, setTouched] = useState(false);
    const { getDistrictByProvinceId, dataDistrict } = useDistrict(formik?.values?.huyenID, formik?.values?.tinhID)
    const { dataCommune } = useCommune(formik?.values?.xaID, formik?.values?.huyenID)
    const [organizationTypes, setOrganizationTypes] = useState<OrganizationType[]>([]);
    useEffect(() => {
        const fetchData = async () => {
            try {
                const accessToken = window.localStorage.getItem('accessToken');
                if (!accessToken) {
                    console.error("Access token không tồn tại");
                    return;
                }
                const headers = { Authorization: `Bearer ${accessToken}` };
                const response = await axios.get(getLoaiCoQuan, { headers });
                setOrganizationTypes(response.data ?? []);
            } catch (error) {
                console.error("Lỗi khi lấy dữ liệu loại cơ quan:", error);
            }
        };

        fetchData();
    }, []);
    useEffect(() => {
        if (defaulValue) {
            formik.setValues({
                maSoThue: defaulValue?.maSoThue ?? "",
                tenCoQuan: defaulValue?.tenCoQuan ?? "",
                email: defaulValue?.email ?? "",
                soDienThoai: defaulValue?.soDienThoai ?? "",
                diaChi: defaulValue?.diaChi ?? "",
                tinhID: defaulValue?.tinhID ?? null,
                huyenID: defaulValue?.huyenID ?? null,
                xaID: defaulValue?.xaID ?? null,
                loaiCoQuanID: defaulValue?.loaiCoQuanID ?? 0
            });
        }
    }, [defaulValue]);
    const handleChangeSelect = (e: any) => {
        if (e.target) {
            setLoaiCoQuan(e.target.value)
            if (e.target.value !== '') {
                setError(false);
            }
        }

    }
    const handleBlur = () => {
        setTouched(true);
        if (loaiCoQuan === '') {
            setError(true);
        } else {
            setError(false);
        }
    };
    const handleChange = (e: any) => {
        if (e.target) {
            const { name, value } = e.target;

            // Kiểm tra định dạng email nếu trường email không trống
            if (name === "email") {
                const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                if (value && !emailRegex.test(value)) {
                    setError(true);
                } else {
                    setError(false);
                }
            }

            setFormData((prevState: any) => ({
                ...prevState,
                [name]: value,
            }));
        }
    };

    const handleAdd = (e: any) => {
        e.preventDefault();
        if (loaiCoQuan === '') {
            setError(true);
            setTouched(true);
        } else {
            setError(false);
            setLoading(true);
            formData.tenCoQuan = loaiCoQuan + " " + formData?.tenCoQuan
            if (formData) addOrganization(formData)
            toast.success("Thêm thành công")
            setLoading(false);
            handleOpen(false)
        }

    }
    const handleUpdate = (e: any) => {
        e.preventDefault();
        if (loaiCoQuan === '') {
            setError(true);
            setTouched(true);
        } else {
            setError(false);
            setLoading(true);
            if (formData) updateOrganization(formData)
            toast.success("Cập nhật thành công")
            setLoading(false);
            handleOpen(false)
        }

    }
    const selectOrganizationType = [
        "SGD",
        "PGD",
        "STC",
        "PTC",
        "BQLDA",
        "UBND",
        "Trường",
    ]

    return (
        <>

            <Dialog
                maxWidth='md'
                fullWidth
                open={open}
                onClose={() => handleOpen(false)}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"

            >
                <DialogTitle sx={{ m: 0, p: 3 }} id="customized-dialog-title">
                    <Typography variant='h3'>{title}</Typography>
                </DialogTitle>
                <IconButton
                    aria-label="close"
                    onClick={() => handleOpen(false)}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                    }}
                >
                    <CloseIcon />
                </IconButton>
                <DialogContent sx={{ p: 3 }} >
                    <Box display='flex' flexDirection='column' justifyContent='space-between' alignItems='center' gap='12px'>
                        <Grid container spacing={2}>
                            {entityError && (
                                <Grid container spacing={2}>
                                    <Grid item xs={6}>
                                        <Typography
                                            sx={{ ml: 1.5, mb: 1.5, fontWeight: "bold" }}
                                            className="required_text"
                                        >
                                            {JSON.stringify(entityError)}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            )}
                            <Grid item md={4}>
                                <Box style={{ width: "100%" }}>
                                    <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                        Mã số thuế <span className="required_text">(*)</span>{" "}
                                    </Typography>
                                    <TextField
                                        variant="outlined"
                                        fullWidth
                                        id="maSoThue"
                                        name="maSoThue"
                                        value={formik.values.maSoThue}
                                        onChange={formik.handleChange}
                                        onBlur={formik.handleBlur}
                                        error={
                                            formik.touched.maSoThue &&
                                            Boolean(formik.errors.maSoThue)
                                        }
                                    />
                                    {formik.touched.maSoThue &&
                                        Boolean(formik.errors.maSoThue) && (
                                            <FormHelperText className="required_text">
                                                {" "}
                                                {formik.errors.maSoThue
                                                    ? formik.errors.maSoThue.toString()
                                                    : ""}
                                            </FormHelperText>
                                        )}
                                </Box>
                            </Grid>
                            <Grid item xs={8}>

                                <Box style={{ width: "100%" }} sx={{ mb: 1.5 }}>
                                    <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>
                                        <span style={{ marginRight: '8px' }}>Loại cơ quan</span>
                                        <span className="required_text">(*)</span>
                                    </Typography>
                                    <FormControl fullWidth>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="loaiCoQuanID"
                                            name="loaiCoQuanID"
                                            type="loaiCoQuanID"
                                            value={formik.values.loaiCoQuanID}
                                            onChange={(event) => {
                                                const selectedId = event.target.value;
                                                const selectedType = organizationTypes.find(
                                                    (item) => item.loaiCoQuanID === selectedId
                                                );

                                                if (selectedType) {
                                                    let updatedTenCoQuan = formik.values.tenCoQuan;

                                                    // Nếu `updatedTenCoQuan` rỗng, gán `selectedType.tenLoaiCoQuan` làm giá trị mới.
                                                    if (!updatedTenCoQuan.trim()) {
                                                        updatedTenCoQuan = selectedType.tenLoaiCoQuan;
                                                    } else {
                                                        updatedTenCoQuan = updatedTenCoQuan.replace(/^\S+/, selectedType.tenLoaiCoQuan);
                                                    }

                                                    formik.setFieldValue('loaiCoQuanID', selectedId);
                                                    formik.setFieldValue('tenCoQuan', updatedTenCoQuan);
                                                }
                                            }}
                                            onBlur={formik.handleBlur}
                                            error={
                                                formik.touched.loaiCoQuanID &&
                                                Boolean(formik.errors.loaiCoQuanID)
                                            }
                                        >
                                            {organizationTypes && organizationTypes.length > 0 ? (
                                                organizationTypes.map((item, index) => (
                                                    <MenuItem
                                                        key={index}
                                                        value={item.loaiCoQuanID}
                                                    >
                                                        {item.tenLoaiCoQuan}
                                                    </MenuItem>
                                                ))
                                            ) : (
                                                <MenuItem value="" disabled>
                                                    Không có dữ liệu
                                                </MenuItem>
                                            )}
                                        </Select>
                                    </FormControl>
                                    {formik.touched.loaiCoQuanID &&
                                        Boolean(formik.errors.loaiCoQuanID) && (
                                            <FormHelperText className="required_text">
                                                {" "}
                                                {formik.errors.loaiCoQuanID
                                                    ? formik.errors.loaiCoQuanID.toString()
                                                    : ""}
                                            </FormHelperText>
                                        )}
                                </Box>
                            </Grid>
                        </Grid>
                        <Box style={{ width: "100%" }}>
                            <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                Tên cơ quan <span className="required_text">(*)</span>{" "}
                                <span style={{ fontSize: '12px' }}>(Lưu ý: tên cơ quan: Loại cơ quan + tên cơ quan. Ví dụ: Sở giáo dục Cần Thơ = SGD Cần Thơ,
                                    Phòng giáo dục Cần Thơ = PGD Cần Thơ, Ban quản lý dự án= BQLDA Cần Thơ,ủy bản nhân dân Cần Thơ = UBND Cần Thơ )</span>
                            </Typography>
                            <TextField
                                variant="outlined"
                                fullWidth
                                id="tenCoQuan"
                                name="tenCoQuan"
                                value={formik.values.tenCoQuan}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={
                                    formik.touched.tenCoQuan &&
                                    Boolean(formik.errors.tenCoQuan)
                                }
                            />
                            {formik.touched.tenCoQuan &&
                                Boolean(formik.errors.tenCoQuan) && (
                                    <FormHelperText className="required_text">
                                        {" "}
                                        {formik.errors.tenCoQuan
                                            ? formik.errors.tenCoQuan.toString()
                                            : ""}
                                    </FormHelperText>
                                )}
                        </Box>
                        <Box style={{ width: "100%" }}>
                            <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                Email
                            </Typography>
                            <TextField
                                variant="outlined"
                                fullWidth
                                id="email"
                                name="email"
                                value={formik.values.email}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={
                                    formik.touched.email &&
                                    Boolean(formik.errors.email)
                                }
                            />
                            {formik.touched.email &&
                                Boolean(formik.errors.email) && (
                                    <FormHelperText className="required_text">
                                        {" "}
                                        {formik.errors.email
                                            ? formik.errors.email.toString()
                                            : ""}
                                    </FormHelperText>
                                )}
                        </Box>
                        <Box style={{ width: "100%" }}>
                            <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                Số điện thoại
                            </Typography>
                            <TextField
                                variant="outlined"
                                fullWidth
                                id="soDienThoai"
                                name="soDienThoai"
                                value={formik.values.soDienThoai}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={
                                    formik.touched.soDienThoai &&
                                    Boolean(formik.errors.soDienThoai)
                                }
                            />
                            {formik.touched.soDienThoai &&
                                Boolean(formik.errors.soDienThoai) && (
                                    <FormHelperText className="required_text">
                                        {" "}
                                        {formik.errors.soDienThoai
                                            ? formik.errors.soDienThoai.toString()
                                            : ""}
                                    </FormHelperText>
                                )}
                        </Box>
                        <Box style={{ width: "100%" }}>
                            <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                Địa chỉ
                            </Typography>
                            <TextField
                                variant="outlined"
                                fullWidth
                                id="diaChi"
                                name="diaChi"
                                value={formik.values.diaChi}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                error={
                                    formik.touched.diaChi &&
                                    Boolean(formik.errors.diaChi)
                                }
                            />
                            {formik.touched.diaChi &&
                                Boolean(formik.errors.diaChi) && (
                                    <FormHelperText className="required_text">
                                        {" "}
                                        {formik.errors.diaChi
                                            ? formik.errors.diaChi.toString()
                                            : ""}
                                    </FormHelperText>
                                )}
                        </Box>
                        <Grid container spacing={2}>
                            <Grid item md={4}>
                                <FormControl
                                    fullWidth
                                    error={formik.touched.tinhID && Boolean(formik.errors.tinhID)}
                                    required
                                >
                                    <Box style={{ width: '100%' }}>
                                        <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                            Tỉnh/Thành phố <span className="required_text">(*)</span>{" "}
                                        </Typography>
                                        <Select
                                            name="tinhID"
                                            value={formik.values.tinhID || ""}
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur} // Để đánh dấu trường này đã được chạm vào
                                            fullWidth
                                            input={<CustomInput />}
                                        >
                                            {dataProvince?.map((item, index) => (
                                                <MenuItem key={index} value={item.tinhID}>
                                                    {item.tenTinh}
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </Box>
                                    {formik.touched.tinhID && formik.errors.tinhID && (
                                        <FormHelperText>{formik.errors.tinhID.toString()}</FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                            <Grid item md={4}>
                                <FormControl
                                    fullWidth
                                    error={formik.touched.huyenID && Boolean(formik.errors.huyenID)}
                                    required
                                >
                                    <Box style={{ width: '100%' }}>
                                        <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                            Quận/Huyện <span className="required_text">(*)</span>{" "}
                                        </Typography>
                                        <Select
                                            name="huyenID"
                                            value={formik.values.huyenID || ""}
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur} // Đánh dấu đã chạm vào trường
                                            fullWidth
                                            input={<CustomInput />}
                                        >
                                            {dataDistrict.length > 0 ? (
                                                dataDistrict.map((item, index) => (
                                                    <MenuItem key={index} value={item.huyenID}>
                                                        {item.tenHuyen}
                                                    </MenuItem>
                                                ))
                                            ) : (
                                                <MenuItem value={undefined}>Vui lòng chọn tỉnh thành trước</MenuItem>
                                            )}
                                        </Select>
                                    </Box>
                                    {formik.touched.huyenID && formik.errors.huyenID && (
                                        <FormHelperText>{formik.errors.huyenID.toString()}</FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                            <Grid item md={4}>
                                <FormControl
                                    fullWidth
                                    error={formik.touched.xaID && Boolean(formik.errors.xaID)}
                                    required
                                >
                                    <Box style={{ width: '100%' }}>
                                        <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                            Phường/Xã <span className="required_text">(*)</span>{" "}
                                        </Typography>
                                        <Select
                                            name="xaID"
                                            value={formik.values.xaID || ""}
                                            onChange={formik.handleChange}
                                            onBlur={formik.handleBlur} // Đánh dấu đã chạm vào trường
                                            fullWidth
                                            input={<CustomInput />}
                                        >
                                            {dataCommune.length > 0 ? (
                                                dataCommune.map((item, index) => (
                                                    <MenuItem key={index} value={item.xaID}>
                                                        {item.tenXa}
                                                    </MenuItem>
                                                ))
                                            ) : (
                                                <MenuItem value={undefined}>
                                                    Vui lòng chọn tỉnh và huyện trước
                                                </MenuItem>
                                            )}
                                        </Select>
                                    </Box>
                                    {formik.touched.xaID && formik.errors.xaID && (
                                        <FormHelperText>{formik.errors.xaID.toString()}</FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                        </Grid>
                    </Box>
                </DialogContent>
                <DialogActions sx={{ p: 3 }}>

                    {isInsert &&
                        <LoadingButton
                            sx={{ p: '12px 24px' }}
                            onClick={() => formik.handleSubmit()}
                            loading={loading}
                            variant="contained"
                            size='large'
                        >
                            Thêm cơ quan
                        </LoadingButton>
                    }
                    {isUpdate &&
                        <LoadingButton
                            sx={{ p: '12px 24px' }}
                            onClick={() => formik.handleSubmit()}
                            loading={loading}
                            variant="contained"
                            size='large'
                        >
                            Cập nhật cơ quan
                        </LoadingButton>
                    }
                </DialogActions>
            </Dialog >


        </>
    );
}
