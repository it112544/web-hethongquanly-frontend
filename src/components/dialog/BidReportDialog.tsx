import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import dynamic from "next/dynamic";
import "react-quill/dist/quill.snow.css";
import { red } from '@mui/material/colors';
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { format } from 'date-fns';
const QuillEditor = dynamic(() => import("react-quill"), { ssr: false });
import {
    Alert,
    Autocomplete,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FilledInput,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    IconButton,
    Input,
    InputAdornment,
    InputBase,
    LinearProgress,
    MenuItem,
    OutlinedInput,
    Radio,
    RadioGroup,
    Rating,
    Select,
    Snackbar,
    Step,
    StepLabel,
    Stepper,
    TextField,
    TextareaAutosize,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import CloseIcon from "@mui/icons-material/Close";
import { PropsDialog } from "@/interfaces/dialog";
import { ChangeEvent, useEffect, useMemo, useRef, useState } from "react";
import useProvince from "@/hooks/useProvince";
import { Contractors, SendMailContractor } from "@/interfaces/contractors";
import useContractors from "@/hooks/useContractors";
import useContractorsType from "@/hooks/useContractorsType";
import { addBidReport, addSampleMail, addUnit, apiPort, getAllAreaOfOperation, getAllNhaThauByTuKhoa, getAllTypeOfCooperation, getHuyenByID, getHuyenByTinhID, getTinh, updateBidReport } from "@/constant/api";
import useUnits from "@/hooks/useUnits";
import dayjs from "dayjs"; // Import dayjs library
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import { vi } from "date-fns/locale";

import { useFormik } from "formik";
import * as yup from "yup";
import { Grades } from "@/interfaces/grades";
import { Circulars } from "@/interfaces/circulars";
import { Subjects } from "@/interfaces/subjects";
import { stringify } from "querystring";
import { toast } from "react-toastify";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import { Units } from "@/interfaces/units";
import { ClearIcon } from "@mui/x-date-pickers/icons";
import axios from "axios";
import { BidReport, CreateBidReportDto, FileBidReportDto } from "@/interfaces/bidReport";
import MultipleSelectCheckBoxGrade from "../multiple-select/MultipleSelectCheckBoxGrade";
import { FieldType } from "@/interfaces/fieldType";
import MultipleSelectCheckBoxFieldType from "../multiple-select/MultipleSelectCheckBoxFieldType";
import { Province } from "@/interfaces/province";
import { District } from "@/interfaces/district";
import DatePicker from "react-datepicker";
import { LoadingButton } from "@mui/lab";
import { IconCalendar } from "@tabler/icons-react";
import { BidReportLevel } from "@/interfaces/bidReportLevel";
import { BidReportType } from "@/interfaces/bidReportType";
import top100Films from "./top100Films";
import useDebounce from "@/hooks/useDebounce";
import { TypeOfCooperations } from "@/interfaces/TypeOfCooperation";
import { AreaOfOperations } from "@/interfaces/areaOfOperation";
import ContractorsDialog from "./ContractorsDialog";
interface CustomInputProps {
    value: string;
    onClick: () => void;
}

export function CustomInput({ value, onClick }: CustomInputProps) {
    return (
        <div className="input-group position-relative">
            <input
                type="text"
                className="form-control custom-input"
                value={value}
                onClick={onClick}
                readOnly
                style={{ width: '100%', height: '20px !important' }} // Giảm chiều rộng để có chỗ cho biểu tượng "X"
            />
            <span className="icon-background" onClick={onClick}>
                <IconCalendar color="white" />
            </span>

        </div>
    );
}
export const validationUtilizedObjectSchema = yup.object({
    tenGoiThau: yup.string().required("Vui lòng nhập tên gói thầu"),
    tbmt: yup.string().required("Vui lòng nhập tên tbmt"),
    benMoiThau: yup.string().required("Vui lòng nhập bên mời thầu"),
    chuDauTu: yup.string().required("Vui lòng nhập chủ đầu tư"),
    thoiGianThucHienHopDong: yup
        .number()
        .typeError("Vui lòng nhập số")
        .required("Vui lòng nhập thời gian thực hiện hợp đồng")
        .positive("Thời gian thực hiện hợp đồng phải là số dương"),
    ngayDangTai: yup.string().required("Vui lòng nhập ngày đăng tải"),
    thoiGianDongMo: yup.string().required("Vui lòng nhập thời gian đóng mở"),
    tienDamBaoDuThau: yup
        .number()
        .typeError("Vui lòng nhập định dạng số")
        .positive("Vui lòng nhập định dạng số") // Validates against negative values
        .required("Vui lòng nhập định dạng số") // Sets it as a compulsory field
        .min(1, "Vui lòng nhập định dạng số"),
    giaGoiThau: yup
        .number()
        .typeError("Vui lòng nhập định dạng số")
        .positive("Vui lòng nhập định dạng số") // Validates against negative values
        .required("Vui lòng nhập định dạng số") // Sets it as a compulsory field
        .min(1, "Vui lòng nhập định dạng số"),
    nguonVon: yup.string().required("Vui lòng nhập nguồn vốn"),
    // nhaThauTrung: yup.string().required("Vui lòng nhập nhà thầu trúng"),
    //  diaChi: yup.string().required("Vui lòng nhập địa chỉ"),
    // nguoiDaiDien: yup.string().required("Vui lòng nhập người đại diện"),
    // linhVucHangHoa: yup.string().required("Vui lòng nhập lĩnh vực hàng hóa"),
    tinhID: yup
        .number()
        .positive("Vui lòng chọn tỉnh") // Validates against negative values
        .required("Vui lòng chọn tỉnh") // Sets it as a compulsory field
        .min(1, "Vui lòng nhập chọn tỉnh"),
    capBaoCaoThauID: yup
        .number()
        .positive("Vui lòng chọn cấp sở ban ngành") // Validates against negative values
        .required("Vui lòng chọn cấp sở ban ngành") // Sets it as a compulsory field
        .min(1, "Vui lòng nhập chọn cấp sở ban ngành"),
    loaiBaoCaoThauID: yup
        .number()
        .positive("Vui lòng chọn lĩnh vực gói thầu") // Validates against negative values
        .required("Vui lòng chọn lĩnh vực gói thầu") // Sets it as a compulsory field
        .min(1, "Vui lòng nhập chọn lĩnh vực gói thầu"),
    // nhaThauID: yup
    //     .number()
    //     .positive("Vui lòng chọn nhà thầu") // Validates against negative values
    //     .required("Vui lòng chọn nhà thầu") // Sets it as a compulsory field
    //     .min(1, "Vui lòng nhập chọn nhà thầu"),
    huyenID: yup
        .number()
        .positive("Vui lòng chọn huyện") // Validates against negative values
        .required("Vui lòng chọn huyện") // Sets it as a compulsory field
        .min(1, "Vui lòng nhập chọn huyện"),
    // loaiLinhVucs: yup
    //     .array()
    //     .min(1, "Vui lòng chọn loại lĩnh vực"),
});
export interface Props {
    title: string,
    defaulValue?: any,
    isInsert?: boolean
    isUpdate?: boolean,
    open: boolean,
    id?: number,
    idParent?: number,
    file?: File | null,
    handleUploadFile?: (e: any) => void,
    handleOpen: (e: boolean) => void,
    handlSaveFile?: (e: any) => void,
    note?: string;
    fetchData?: () => void;
    dataFieldTypes: FieldType[];
    dataBidReportLevels: BidReportLevel[];
    dataBidReportTypes: BidReportType[];
    size?: "xs" | "sm" | "md" | "lg" | "xl"
}

export default function BidReportDialog(props: Props) {
    const { title, defaulValue, isInsert, handleOpen, open, isUpdate, fetchData, dataFieldTypes, dataBidReportLevels, dataBidReportTypes } = props;
    const [loading, setLoading] = useState<boolean>(false);
    const [entityError, setEntityError] = useState(null);
    const [selectedFieldTypes, setSelectedFieldTypes] = useState<FieldType[]>([]);
    const [dataProvinces, setDataProvinces] = useState<Province[]>([]);
    const [dataDistricts, setDataDistricts] = useState<District[]>([]);
    const [giaGoiThau, setGiaGoiThau] = useState<string>("");
    const [tienDamBaoDuThau, setTienDamBaoGoiDuThau] = useState<string>("");
    const [ngayDangTai, setNgayDangTai] = useState(new Date());
    const [filePreviews, setFilePreviews] = useState<{ fileUrl: string, fileName: string, fileType: string, fileID?: number | null, link?: string }[]>([]);
    const [thoiGianDongMo, setThoiGianDongMo] = useState(new Date());
    const [searchString, setSearchString] = useState('');
    const debouncedSearchTerm = useDebounce(searchString, 500);
    const [dropdownOptions, setDropdownOptions] = useState<Contractors[]>([]);
    const [openContractorDialog, setOpenContractorDialog] = useState(false);
    const [typeOfCooperations, setTypeOfCooperations] = useState<TypeOfCooperations[]>([]);
    const [areaOfOperations, setAreaOfOperations] = useState<AreaOfOperations[]>([]);
    useEffect(() => {
        const fetchData = async () => {
            const accessToken = window.localStorage.getItem('accessToken');
            const headers = { Authorization: `Bearer ${accessToken}` };
            const responseDataAreaOfOpertations = await axios.get(getAllAreaOfOperation, { headers });
            const responseDataTypeOfCooperations = await axios.get(getAllTypeOfCooperation, { headers });
            setTypeOfCooperations(responseDataTypeOfCooperations.data);
            setAreaOfOperations(responseDataAreaOfOpertations.data);
        };
        fetchData(); // Call the async function inside useEffect
    }, []);
    const [contractors, setContractors] = useState<Contractors | null>();

    const [loadingSearch, setLoadingSearch] = useState<boolean>(false);
    useEffect(() => {
        const fetchData = async () => {
            if (debouncedSearchTerm.length >= 2) {
                setLoadingSearch(true);
                try {
                    const accessToken = window.localStorage.getItem("accessToken");
                    if (!accessToken) {
                        throw new Error("No access token found");
                    }
                    const headers = { Authorization: `Bearer ${accessToken}` };
                    const response = await axios.get(getAllNhaThauByTuKhoa + '?Key=' + debouncedSearchTerm, { headers });

                    if (response.data) {
                        setDropdownOptions(response.data);
                    }
                } catch (error) {
                    console.error("Error fetching data:", error);
                } finally {
                    setLoadingSearch(false);
                }
                console.log('API Call with:', debouncedSearchTerm);
            } else {
                setDropdownOptions([]); // Clear options if the search term is too short
            }
        };

        fetchData();
    }, [debouncedSearchTerm]);
    const fileInputRef = useRef<HTMLInputElement>(null);
    const formik = useFormik({
        initialValues: {
            tenGoiThau: defaulValue?.tenGoiThau ?? "",
            tbmt: defaulValue?.tbmt ?? "",
            benMoiThau: defaulValue?.benMoiThau ?? "",
            chuDauTu: defaulValue?.chuDauTu ?? "",
            thoiGianThucHienHopDong: defaulValue?.thoiGianThucHienHopDong ?? "",
            ngayDangTai: defaulValue?.ngayDangTai ?? format(new Date(), 'dd/MM/yyyy HH:mm:ss'),
            thoiGianDongMo: defaulValue?.thoiGianDongMo ?? format(new Date(), 'dd/MM/yyyy HH:mm:ss'),
            tienDamBaoDuThau: defaulValue?.tienDamBaoDuThau ?? 0,
            giaGoiThau: defaulValue?.giaGoiThau ?? 0,
            nguonVon: defaulValue?.nguonVon ?? "",
            maDinhDanh: defaulValue?.maDinhDanh ?? "",
            diaChi: defaulValue?.diaChi ?? "",
            nguoiDaiDien: defaulValue?.nguoiDaiDien ?? "",
            capBaoCaoThauID: defaulValue?.capBaoCaoThauID ?? 0,
            loaiBaoCaoThauID: defaulValue?.loaiBaoCaoThauID ?? 0,
            tinhID: defaulValue?.tinhID ?? 0,
            huyenID: defaulValue?.huyenID ?? 0,
            nhaThauID: defaulValue?.nhaThauID ?? null,
            loaiLinhVucs: selectedFieldTypes,
        },
        validationSchema: validationUtilizedObjectSchema,
        onSubmit: async (values) => {
            setEntityError(null);
            let createBidReportDto: BidReport = {
                ...values,
            }
            try {
                let createBidReportDto: CreateBidReportDto = {
                    baoCaoThauDto: {
                        ...values,
                        nhaThauID: contractors ? values.nhaThauID : null
                    },
                    fileBaoCaoThauDtos: [
                        ...filePreviews
                    ]
                }
                setLoading(true);
                if (isInsert) {
                    const accessToken = window.localStorage.getItem('accessToken');
                    const headers = { Authorization: `Bearer ${accessToken}`, 'Content-Type': 'application/json' };
                    const response = await axios.post(addBidReport, createBidReportDto, { headers });
                    handleOpen(false);
                    fetchData && fetchData();
                    formik.resetForm();
                    setLoading(false);
                    toast.success("Thêm dữ liệu thành công", {});
                } else {
                    const accessToken = window.localStorage.getItem('accessToken');
                    const headers = { Authorization: `Bearer ${accessToken}`, 'Content-Type': 'application/json' };
                    createBidReportDto.baoCaoThauDto.baoCaoThauID = defaulValue.baoCaoThauID;
                    const response = await axios.put(updateBidReport, createBidReportDto, { headers });
                    handleOpen(false);
                    fetchData && fetchData();
                    formik.resetForm();
                    setLoading(false);
                    toast.success("cập nhật dữ liệu thành công", {});
                }
            } catch (error: any) {
                setEntityError(error.response.data);
            }
        },
    });
    const handleSelectedFieldTypes = (data: FieldType[]) => {
        formik.setFieldValue("loaiLinhVucs", data);
        setSelectedFieldTypes(data);
    };
    console.log('ds', formik.errors);

    const handleChangeFile = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files) {
            const filesArray = Array.from(e.target.files);

            const readerPromises = filesArray.map((file) => {
                return new Promise<{ fileUrl: string, fileName: string, fileType: string, fileID: number | null }>((resolve, reject) => {
                    const reader = new FileReader();
                    reader.onloadend = () => {
                        const base64String = reader.result as string;
                        resolve({
                            fileUrl: base64String,
                            fileName: file.name,
                            fileType: file.type,
                            fileID: 0 // Assuming fileID is initially null for new uploads
                        });
                    };
                    reader.onerror = reject;
                    reader.readAsDataURL(file); // Read file as base64
                });
            });

            Promise.all(readerPromises).then(fileObjects => {
                setFilePreviews(prevState => [...prevState, ...fileObjects]);
            });

            e.target.value = "";
        }
    };
    const handleChangeText = (event: any) => {
        const inputValue = event.target.value.trim();
        setSearchString(inputValue);
    };
    function parseDateString(dateString: string) {
        const regex = /(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2}):(\d{2})/;
        const match = dateString.match(regex);
        if (!match) return null;

        const [, day, month, year, hour, minute, second] = match.map(Number);
        return new Date(year, month - 1, day, hour, minute, second);
    }
    const convertUrlToBase64 = async (url: string): Promise<string> => {
        const response = await fetch(url);
        const blob = await response.blob();
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result as string);
            reader.onerror = reject;
            reader.readAsDataURL(blob);
        });
    };
    useEffect(() => {
        // Kiểm tra xem defaulValue có tồn tại hay không trước khi cập nhật giá trị mặc định của formik
        const fetchData = async () => {
            if (defaulValue) {

                formik.setValues({
                    tenGoiThau: defaulValue?.tenGoiThau ?? "",
                    tbmt: defaulValue?.tbmt ?? "",
                    benMoiThau: defaulValue?.benMoiThau ?? "",
                    chuDauTu: defaulValue?.chuDauTu ?? "",
                    thoiGianThucHienHopDong: defaulValue?.thoiGianThucHienHopDong ?? "",
                    ngayDangTai: defaulValue?.ngayDangTai ?? "",
                    thoiGianDongMo: defaulValue?.thoiGianDongMo ?? "",
                    tienDamBaoDuThau: defaulValue?.tienDamBaoDuThau ?? 0,
                    giaGoiThau: defaulValue?.giaGoiThau ?? 0,
                    nguonVon: defaulValue?.nguonVon ?? "",
                    maDinhDanh: defaulValue?.maDinhDanh ?? "",
                    loaiLinhVucs: defaulValue.loaiLinhVucs,
                    capBaoCaoThauID: defaulValue?.capBaoCaoThauID ?? 0,
                    nhaThauID: defaulValue?.nhaThauID ?? 0,
                    loaiBaoCaoThauID: defaulValue?.loaiBaoCaoThauID ?? 0,
                    tinhID: defaulValue?.tinhID ?? 0,
                    huyenID: defaulValue?.huyenID ?? 0,
                    diaChi: defaulValue?.diaChi ?? "",
                    nguoiDaiDien: defaulValue?.nguoiDaiDien ?? "",
                });
                setSelectedFieldTypes(defaulValue.loaiLinhVucs);
                setGiaGoiThau(formatPrice(defaulValue.giaGoiThau));
                setTienDamBaoGoiDuThau(formatPrice(defaulValue.tienDamBaoDuThau));
                setContractors(defaulValue?.nhaThau);
                const newDropdownOptions = [...dropdownOptions]; // Create a copy of the existing options
                if (defaulValue?.nhaThau) {
                    newDropdownOptions.push(defaulValue.nhaThau); // Push the default value into the array
                }
                setDropdownOptions(newDropdownOptions);
                const parsedNgayDangTai = defaulValue.ngayDangTai ? parseDateString(defaulValue.ngayDangTai) : new Date();
                const parsedThoiGianDongMo = defaulValue.thoiGianDongMo ? parseDateString(defaulValue.thoiGianDongMo) : new Date();
                setThoiGianDongMo(parsedThoiGianDongMo as Date);
                setNgayDangTai(parsedNgayDangTai as Date);

                let arrayFile: FileBidReportDto[] = [];
                if (defaulValue.fileBaoCaoThaus && defaulValue.fileBaoCaoThaus.length > 0) {
                    for (const item of defaulValue.fileBaoCaoThaus) {
                        let newItem = { ...item };
                        newItem.fileUrl = apiPort + newItem.fileUrl;
                        try {
                            newItem.link = newItem.fileUrl;
                            const base64Url = await convertUrlToBase64(newItem.fileUrl);
                            newItem.fileUrl = base64Url;

                            if (newItem.loai === 1) {
                                arrayFile.push(newItem);
                            } else {
                                arrayFile.push(newItem);
                            }
                        } catch (error) {
                            console.error("Error converting file URL to base64:", error);
                        }
                    }
                }
                setFilePreviews(arrayFile);
            }

        }

        fetchData();
    }, [defaulValue]);
    const handleDeleteFile = (index: number) => {
        setFilePreviews(prevState => {
            const updatedPreviews = [...prevState];
            updatedPreviews.splice(index, 1);
            return updatedPreviews;
        });
    };
    console.log('hahah', defaulValue);

    const onHandleClose = () => {
        handleOpen(false);
        if (isInsert) {
            formik.resetForm();
            setEntityError(null);
        }
    };
    const handleButtonClickFile = () => {
        if (fileInputRef.current) {
            fileInputRef.current.click();
        }
    };
    const handleContractorChange = (
        event: any,
        value: Contractors,
    ) => {
        // Kiểm tra xem giá trị có hợp lệ không
        if (value) {
            formik.setFieldValue('nhaThauID', value?.nhaThauID);
            setContractors(value);
        }
    };
    const handleDownloadFile = (file: string) => {
        const downloadUrl = `${file}`;

        window.open(downloadUrl, "_blank");
    };
    const formatPrice = (value: string | null | undefined): string => {
        return (value ?? "").toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };
    const handleChangePrice = (
        e: ChangeEvent<HTMLInputElement>,
        setter: Function
    ) => {
        let inputValue: string = e.target.value;
        let inputName: string = e.target.name;
        inputValue = inputValue.replace(/,/g, "");
        if (!isNaN(Number(inputValue))) {
            const formattedValue: string = formatPrice(inputValue);
            const numericValue = parseFloat(formattedValue.replace(/,/g, ""));
            formik.setFieldValue(inputName, numericValue);
            setter(formattedValue);
        } else {
        }
    };
    const fetchProvinces = async () => {
        try {
            const accessToken = window.localStorage.getItem("accessToken");
            if (!accessToken) {
                throw new Error("No access token found");
            }
            const headers = { Authorization: `Bearer ${accessToken}` };
            const response = await axios.get(getTinh, { headers });
            setDataProvinces(response.data)

        } catch (error) {
            console.log(error);
        } finally {
        }
    };
    const fetchDistricts = async () => {
        try {

            const accessToken = window.localStorage.getItem("accessToken");
            if (!accessToken) {
                throw new Error("No access token found");
            }
            const headers = { Authorization: `Bearer ${accessToken}` };
            const response = await axios.get(`${getHuyenByTinhID}/${formik.values.tinhID}`, { headers });
            setDataDistricts(response.data)

        } catch (error) {
            console.log(error);
        } finally {
        }
    };
    useEffect(() => {
        fetchProvinces();
    }, []);
    useEffect(() => {
        if (formik.values.tinhID) {
            fetchDistricts()
            if (!defaulValue && formik.values.huyenID !== null) {
                formik.setFieldValue('huyenID', null);
            }
            formik.setTouched({ ...formik.touched, huyenID: false });
        }
    }, [formik.values.tinhID]);
    console.log('đ', filePreviews);

    return (
        <>
            <Dialog
                maxWidth="lg"
                fullWidth
                open={open}
                // onClose={() => handleOpen(false)}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <DialogTitle sx={{ m: 0, p: 3 }} id="customized-dialog-title">
                    <Typography variant="h3">{title}</Typography>
                </DialogTitle>
                <IconButton
                    aria-label="close"
                    onClick={() => onHandleClose()}
                    sx={{
                        position: "absolute",
                        right: 8,
                        top: 8,
                    }}
                >
                    <CloseIcon />
                </IconButton>
                <DialogContent sx={{ p: 3 }}>
                    <form onSubmit={formik.handleSubmit}>
                        <Box
                            display="flex"
                            flexDirection="column"
                            justifyContent="space-between"
                            alignItems="center"
                            gap="12px"
                        >
                            {entityError && (
                                <Grid container spacing={2}>
                                    <Grid item xs={6}>
                                        <Typography
                                            sx={{ mb: 1.5, fontWeight: "bold" }}
                                            className="required_text"
                                        >
                                            {JSON.stringify(entityError)}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            )}
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <Grid container spacing={3} sx={{ mb: 1.5 }}>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                    Tên gói thầu <span className="required_text">(*)</span>{" "}
                                                </Typography>
                                                <TextField
                                                    variant="outlined"
                                                    fullWidth
                                                    id="tenGoiThau"
                                                    name="tenGoiThau"
                                                    value={formik.values.tenGoiThau}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur}
                                                    error={
                                                        formik.touched.tenGoiThau &&
                                                        Boolean(formik.errors.tenGoiThau)
                                                    }
                                                />
                                                {formik.touched.tenGoiThau &&
                                                    Boolean(formik.errors.tenGoiThau) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.tenGoiThau
                                                                ? formik.errors.tenGoiThau.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                    TBMT <span className="required_text">(*)</span>{" "}
                                                </Typography>
                                                <TextField
                                                    variant="outlined"
                                                    fullWidth
                                                    id="tbmt"
                                                    name="tbmt"
                                                    value={formik.values.tbmt}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur}
                                                    error={
                                                        formik.touched.tbmt &&
                                                        Boolean(formik.errors.tbmt)
                                                    }
                                                />
                                                {formik.touched.tbmt &&
                                                    Boolean(formik.errors.tbmt) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.tbmt
                                                                ? formik.errors.tbmt.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3} sx={{ mb: 1.5 }}>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                    Bên mời thầu <span className="required_text">(*)</span>{" "}
                                                </Typography>
                                                <TextField
                                                    variant="outlined"
                                                    fullWidth
                                                    id="benMoiThau"
                                                    name="benMoiThau"
                                                    value={formik.values.benMoiThau}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur}
                                                    error={
                                                        formik.touched.benMoiThau &&
                                                        Boolean(formik.errors.benMoiThau)
                                                    }
                                                />
                                                {formik.touched.benMoiThau &&
                                                    Boolean(formik.errors.benMoiThau) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.benMoiThau
                                                                ? formik.errors.benMoiThau.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }} sx={{ mb: 1.5 }}>
                                                <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>
                                                    <span style={{ marginRight: '8px' }}>Cấp sở ban ngành (CĐT)</span>
                                                    <span className="required_text">(*)</span>
                                                </Typography>
                                                <FormControl fullWidth>
                                                    <Select
                                                        labelId="demo-simple-select-label"
                                                        id="capBaoCaoThauID"
                                                        name="capBaoCaoThauID"
                                                        type="capBaoCaoThauID"
                                                        value={formik.values.capBaoCaoThauID}
                                                        onChange={formik.handleChange}
                                                        onBlur={formik.handleBlur}
                                                        error={
                                                            formik.touched.capBaoCaoThauID &&
                                                            Boolean(formik.errors.capBaoCaoThauID)
                                                        }
                                                    >
                                                        {dataBidReportLevels?.map((item, index) => (
                                                            <MenuItem
                                                                key={index}
                                                                defaultValue={formik.values.capBaoCaoThauID}
                                                                value={item.capBaoCaoThauID}
                                                            >
                                                                {item.tenCapBaoCaoThau}
                                                            </MenuItem>
                                                        ))}
                                                    </Select>
                                                </FormControl>
                                                {formik.touched.capBaoCaoThauID &&
                                                    Boolean(formik.errors.capBaoCaoThauID) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.capBaoCaoThauID
                                                                ? formik.errors.capBaoCaoThauID.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                        <Grid item xs={12} md={12}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                    Chủ đầu tư <span className="required_text">(*)</span>{" "}
                                                </Typography>
                                                <TextField
                                                    variant="outlined"
                                                    fullWidth
                                                    id="chuDauTu"
                                                    name="chuDauTu"
                                                    value={formik.values.chuDauTu}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur}
                                                    error={
                                                        formik.touched.chuDauTu &&
                                                        Boolean(formik.errors.chuDauTu)
                                                    }
                                                />
                                                {formik.touched.chuDauTu &&
                                                    Boolean(formik.errors.chuDauTu) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.chuDauTu
                                                                ? formik.errors.chuDauTu.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3} sx={{ mb: 1.5 }}>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }} sx={{ mb: 1.5 }}>
                                                <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>
                                                    <span style={{ marginRight: '8px' }}>Tỉnh/Thành phố (CĐT)</span>
                                                    <span className="required_text">(*)</span>
                                                </Typography>
                                                <FormControl fullWidth>
                                                    <Select
                                                        labelId="demo-simple-select-label"
                                                        id="tinhID"
                                                        name="tinhID"
                                                        type="tinhID"
                                                        value={formik.values.tinhID}
                                                        onChange={formik.handleChange}
                                                        onBlur={formik.handleBlur}
                                                        error={
                                                            formik.touched.tinhID &&
                                                            Boolean(formik.errors.tinhID)
                                                        }
                                                    >
                                                        {dataProvinces?.map((item, index) => (
                                                            <MenuItem
                                                                key={index}
                                                                defaultValue={formik.values.tinhID}
                                                                value={item.tinhID}
                                                            >
                                                                {item.tenTinh}
                                                            </MenuItem>
                                                        ))}
                                                    </Select>
                                                </FormControl>
                                                {formik.touched.tinhID &&
                                                    Boolean(formik.errors.tinhID) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.tinhID
                                                                ? formik.errors.tinhID.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }} sx={{ mb: 1.5 }}>
                                                <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>
                                                    <span style={{ marginRight: '8px' }}>Quận/Huyện (CĐT)</span>
                                                    <span className="required_text">(*)</span>
                                                </Typography>
                                                <FormControl fullWidth>
                                                    <Select
                                                        labelId="demo-simple-select-label"
                                                        id="huyenID"
                                                        name="huyenID"
                                                        type="huyenID"
                                                        value={formik.values.huyenID}
                                                        onChange={formik.handleChange}
                                                        onBlur={formik.handleBlur}
                                                        error={
                                                            formik.touched.huyenID &&
                                                            Boolean(formik.errors.huyenID)
                                                        }
                                                    >
                                                        {dataDistricts?.map((item, index) => (
                                                            <MenuItem
                                                                key={index}
                                                                defaultValue={formik.values.huyenID}
                                                                value={item.huyenID}
                                                            >
                                                                {item.tenHuyen}
                                                            </MenuItem>
                                                        ))}
                                                        {
                                                            !formik.values?.tinhID &&
                                                            <MenuItem
                                                                key="filterTInhID"
                                                                disabled
                                                            >
                                                                Vui lòng chọn thành phố trước
                                                            </MenuItem>
                                                        }
                                                    </Select>
                                                </FormControl>
                                                {formik.touched.huyenID &&
                                                    Boolean(formik.errors.huyenID) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.huyenID
                                                                ? formik.errors.huyenID.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3} sx={{ mb: 1.5 }}>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                    Thời gian thực hiện hợp đồng <span className="required_text">(*)</span>{" "}
                                                </Typography>
                                                <TextField
                                                    variant="outlined"
                                                    fullWidth
                                                    id="thoiGianThucHienHopDong"
                                                    name="thoiGianThucHienHopDong"
                                                    value={formik.values.thoiGianThucHienHopDong}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur}
                                                    error={
                                                        formik.touched.thoiGianThucHienHopDong &&
                                                        Boolean(formik.errors.thoiGianThucHienHopDong)
                                                    }
                                                    InputProps={{
                                                        endAdornment: (
                                                            <InputAdornment position="end">
                                                                <p style={{ fontWeight: 900, color: "black" }}>
                                                                    Ngày
                                                                </p>
                                                            </InputAdornment>
                                                        ),
                                                    }}
                                                />
                                                {formik.touched.thoiGianThucHienHopDong &&
                                                    Boolean(formik.errors.thoiGianThucHienHopDong) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.thoiGianThucHienHopDong
                                                                ? formik.errors.thoiGianThucHienHopDong.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                    Ngày đăng tải  <span className="required_text">(*)</span>{" "}
                                                </Typography>
                                                <DatePicker
                                                    locale={vi}
                                                    className="react-date-picker"
                                                    selected={ngayDangTai}
                                                    onChange={(date: Date) => {
                                                        setNgayDangTai(date);
                                                        const dateString = date ? format(date, 'dd/MM/yyyy HH:mm:ss') : '';
                                                        formik.setFieldValue("ngayDangTai", dateString);
                                                    }}
                                                    onYearChange={(date: Date) => {
                                                        setNgayDangTai(date);
                                                        const dateString = date ? format(date, 'dd/MM/yyyy HH:mm:ss') : '';
                                                        formik.setFieldValue("ngayDangTai", dateString);
                                                    }}
                                                    onMonthChange={(date: Date) => {
                                                        setNgayDangTai(date);
                                                        const dateString = date ? format(date, 'dd/MM/yyyy HH:mm:ss') : '';
                                                        formik.setFieldValue("ngayDangTai", dateString);

                                                    }}
                                                    customInput={<CustomInput value={ngayDangTai ? ngayDangTai.toLocaleDateString() : ''} onClick={() => { }} />}
                                                    dateFormat="dd/MM/yyyy HH:mm" // Date and time format
                                                    showTimeSelect // Enable time selection
                                                    timeIntervals={1} // Time intervals in minutes
                                                    timeFormat="HH:mm" // Format for time
                                                    timeCaption="Thời gian" // Caption for time selection
                                                    popperPlacement="top-end"
                                                    showPopperArrow={true} // Optionally show arrow on the popper
                                                />
                                                {formik.touched.ngayDangTai &&
                                                    Boolean(formik.errors.ngayDangTai) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.ngayDangTai
                                                                ? formik.errors.ngayDangTai.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3} sx={{ mb: 1.5 }}>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }} >
                                                <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                    Thời gian đóng mở  <span className="required_text">(*)</span>{" "}
                                                </Typography>
                                                <DatePicker
                                                    locale={vi}
                                                    className="react-date-picker"
                                                    selected={thoiGianDongMo}
                                                    onChange={(date: Date) => {
                                                        setThoiGianDongMo(date);
                                                        const dateString = date ? format(date, 'dd/MM/yyyy HH:mm:ss') : '';
                                                        formik.setFieldValue("thoiGianDongMo", dateString);
                                                    }}
                                                    onYearChange={(date: Date) => {
                                                        setThoiGianDongMo(date);
                                                        const dateString = date ? format(date, 'dd/MM/yyyy HH:mm:ss') : '';
                                                        formik.setFieldValue("thoiGianDongMo", dateString);

                                                    }}
                                                    onMonthChange={(date: Date) => {
                                                        setThoiGianDongMo(date);
                                                        const dateString = date ? format(date, 'dd/MM/yyyy HH:mm:ss') : '';
                                                        formik.setFieldValue("thoiGianDongMo", dateString);
                                                    }}
                                                    customInput={<CustomInput value={thoiGianDongMo ? thoiGianDongMo.toLocaleDateString() : ''} onClick={() => { }} />}
                                                    dateFormat="dd/MM/yyyy HH:mm" // Date and time format
                                                    showTimeSelect // Enable time selection
                                                    timeIntervals={1} // Time intervals in minutes
                                                    timeFormat="HH:mm" // Format for time
                                                    timeCaption="Thời gian" // Caption for time selection
                                                    popperPlacement="top-end"
                                                    showPopperArrow={true} // Optionally show arrow on the popper
                                                />
                                                {formik.touched.thoiGianDongMo &&
                                                    Boolean(formik.errors.thoiGianDongMo) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.thoiGianDongMo
                                                                ? formik.errors.thoiGianDongMo.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                    Tiền đảm bảo dự thầu <span className="required_text">(*)</span>{" "}
                                                </Typography>
                                                <TextField
                                                    style={{ width: "100%" }}
                                                    name="tienDamBaoDuThau"
                                                    value={tienDamBaoDuThau}
                                                    error={
                                                        formik.touched.tienDamBaoDuThau &&
                                                        Boolean(formik.errors.tienDamBaoDuThau)
                                                    }
                                                    onChange={(e: ChangeEvent<HTMLInputElement>) =>
                                                        handleChangePrice(e, setTienDamBaoGoiDuThau)
                                                    }
                                                    InputProps={{
                                                        endAdornment: (
                                                            <InputAdornment position="end">
                                                                <p style={{ fontWeight: 900, color: "black" }}>
                                                                    VNĐ
                                                                </p>
                                                            </InputAdornment>
                                                        ),
                                                    }}
                                                />
                                                {formik.touched.tienDamBaoDuThau &&
                                                    Boolean(formik.errors.tienDamBaoDuThau) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.tienDamBaoDuThau
                                                                ? formik.errors.tienDamBaoDuThau.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3} sx={{ mb: 1.5 }}>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                    Nguồn vốn <span className="required_text">(*)</span>{" "}
                                                </Typography>
                                                <TextField
                                                    variant="outlined"
                                                    fullWidth
                                                    id="nguonVon"
                                                    name="nguonVon"
                                                    value={formik.values.nguonVon}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur}
                                                    error={
                                                        formik.touched.nguonVon &&
                                                        Boolean(formik.errors.nguonVon)
                                                    }
                                                />
                                                {formik.touched.nguonVon &&
                                                    Boolean(formik.errors.nguonVon) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.nguonVon
                                                                ? formik.errors.nguonVon.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                    Giá gói thầu <span className="required_text">(*)</span>{" "}
                                                </Typography>
                                                <TextField
                                                    style={{ width: "100%" }}
                                                    name="giaGoiThau"
                                                    value={giaGoiThau}
                                                    error={
                                                        formik.touched.giaGoiThau &&
                                                        Boolean(formik.errors.giaGoiThau)
                                                    }
                                                    onChange={(e: ChangeEvent<HTMLInputElement>) =>
                                                        handleChangePrice(e, setGiaGoiThau)
                                                    }
                                                    InputProps={{
                                                        endAdornment: (
                                                            <InputAdornment position="end">
                                                                <p style={{ fontWeight: 900, color: "black" }}>
                                                                    VNĐ
                                                                </p>
                                                            </InputAdornment>
                                                        ),
                                                    }}
                                                />
                                                {formik.touched.giaGoiThau &&
                                                    Boolean(formik.errors.giaGoiThau) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.giaGoiThau
                                                                ? formik.errors.giaGoiThau.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>

                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3} sx={{ mb: 1.5 }}>
                                        <Grid item xs={12} md={12}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>
                                                    <span style={{ marginRight: '8px' }}>Nhà thầu trúng</span>
                                                    <AddCircleIcon onClick={() => setOpenContractorDialog(true)} sx={{ ml: 1, cursor: 'pointer' }} color="success" />
                                                </Typography>
                                                <Autocomplete
                                                    getOptionLabel={(option) => (option?.maSoThue + ' - ' + option?.tenCongTy)}
                                                    loading={loadingSearch}
                                                    options={dropdownOptions}
                                                    value={contractors ?? null}
                                                    renderOption={(props, option) => (
                                                        <li {...props}>
                                                            <strong>{option.maSoThue}</strong> - {option.tenCongTy} {/* Display both */}
                                                        </li>
                                                    )}
                                                    onChange={(event, value) =>
                                                        handleContractorChange(event, value as Contractors)
                                                    }
                                                    onInputChange={(event, newInputValue, reason) => {
                                                        if (reason === 'reset') {
                                                            setContractors(null);
                                                            // formik.setFieldValue('nhaThauID', null);
                                                            setDropdownOptions([])
                                                            return
                                                        }
                                                    }}
                                                    renderInput={(params) => <TextField onChange={(event) =>
                                                        handleChangeText(event)
                                                    }  {...params} />}
                                                />
                                                {formik.touched.nhaThauID &&
                                                    Boolean(formik.errors.nhaThauID) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.nhaThauID
                                                                ? formik.errors.nhaThauID.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>

                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3} sx={{ mb: 1.5 }}>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }} sx={{ mb: 1.5 }}>
                                                <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>
                                                    <span style={{ marginRight: '8px' }}>Lĩnh vực gói thầu</span>
                                                    <span className="required_text">(*)</span>
                                                </Typography>
                                                <FormControl fullWidth>
                                                    <Select
                                                        labelId="demo-simple-select-label"
                                                        id="loaiBaoCaoThauID"
                                                        name="loaiBaoCaoThauID"
                                                        type="loaiBaoCaoThauID"
                                                        value={formik.values.loaiBaoCaoThauID}
                                                        onChange={formik.handleChange}
                                                        onBlur={formik.handleBlur}
                                                        error={
                                                            formik.touched.loaiBaoCaoThauID &&
                                                            Boolean(formik.errors.loaiBaoCaoThauID)
                                                        }
                                                    >
                                                        {dataBidReportTypes?.map((item, index) => (
                                                            <MenuItem
                                                                key={index}
                                                                defaultValue={formik.values.loaiBaoCaoThauID}
                                                                value={item.loaiBaoCaoThauID}
                                                            >
                                                                {item.tenLoaiBaoCaoThau}
                                                            </MenuItem>
                                                        ))}
                                                    </Select>
                                                </FormControl>
                                                {formik.touched.loaiBaoCaoThauID &&
                                                    Boolean(formik.errors.loaiBaoCaoThauID) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.loaiBaoCaoThauID
                                                                ? formik.errors.loaiBaoCaoThauID.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>
                                                    <span style={{ marginRight: '8px' }}>Nhóm hàng hóa/dịch vụ</span>
                                                </Typography>
                                                <MultipleSelectCheckBoxFieldType
                                                    touched={Boolean(formik.touched.loaiLinhVucs)}
                                                    error={Boolean(formik.errors.loaiLinhVucs)}
                                                    onHandleSelectedFieldTypes={handleSelectedFieldTypes}
                                                    fieldTypes={dataFieldTypes}
                                                    selectedFieldTypes={selectedFieldTypes}
                                                />
                                                {formik.touched.loaiLinhVucs &&
                                                    Boolean(formik.errors.loaiLinhVucs) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.loaiLinhVucs
                                                                ? formik.errors.loaiLinhVucs.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3} sx={{ mb: 1.5 }}>


                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                    Mã định danh
                                                </Typography>
                                                <TextField
                                                    variant="outlined"
                                                    fullWidth
                                                    id="maDinhDanh"
                                                    name="maDinhDanh"
                                                    value={formik.values.maDinhDanh}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur}
                                                    error={
                                                        formik.touched.maDinhDanh &&
                                                        Boolean(formik.errors.maDinhDanh)
                                                    }
                                                />
                                                {formik.touched.maDinhDanh &&
                                                    Boolean(formik.errors.maDinhDanh) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.maDinhDanh
                                                                ? formik.errors.maDinhDanh.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                        <Grid item xs={12} md={6}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                    Người đại diện
                                                </Typography>
                                                <TextField
                                                    variant="outlined"
                                                    fullWidth
                                                    id="nguoiDaiDien"
                                                    name="nguoiDaiDien"
                                                    value={formik.values.nguoiDaiDien}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur}
                                                    error={
                                                        formik.touched.nguoiDaiDien &&
                                                        Boolean(formik.errors.nguoiDaiDien)
                                                    }
                                                />
                                                {formik.touched.nguoiDaiDien &&
                                                    Boolean(formik.errors.nguoiDaiDien) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.nguoiDaiDien
                                                                ? formik.errors.nguoiDaiDien.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>
                                        </Grid>
                                    </Grid>

                                    <Grid container spacing={3} sx={{ mb: 1.5 }}>
                                        <Grid item xs={12} md={12}>
                                            <Box style={{ width: "100%" }}>
                                                <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                    Địa chỉ
                                                </Typography>
                                                <TextField
                                                    variant="outlined"
                                                    fullWidth
                                                    id="diaChi"
                                                    name="diaChi"
                                                    value={formik.values.diaChi}
                                                    onChange={formik.handleChange}
                                                    onBlur={formik.handleBlur}
                                                    error={
                                                        formik.touched.diaChi &&
                                                        Boolean(formik.errors.diaChi)
                                                    }
                                                />
                                                {formik.touched.diaChi &&
                                                    Boolean(formik.errors.diaChi) && (
                                                        <FormHelperText className="required_text">
                                                            {" "}
                                                            {formik.errors.diaChi
                                                                ? formik.errors.diaChi.toString()
                                                                : ""}
                                                        </FormHelperText>
                                                    )}
                                            </Box>

                                        </Grid>
                                    </Grid>
                                    <Grid container spacing={3} sx={{ mb: 1.5 }}>
                                        <Grid item xs={12} md={12}>
                                            <Box style={{ width: "100%" }} sx={{ mt: 2 }}>
                                                {isInsert ? (
                                                    <>
                                                        <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                            Tệp đính kèm
                                                        </Typography>
                                                        <Box sx={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', gap: '10px' }}>
                                                            {filePreviews.map((file, index) => (
                                                                <Box key={index} sx={{ position: 'relative', mb: 1 }}>
                                                                    <Typography variant="body1" component="span" sx={{ cursor: 'pointer' }}>
                                                                        {file.fileName}
                                                                    </Typography>
                                                                    <IconButton
                                                                        color="secondary"
                                                                        onClick={() => handleDeleteFile(index)}
                                                                    >
                                                                        <ClearIcon />
                                                                    </IconButton>
                                                                </Box>
                                                            ))}
                                                        </Box>
                                                        <Button
                                                            variant="contained"
                                                            type="button"
                                                            onClick={handleButtonClickFile}
                                                        >
                                                            Chọn tệp
                                                        </Button>
                                                    </>
                                                ) : (
                                                    <>
                                                        <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                                            Cập nhật tệp đính kèm
                                                        </Typography>
                                                        <Box sx={{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', gap: '10px' }}>
                                                            {filePreviews.map((file, index) => (
                                                                <Box key={index} sx={{ position: 'relative', mb: 1 }}>
                                                                    <Typography variant="body1" component="span" sx={{ cursor: 'pointer' }}>
                                                                        {file.fileName}
                                                                    </Typography>
                                                                    <IconButton>
                                                                        <FileDownloadIcon
                                                                            sx={{ ml: 1, cursor: 'pointer' }}
                                                                            onClick={() => handleDownloadFile(file.link!)}
                                                                        />
                                                                    </IconButton>
                                                                    <IconButton
                                                                        color="secondary"
                                                                        onClick={() => handleDeleteFile(index)}
                                                                    >
                                                                        <ClearIcon />
                                                                    </IconButton>

                                                                </Box>
                                                            ))}
                                                        </Box>
                                                        <Button
                                                            variant="contained"
                                                            type="button"
                                                            onClick={handleButtonClickFile}
                                                        >
                                                            Chọn tệp
                                                        </Button>
                                                    </>
                                                )}
                                                <div>
                                                    <Box>
                                                        <input
                                                            type="file"
                                                            accept=".pdf,.doc,.docx,.xls,.xlsx"
                                                            multiple
                                                            style={{ display: "none" }}
                                                            ref={fileInputRef}
                                                            onChange={handleChangeFile}
                                                        />
                                                    </Box>
                                                </div>

                                                {/* Modal or enlarged view logic can be added similar to the image handling */}
                                            </Box>
                                        </Grid>

                                    </Grid>
                                </Grid>
                            </Grid>
                        </Box>
                    </form>
                    {
                        typeOfCooperations && typeOfCooperations.length > 0 && areaOfOperations && areaOfOperations.length > 0 ? <ContractorsDialog
                            title="Thêm nhà thầu"
                            typeOfCooperations={typeOfCooperations}
                            areaOfOperations={areaOfOperations}
                            defaulValue={null}
                            isInsert
                            handleOpen={setOpenContractorDialog}
                            open={openContractorDialog}
                        /> : <></>
                    }
                </DialogContent>
                <DialogActions sx={{ p: 3 }}>
                    {isInsert && (
                        <LoadingButton
                            sx={{ p: "12px 24px" }}
                            onClick={() => formik.handleSubmit()}
                            type="submit"
                            loading={loading}
                            variant="contained"
                            size="large"
                        >
                            Lưu
                        </LoadingButton>
                    )}
                    {isUpdate && (
                        <LoadingButton
                            sx={{ p: "12px 24px" }}
                            type="submit"
                            onClick={() => formik.handleSubmit()}
                            loading={loading}
                            variant="contained"
                            size="large"
                        >
                            Lưu
                        </LoadingButton>
                    )}
                </DialogActions>
            </Dialog>
        </>
    );
}
