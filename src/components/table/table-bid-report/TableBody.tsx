import * as React from "react";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import { StyledButton } from "../../styled-button";
import {
    Button,
    Chip,
    Icon,
    Rating,
    Tooltip,
    TooltipProps,
    tooltipClasses,
} from "@mui/material";
import { useRouter } from "next/router";
import SnackbarAlert from "../../alert";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
import StyledIconButton from "@/components/styled-button/StyledIconButton";
import ModeEditOutlinedIcon from "@mui/icons-material/ModeEditOutlined";
import DeleteOutlineOutlinedIcon from "@mui/icons-material/DeleteOutlineOutlined";
import RemoveRedEyeTwoToneIcon from "@mui/icons-material/RemoveRedEyeTwoTone";
import { BidReport } from "@/interfaces/bidReport";
import ContractorsDialog from "@/components/dialog/ContractorsDialog";
import useProvince from "@/hooks/useProvince";
import BidReportsDialog from "@/components/dialog/BidReportDialog";
import { green } from "@mui/material/colors";
import { loadCSS } from "fg-loadcss";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import AlertDialog from "@/components/alert/confirmAlertDialog";
import AlertConfirmDialog from "@/components/alert/confirmAlertDialog";
import { toast } from "react-toastify";
import { deleteBidReport } from "@/constant/api";
import axios from "axios";
import QueueIcon from '@mui/icons-material/Queue';
import AddMulStaffsInGroupTelegramDialog from "@/components/dialog/AddMulStaffsInGroupTelegramDialog";
import { FieldType } from "@/interfaces/fieldType";
import { BidReportLevel } from "@/interfaces/bidReportLevel";
import { BidReportType } from "@/interfaces/bidReportType";
import BidReportViewDetailDialog from "@/components/dialog/BidReportViewDetailDialog";
const LightTooltip = styled(({ className, ...props }: TooltipProps) => (
    <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
    [`& .${tooltipClasses.tooltip}`]: {
        backgroundColor: theme.palette.common.white,
        color: "rgba(0, 0, 0, 0.87)",
        boxShadow: "0px 0px 2px 1px rgba(0, 0, 0, 0.2)", // Thêm viền đen
        fontSize: 13,
        maxWidth: 500, // Thiết lập chiều rộng tối đa là 300px
    },
}));
interface BodyDataProps {
    handleView: (e: any) => void;
    handleEdit?: (e: any) => void;
    data: BidReport[];
    page: number;
    rowsPerPage: number;
    editLink?: string;
    viewLink: string;
    isAdmin: boolean;
    dataFieldTypes: FieldType[]
    dataBidReportLevels: BidReportLevel[],
    dataBidReportTypes: BidReportType[],
    fetchData?: () => void;
}
const TableBodyBidReports = (props: BodyDataProps) => {
    const [alertContent, setAlertContent] = React.useState({
        type: "",
        message: "",
    });
    const [openAlert, setOpenAlert] = React.useState(false);
    const [open, setOpen] = React.useState(false);
    const [openAddMulStaffs, setOpenMulStaffs] = React.useState(false);
    const [openConfirmDialog, setOpenConfirmDialog] = React.useState(false);
    const {
        data,
        handleEdit,
        handleView,
        page,
        rowsPerPage,
        editLink,
        viewLink,
        isAdmin,
        fetchData,
        dataFieldTypes,
        dataBidReportLevels,
        dataBidReportTypes
    } = props;
    const [selectedID, setSelectedID] = React.useState<number>();
    const [selectedAddMultipleStaffID, setSelectedAddMultipleStaffID] = React.useState<number>();
    const [selectedDeleteID, setSelectedDeleteID] = React.useState<number>();

    const [openViewItem, setOpenViewItem] = React.useState(false);
    const handleViewItem = (
        e: React.MouseEventHandler<HTMLTableRowElement> | undefined,
        id: any
    ) => {
        setOpenViewItem(true);
        setSelectedID(id);
    };

    const handleDeleteItem = (
        e: React.MouseEventHandler<HTMLTableRowElement> | undefined,
        id: any
    ) => {
        setOpenConfirmDialog(true);
        setSelectedDeleteID(id);
    };
    const handleConfirmDeleteItem = async () => {
        // if (selectedDeleteID) deleteBidReports(selectedDeleteID);
        let respone = await axios.delete(deleteBidReport, { params: { id: selectedDeleteID } });
        if (respone.status === 200) {
            fetchData!()
        }
        setSelectedDeleteID(undefined);
        setOpenConfirmDialog(false);
        toast.success("Xóa dữ liệu thành công", {});
    };
    const handleEditItem = (
        e: React.MouseEventHandler<HTMLTableRowElement> | undefined,
        id: any
    ) => {
        setSelectedID(id);
        setOpen(true);
    };
    const handleAddMulStaffs = (
        e: React.MouseEventHandler<HTMLTableRowElement> | undefined,
        id: any
    ) => {
        setSelectedAddMultipleStaffID(id);
        setOpenMulStaffs(true);
    };
    const handleViewBidReport = (
        e: React.MouseEventHandler<HTMLTableRowElement> | undefined,
        id: any
    ) => {
        setOpenViewItem(true);
        setSelectedID(id);
    };
    return (
        <TableBody>
            {data?.map((row: BidReport, index: any) => (
                <StyledTableRow
                    hover
                    role="checkbox"
                    tabIndex={-1}
                    key={row.baoCaoThauID}
                    sx={{ cursor: "pointer" }}
                >
                    <StyledTableCell padding="normal">
                        {page > 0 ? page * rowsPerPage + index + 1 : index + 1}
                    </StyledTableCell>
                    <StyledTableCell align="left">
                        <LightTooltip
                            title={row.tbmt ? row.tbmt : "Chưa có dữ liệu"}
                        >
                            <span>{row.tbmt ? row.tbmt : "Chưa có dữ liệu"}</span>
                        </LightTooltip>
                    </StyledTableCell>
                    <StyledTableCell align="left">
                        <LightTooltip
                            title={row.tenGoiThau ? row.tenGoiThau : "Chưa có dữ liệu"}
                        >
                            <span>{row.tenGoiThau ? row.tenGoiThau : "Chưa có dữ liệu"}</span>
                        </LightTooltip>
                    </StyledTableCell>
                    <StyledTableCell align="left">
                        <LightTooltip
                            title={row.chuDauTu ? row.chuDauTu : "Chưa có dữ liệu"}
                        >
                            <span>{row.chuDauTu ? row.chuDauTu : "Chưa có dữ liệu"}</span>
                        </LightTooltip>
                    </StyledTableCell>
                    <StyledTableCell align="left">
                        <LightTooltip
                            title={row.benMoiThau ? row.benMoiThau : "Chưa có dữ liệu"}
                        >
                            <span>{row.benMoiThau ? row.benMoiThau : "Chưa có dữ liệu"}</span>
                        </LightTooltip>
                    </StyledTableCell>
                    <StyledTableCell align="left">
                        <LightTooltip
                            title={row.nhaThau ? row?.nhaThau?.tenCongTy : "Chưa có dữ liệu"}
                        >
                            <span>{row.nhaThau ? row?.nhaThau.tenCongTy : "Chưa có dữ liệu"}</span>
                        </LightTooltip>
                    </StyledTableCell>
                    <StyledTableCell align="left">
                        <LightTooltip
                            title={row.thoiGianDongMo ? row.thoiGianDongMo : "Chưa có dữ liệu"}
                        >
                            <span>{row.thoiGianDongMo ? row.thoiGianDongMo : "Chưa có dữ liệu"}</span>
                        </LightTooltip>
                    </StyledTableCell>
                    <StyledTableCell align="left">
                        <LightTooltip
                            title={row.giaGoiThau ? row.giaGoiThau.toLocaleString() : "Chưa có dữ liệu"}
                        >
                            <span>{row.giaGoiThau ? row.giaGoiThau.toLocaleString() : "Chưa có dữ liệu"}</span>
                        </LightTooltip>
                    </StyledTableCell>
                    <StyledTableCell align="center">
                        <Box display='flex' gap={2} alignItems='center' justifyContent='center'>
                            {isAdmin &&
                                <Box display='flex' gap={2} alignItems='center' justifyContent='center' zIndex={3}>
                                    <StyledIconButton
                                        variant="contained"
                                        color="dark"
                                        onClick={(e: any) => handleViewBidReport(e, row.baoCaoThauID)}
                                    >
                                        <RemoveRedEyeTwoToneIcon />
                                    </StyledIconButton>
                                    <StyledIconButton
                                        variant="contained"
                                        color="primary"
                                        onClick={(e: any) => handleEditItem(e, row.baoCaoThauID)}
                                    >
                                        <ModeEditOutlinedIcon />
                                    </StyledIconButton>
                                    <StyledIconButton
                                        variant="contained"
                                        color="secondary"
                                        onClick={(e: any) => handleDeleteItem(e, row.baoCaoThauID)}
                                    >
                                        <DeleteOutlineOutlinedIcon />
                                    </StyledIconButton>
                                </Box>
                            }
                        </Box>
                    </StyledTableCell>
                </StyledTableRow>
            ))}
            {alertContent && (
                <SnackbarAlert
                    message={alertContent.message}
                    type={alertContent.type}
                    setOpenAlert={setOpenAlert}
                    openAlert={openAlert}
                />
            )}
            {data.length === 0 && (
                <StyledTableRow style={{ height: 83 }}>
                    <StyledTableCell align="center" colSpan={6}>
                        Chưa có dữ liệu
                    </StyledTableCell>
                </StyledTableRow>
            )}
            {open === true && selectedID && (
                <BidReportsDialog
                    dataBidReportLevels={dataBidReportLevels}
                    dataBidReportTypes={dataBidReportTypes}
                    dataFieldTypes={dataFieldTypes}
                    title="Cập nhật báo cáo thầu"
                    fetchData={fetchData}
                    defaulValue={data.find((item) => item.baoCaoThauID === selectedID)}
                    handleOpen={setOpen}
                    open={open}
                    isUpdate
                />
            )}
            {openConfirmDialog && (
                <AlertConfirmDialog
                    title="Xác nhận xóa dữ liệu?"
                    message="Dữ liệu đã xóa thì không khôi phục được"
                    onHandleConfirm={handleConfirmDeleteItem}
                    openConfirm={openConfirmDialog}
                    handleOpenConfirmDialog={setOpenConfirmDialog}
                />
            )}
            {openViewItem === true && selectedID && (
                <BidReportViewDetailDialog
                    title="Thông tin chi tiết"
                    defaulValue={data.find((item) => item.baoCaoThauID === selectedID)}
                    handleOpen={setOpenViewItem}
                    open={openViewItem}
                    isUpdate
                />
            )}
        </TableBody>
    );
};
export default TableBodyBidReports;

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    "&:last-child td, &:last-child th": {
        border: 0,
    },
}));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.common.white,
        border: 0,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
        paddingTop: "24px",
        paddingBottom: "24px",
    },
}));
