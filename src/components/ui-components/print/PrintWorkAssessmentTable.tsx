import { PlanEvaluation } from "@/interfaces/plan";
import { useState, useEffect } from 'react';
import {
    Autocomplete,
    Box,
    Button,
    Checkbox,
    Divider,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    IconButton,
    InputAdornment,
    InputLabel,
    ListItemText,
    MenuItem,
    OutlinedInput,
    Paper,
    Select,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    Typography,
    useMediaQuery,
    useTheme
} from '@mui/material';
import * as Yup from 'yup';
import { Formik } from 'formik';
import AnimateButton from '@/components/button/AnimateButton';
import { toast } from 'react-toastify';
import { CustomInput } from '@/components/input';
import dayjs, { Dayjs } from 'dayjs';
import 'dayjs/locale/vi';
dayjs.locale('vi');
import { PlanMonth, PlanWeek } from '@/interfaces/plan';
import usePlanWeek from '@/hooks/usePlanWeek';
import usePlanMonth from '@/hooks/usePlanMonth';
import useRoleLocalStorage from '@/hooks/useRoleLocalStorage';
import dynamic from "next/dynamic";
import "react-quill/dist/quill.snow.css";
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import useStaff from '@/hooks/useStaff';
import { format } from 'date-fns';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { tableCellClasses } from '@mui/material';
import { styled } from '@mui/system';
const StyledTableCell1 = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.background.paper,
        color: theme.palette.text.primary,
        borderBottom: `1px solid ${theme.palette.text.secondary}`,
        paddingTop: '9px',
        paddingBottom: '18px',
        fontFamily: "Times New Roman",
        fontSize: "14px"
    },
    [`&.${tableCellClasses.body}`]: {
        paddingTop: '18px',
        paddingBottom: '18px',
        fontFamily: "Times New Roman",
        fontSize: "14px",
        border: '1px solid black !important'
    },
}));
interface Props {
    defaulValue?: any,

}
export default function PrintWorkAssessmentTable(props: Props) {
    const theme = useTheme()

    return (
        <>
            <Grid sx={{ mt: 2, mb: -1 }} item xs={12}>
                {/* Sử dụng TableCotainer để bao quanh bảng, giúp hỗ trợ cuộn nếu cần */}
                <TableContainer>
                    <Table sx={{ border: 1, borderColor: 'black', borderRadius: "8px" }} aria-label="customized table">
                        <TableHead>
                            <TableRow>
                                {/* Merge "STT" cell across 2 rows */}
                                <StyledTableCell1
                                    width="5%"
                                    align="center"
                                    sx={{ border: '1px solid black !important' }} // Set border color to black
                                    rowSpan={2}
                                >
                                    STT
                                </StyledTableCell1>

                                {/* Merge "Nội dung" cell across 2 rows */}
                                <StyledTableCell1
                                    align="left"
                                    width="20%"
                                    sx={{ border: '1px solid black !important' }} // Set border color to black
                                    rowSpan={2}
                                >
                                    Nội dung
                                </StyledTableCell1>

                                {/* Merge "Nhân sự" cells across 2 columns */}
                                <StyledTableCell1
                                    align="center"
                                    width="20%"
                                    colSpan={2}
                                    sx={{ border: '1px solid black !important' }} // Set border color to black
                                >
                                    Nhân sự
                                </StyledTableCell1>

                                {/* Merge "QL đánh giá" cells across 3 columns */}
                                <StyledTableCell1
                                    align="center"
                                    width="10%"
                                    rowSpan={2}
                                    sx={{ border: '1px solid black !important' }} // Set border color to black
                                >
                                    QL đánh giá
                                </StyledTableCell1>

                                {/* Conditionally render "Nguyên nhân" column */}
                                {(
                                    (props?.defaulValue?.khcT_DanhGias?.some((item: any) => item.trangThai === false) ||
                                        props?.defaulValue?.khcT_DanhGias?.some((item: any) => item.trangThai === 'false') ||
                                        props?.defaulValue?.khcT_DanhGias?.some((item: any) => item.trangThaiQuanLy === false) ||
                                        props?.defaulValue?.khcT_DanhGias?.some((item: any) => item.trangThaiQuanLy === 'false')) && (
                                        <StyledTableCell1
                                            align="left"
                                            width="25%"
                                            sx={{ border: '1px solid black !important' }} // Set border color to black
                                            rowSpan={2}
                                        >
                                            Nguyên nhân
                                        </StyledTableCell1>
                                    )
                                )}
                            </TableRow>

                            {/* Second row to define the columns under "QL đánh giá" */}
                            <TableRow>
                                <StyledTableCell1
                                    align="center"
                                    width="10%"
                                    sx={{ border: '1px solid black !important' }} // Set border color to black
                                >
                                    Đánh giá (%)
                                </StyledTableCell1>

                                <StyledTableCell1
                                    align="center"
                                    sx={{ border: '1px solid black !important' }} // Set border color to black
                                >
                                    Trạng thái
                                </StyledTableCell1>
                            </TableRow>
                        </TableHead>
                        <TableBody style={{
                            border: '1px solid black !important', // Set border color to black

                        }}>
                            {props?.defaulValue?.khcT_DanhGias?.map((item: any, index: any) => (
                                <TableRow key={index}>
                                    {/* STT */}
                                    <StyledTableCell1
                                        align="center"
                                        style={{
                                            border: '1px solid black !important', // Set border color to black
                                            textAlign: 'center',
                                            verticalAlign: 'middle',
                                        }}
                                    >
                                        {index + 1}
                                    </StyledTableCell1>

                                    {/* Nội dung */}
                                    <StyledTableCell1
                                        style={{
                                            border: '1px solid black !important', // Set border color to black
                                            borderBottom: '1px solid black !important',
                                            color: 'black'
                                        }}
                                    >
                                        {item.noiDung}
                                    </StyledTableCell1>

                                    {/* Điểm đánh giá */}
                                    <StyledTableCell1
                                        align="center"
                                        sx={{
                                            p: 2,
                                            border: '1px solid black !important', // Set border color to black
                                            backgroundColor: 'white',
                                            color: 'black'
                                        }}
                                    >
                                        {item.diemTuDanhGia}
                                    </StyledTableCell1>

                                    {/* Trạng thái đánh giá */}
                                    <StyledTableCell1
                                        align="center"
                                        sx={{
                                            p: 2,
                                            border: '1px solid black !important', // Set border color to black
                                            backgroundColor: 'white',
                                            color: 'black'
                                        }}
                                    >
                                        {item.trangThai === null
                                            ? 'Chưa xác định'
                                            : item.trangThai === true
                                                ? 'Đạt'
                                                : item.trangThai === false
                                                    ? 'Chưa đạt'
                                                    : 'Chưa xác định'}
                                    </StyledTableCell1>

                                    {/* Trạng thái quản lý */}
                                    <StyledTableCell1
                                        align="center"
                                        sx={{
                                            p: 2,
                                            border: '1px solid black !important', // Set border color to black
                                            backgroundColor: 'white',
                                            color: 'black'
                                        }}
                                    >
                                        {item.trangThaiQuanLy === null
                                            ? 'Chưa xác định'
                                            : item.trangThaiQuanLy === true
                                                ? 'Đạt'
                                                : item.trangThaiQuanLy === false
                                                    ? 'Chưa đạt'
                                                    : 'Chưa xác định'}
                                    </StyledTableCell1>

                                    {/* Nguyên nhân thất bại */}
                                    {(
                                        item.trangThai === false ||
                                        item.trangThai === 'false' ||
                                        item.trangThaiQuanLy === false ||
                                        item.trangThaiQuanLy === 'false'
                                    ) ? (
                                        <StyledTableCell1
                                            align="left"
                                            sx={{
                                                p: 2,
                                                border: '1px solid black !important', // Set border color to black
                                                backgroundColor: 'white',
                                                color: 'black'
                                            }}
                                        >
                                            {item.nguyenNhanThatBai}
                                        </StyledTableCell1>
                                    ) : (
                                        <></>
                                    )}
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
        </>
    );
}
