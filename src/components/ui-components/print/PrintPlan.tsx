import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import { forwardRef, ReactNode, useEffect, useMemo, useRef, useState } from 'react';
import { Avatar, Button, Checkbox, Chip, ClickAwayListener, Fade, Grid, IconButton, Paper, Popper, PopperPlacementType, Switch, TableHead, TableRow, Typography, styled } from '@mui/material';
import { IconChevronRight, IconDotsVertical, IconEye, IconInfoSquare, IconPencil, IconPrinter } from '@tabler/icons-react';
import { useRouter } from 'next/router';
import EnhancedTableToolbar from '@/components/table/table-custom/TableTool';
import { StyledTableCell } from '@/components/table/table-custom/TableCell';
import TableCustomizePagination from '@/components/table/TablePagination';
import { Order, PropsTable } from '@/components/table/table-custom/type';
import { PlanMonth, PlanQuarter, PlanWeek } from '@/interfaces/plan';
import { StyledButton } from '@/components/styled-button';
import CustomDialog from '@/components/dialog/CustomDialog';
import usePlanQuarter from '@/hooks/usePlanQuarter';
import { toast } from 'react-toastify';
import useRoleLocalStorage from '@/hooks/useRoleLocalStorage';
import ReasonForRejectionDialog from '@/components/dialog/ReasonForRejectionDialog';
import AlertConfirmDialog from '@/components/alert/confirmAlertDialog';
import WorkAssessmentTable from '@/components/ui-components/WorkAssessmentTable';
import ReactToPrint from 'react-to-print';
import useStaff from '@/hooks/useStaff';
import { Staff } from '@/interfaces/user';
const TypographyPrint = styled(Typography)(() => ({
    fontFamily: "Times New Roman",
    fontSize: "17px"
}))
interface Props {
    // Define any props if necessary
    staff: Staff,
    title: string;
    content: ReactNode;
}

const PrintPlan = forwardRef<HTMLDivElement, Props>(({ staff, title, content }, ref) => {
    return (
        <Grid ref={ref} container spacing={2} padding="1cm" >
            <Grid item sm={4}>
                <TypographyPrint textAlign="center" variant='h4' textTransform={"uppercase"}>
                    {staff?.lstChucVuView?.[0]?.lstCongTy?.tenCongTy}
                </TypographyPrint>
            </Grid>
            <Grid item sm={8}>
                <TypographyPrint textAlign="center" variant='h4' align="center" textTransform={"uppercase"}>CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM</TypographyPrint>
                <TypographyPrint textAlign="center" variant='h4' align="center">Độc lập - Tự do - Hạnh phúc</TypographyPrint>
            </Grid>
            <Grid item sm={12}>
                <TypographyPrint fontFamily="Times New Roman" textTransform={"uppercase"} textAlign="center" py={2} variant='h4'>{title}</TypographyPrint>
            </Grid>
            {content}
        </Grid>
    );
});

export default PrintPlan;
