import { PlanEvaluation } from "@/interfaces/plan";
import { useState, useEffect } from 'react';
import { useTheme } from '@mui/material/styles';
import {
    Autocomplete,
    Box,
    Button,
    Checkbox,
    Divider,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    IconButton,
    InputAdornment,
    InputLabel,
    ListItemText,
    MenuItem,
    OutlinedInput,
    Paper,
    Select,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    Typography,
    useMediaQuery
} from '@mui/material';
import * as Yup from 'yup';
import { Formik } from 'formik';
import AnimateButton from '@/components/button/AnimateButton';
import { toast } from 'react-toastify';
import { CustomInput } from '@/components/input';
import dayjs, { Dayjs } from 'dayjs';
import 'dayjs/locale/vi';
dayjs.locale('vi');
import { PlanMonth, PlanWeek } from '@/interfaces/plan';
import usePlanWeek from '@/hooks/usePlanWeek';
import usePlanMonth from '@/hooks/usePlanMonth';
import useRoleLocalStorage from '@/hooks/useRoleLocalStorage';
import dynamic from "next/dynamic";
import "react-quill/dist/quill.snow.css";
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import useStaff from '@/hooks/useStaff';
import { format } from 'date-fns';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

interface Props {
    defaulValue?: any,

}
export default function WorkArisesTable(props: Props) {

    return (
        <>
            <Grid sx={{ mt: 2 }} item xs={12}>
                {/* Sử dụng TableContainer để bao quanh bảng, giúp hỗ trợ cuộn nếu cần */}
                <TableContainer component={Paper}>
                    <Table>
                        <TableHead sx={{ backgroundColor: '#2c8bf2' }}>
                            <TableRow>
                                <TableCell width="5%" align="center" sx={{ color: 'white' }}>STT</TableCell>
                                <TableCell align="left" width="35%" sx={{ color: 'white' }}>Nội dung</TableCell>
                                <TableCell align="center" width="20%" sx={{ color: 'white' }} colSpan={2}>Nhân sự</TableCell>

                                <TableCell align="center" width="10%" sx={{ color: 'white' }}>QL đánh giá</TableCell>
                                {(
                                    (props?.defaulValue?.khcT_CongViecPhatSinhs?.some((item: any) => item.trangThai === false) ||
                                        props?.defaulValue?.khcT_CongViecPhatSinhs?.some((item: any) => item.trangThai === 'false') ||
                                        props?.defaulValue?.khcT_CongViecPhatSinhs?.some((item: any) => item.trangThaiQuanLy === false) ||
                                        props?.defaulValue?.khcT_CongViecPhatSinhs?.some((item: any) => item.trangThaiQuanLy === 'false'))
                                    && (
                                        <TableCell align="left" width="25%" sx={{ color: 'white' }}>
                                            Nguyên nhân
                                        </TableCell>
                                    )
                                )}
                                <TableCell align="center" width="12%" sx={{ color: 'white' }}>Thao tác</TableCell>
                            </TableRow>
                            {
                                <TableRow>
                                    <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                    <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                    <TableCell align="center" width="10%" sx={{ color: 'white' }}>Đánh giá (%)</TableCell>
                                    {/* <TableCell align="center" width="10%" sx={{ color: 'white' }}>Điểm quản lý</TableCell> */}
                                    <TableCell align="center" sx={{ color: 'white' }}>Trạng thái</TableCell>
                                    <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                    {(
                                        (props?.defaulValue?.khcT_CongViecPhatSinhs?.some((item: any) => item.trangThai === false) ||
                                            props?.defaulValue?.khcT_CongViecPhatSinhs?.some((item: any) => item.trangThai === 'false') ||
                                            props?.defaulValue?.khcT_CongViecPhatSinhs?.some((item: any) => item.trangThaiQuanLy === false) ||
                                            props?.defaulValue?.khcT_CongViecPhatSinhs?.some((item: any) => item.trangThaiQuanLy === 'false')) && (
                                            <TableCell align="center" sx={{ color: 'white' }}>
                                                {/* Nội dung ở đây */}
                                            </TableCell>
                                        )
                                    )}
                                    <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                </TableRow>
                            }
                        </TableHead>
                        <TableBody>
                            {props?.defaulValue?.khcT_CongViecPhatSinhs?.map((item: any, index: any) => (
                                <TableRow key={index} >
                                    {/* Nội dung */}
                                    <TableCell
                                        align="center"
                                        style={{ textAlign: 'center', verticalAlign: 'middle' }}
                                    >
                                        {index + 1}
                                    </TableCell>
                                    <TableCell  >
                                        {item.noiDung}
                                    </TableCell>
                                    {/* Điểm đánh giá */}
                                    <TableCell align="center" sx={{ p: 2 }}>
                                        {item.diemTuDanhGia}
                                    </TableCell>
                                    <TableCell align="center" sx={{ p: 2 }}>
                                        {item.trangThai === null ? "Chưa xác định" : item.trangThai === true ? "Đạt" : item.trangThai === false ? "Chưa đạt" : "Chưa xác định"}
                                    </TableCell>
                                    <TableCell align="center" sx={{ p: 2 }}>
                                        {item.trangThaiQuanLy === null ? "Chưa xác định" : item.trangThaiQuanLy === true ? "Đạt" : item.trangThaiQuanLy === false ? "Chưa đạt" : "Chưa xác định"}
                                    </TableCell>
                                    {((item.trangThai === false || (item.trangThai === 'false')) || (item.trangThaiQuanLy === false || (item.trangThaiQuanLy === 'false'))) ?
                                        <TableCell align="left" sx={{ p: 2 }}>
                                            {item.nguyenNhanThatBai}
                                        </TableCell> :
                                        <>
                                            {(
                                                (props?.defaulValue?.khcT_CongViecPhatSinhs?.some((item: any) => item.trangThai === false) ||
                                                    props?.defaulValue?.khcT_CongViecPhatSinhs?.some((item: any) => item.trangThai === 'false') ||
                                                    props?.defaulValue?.khcT_CongViecPhatSinhs?.some((item: any) => item.trangThaiQuanLy === false) ||
                                                    props?.defaulValue?.khcT_CongViecPhatSinhs?.some((item: any) => item.trangThaiQuanLy === 'false')) && (
                                                    <TableCell align="center" sx={{ color: 'white' }}>
                                                        {/* Nội dung ở đây */}
                                                    </TableCell>
                                                )
                                            )}
                                        </>
                                    }
                                    <TableCell align="center">

                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
        </>
    );
}
