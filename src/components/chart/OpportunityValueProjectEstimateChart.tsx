import * as React from 'react';
import { BarChart } from '@mui/x-charts/BarChart';
import { ContractorEstimate } from '@/interfaces/contractorEstimate';
import { Staff } from '@/interfaces/user';
import { ProjectEstimate } from '@/interfaces/projectEstimate';
import { ReportProjectInteract } from '@/interfaces/reportProjectInteract';

const chartSetting = {
    height: 500,
};

const valueFormatterX = (value: number | null) => `${value?.toLocaleString()} Đ`;
const valueFormatterY = (value: number | null) => {
    if (value === null || value === undefined) return '0';
    if (value >= 1_000_000_000) {
        return `${(value / 1_000_000_000).toFixed(1)}B`;
    } else if (value >= 1_000_000) {
        return `${(value / 1_000_000).toFixed(1)}M `;
    } else if (value >= 1_000) {
        return `${(value / 1_000).toFixed(1)}K`;
    }
    return `${value.toLocaleString()} Đ`;
};
const generateTicks = (min: number, max: number, numTicks: number) => {
    const step = (max - min) / (numTicks - 1);
    return Array.from({ length: numTicks }, (_, i) => min + step * i);
};
export interface Props {
    dataReportProjectInteract?: ReportProjectInteract[],
    dataStaffs: Staff[],
    width?: number,
    filterResult: string | number | null
}

export default function OpportunityValueProjectEstimateChart({ dataReportProjectInteract, dataStaffs, filterResult }: Props) {
    const [dataset, setDataset] = React.useState([
        {
            nguoiTao: 0,
            nguoiVanHanh: 0,
            name: 'Tất cả',
        },
    ]);
    const convertToAbbreviation = (fullName: string) => {
        const nameParts = fullName.split(' '); // Tách tên thành mảng các phần
        const lastName = nameParts[0]; // Họ
        const firstName = nameParts[nameParts.length - 1]; // Tên cuối
        const middleName = nameParts.slice(1, -1).join(' '); // Tên đệm (nếu có)

        // Lấy chữ cái đầu tiên của họ và tên đệm, nếu có
        const abbreviatedMiddleName = middleName ? middleName.split(' ').map(word => word.charAt(0).toUpperCase()).join('.') + '.' : '';

        // Ghép lại họ + tên viết tắt + tên cuối
        return `${lastName.charAt(0).toUpperCase()}.${abbreviatedMiddleName}${firstName}`;
    };
    React.useEffect(() => {
        if (dataReportProjectInteract && dataReportProjectInteract.length > 0 && dataStaffs && dataStaffs.length > 0) {
            let updatedDataList: any = [];

            // Lọc theo filterResult
            const filterData = (item: any) => {
                if (filterResult === "null") {
                    return item.ketQua === null;
                } else if (filterResult === "true") {
                    return item.ketQua === true;
                } else if (filterResult === "false") {
                    return item.ketQua === false;
                }
                return true; // Trả về tất cả nếu filterResult không phải "null", "true", "false"
            };

            dataStaffs.forEach((staff) => {
                let nguoiTao = 0;
                let nguoiVanHanh = 0;

                dataReportProjectInteract.forEach((item) => {
                    // Kiểm tra điều kiện lọc
                    if (filterData(item)) {
                        // Lấy nhanVienID cho nguoiTao
                        const nhanVienTaoID = item?.nguoiTao?.nhanVienID || item?.nhanVien?.nhanVienID;

                        // Cộng doanh thu dự kiến cho nguoiTao
                        if (nhanVienTaoID === staff.nhanVienID) {
                            nguoiTao += item.doanhThuDuKien || 0;
                        }

                        // Lấy nhanVienID cho nguoiVanHanh
                        const nhanVienVanHanhID = item?.nguoiVanHanh?.nhanVienID;

                        // Cộng doanh thu dự kiến cho nguoiVanHanh
                        if (nhanVienVanHanhID === staff.nhanVienID) {
                            nguoiVanHanh += item.doanhThuDuKien || 0;
                        }
                    }
                });
                const name = convertToAbbreviation(staff.tenNhanVien);
                updatedDataList.push({
                    nguoiTao,
                    nguoiVanHanh,
                    name,
                });
            });
            // Sắp xếp danh sách theo nguoiTao giảm dần
            updatedDataList.sort((a: any, b: any) => b.nguoiTao - a.nguoiTao);
            setDataset(updatedDataList);
        }
    }, [filterResult, dataReportProjectInteract, dataStaffs]);

    return (
        <BarChart
            dataset={dataset.slice(0, 8)}
            yAxis={[
                {
                    scaleType: 'linear',
                    valueFormatter: valueFormatterY,
                    tickNumber: 10,
                },
            ]}
            xAxis={[{ scaleType: 'band', dataKey: 'name' }]}
            series={[
                { dataKey: 'nguoiTao', label: 'Tạo dự án', valueFormatter: valueFormatterX, color: '#0CC0F1' },
                { dataKey: 'nguoiVanHanh', label: 'Vận hành dự án', valueFormatter: valueFormatterX, color: '#0e6325' },
            ]}
            {...chartSetting}
            slotProps={{
                bar: {
                    clipPath: `inset(0px round 4px 4px 0px 0px)`,
                },
                popper: {
                    sx: {
                        borderRadius: '20px',
                        '& .MuiChartsTooltip-root': {
                            '& .MuiTypography-root': {
                                color: 'black',
                            },
                        },
                    },
                },
            }}
        />
    );
}
