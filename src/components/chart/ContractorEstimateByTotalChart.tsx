import * as React from 'react';
import { BarChart } from '@mui/x-charts/BarChart';
import { axisClasses } from '@mui/x-charts/ChartsAxis';
import { ContractorEstimate } from '@/interfaces/contractorEstimate';
import { Staff } from '@/interfaces/user';

const chartSetting = {
    height: 500,

};


const valueFormatterX = (value: number | null) => `${value?.toLocaleString()} Đ`;
const valueFormatterY = (value: number | null) => {
    if (value === null || value === undefined) return '0';
    if (value >= 1_000_000_000) {
        return `${(value / 1_000_000_000).toFixed(1)}B`;
    } else if (value >= 1_000_000) {
        return `${(value / 1_000_000).toFixed(1)}M `;
    } else if (value >= 1_000) {
        return `${(value / 1_000).toFixed(1)}K`;
    }
    return `${value.toLocaleString()} Đ`;
};
export interface Props {
    filterModify: number,
    dataContractorEstimate?: ContractorEstimate[],
    dataStaffs: Staff[],
    filterContractorEstimateStatusID: number;
    filterResult: string | number | null;
}

export default function ContractorEstimateByTotalChart({ filterModify, dataContractorEstimate, dataStaffs, filterContractorEstimateStatusID, filterResult }: Props) {
    const [dataset, setDataset] = React.useState([
        {
            tong: 0,
            duKien: 0,
            thanhCong: 0,
            thatBai: 0,
            name: 'Tất cả',
        },
    ]);

    React.useEffect(() => {
        if (dataContractorEstimate && dataContractorEstimate.length > 0) {
            // First filter based on the statusID (filterContractorEstimateStatusID)
            let filteredData = filterContractorEstimateStatusID === 0
                ? dataContractorEstimate
                : dataContractorEstimate.filter((item) => item.trangThaiID === filterContractorEstimateStatusID);

            // Then filter based on the filterResult (if provided)
            if (filterResult !== undefined) {
                filteredData = filteredData.filter((item) => {
                    // Check if filterResult is a string representing boolean or null
                    if (filterResult === "true") {
                        return item.ketQua === true;
                    } else if (filterResult === "false") {
                        return item.ketQua === false;
                    } else if (filterResult === "null") {
                        return item.ketQua === null;
                    } else if (filterResult === 0) {
                        // No filtering for ketQua if filterResult is 0
                        return true;
                    }
                    return true; // If filterResult is null or undefined, no further filtering is applied.
                });
            }
            let tong = 0;
            let duKien = 0;
            let thanhCong = 0;
            let thatBai = 0;
            filteredData.forEach((item) => {
                tong += item.doanhThuDuKien;
                if (item.ketQua === true) {
                    thanhCong += item.doanhThuDuKien;
                } else if (item.ketQua === false) {
                    thatBai += item.doanhThuDuKien;
                }
            });

            const name = "Tổng tiền";
            duKien = tong - thanhCong - thatBai;
            const updatedData = [
                {
                    tong,
                    duKien,
                    thanhCong,
                    thatBai,
                    name,
                },
            ];
            setDataset(updatedData);
        }
    }, [dataContractorEstimate, filterContractorEstimateStatusID, filterResult]);
    console.log('total', dataset);
    console.log('total', dataContractorEstimate);

    return (
        <BarChart
            dataset={dataset}
            yAxis={[
                {
                    scaleType: 'linear',
                    valueFormatter: valueFormatterY,
                    tickNumber: 5
                },
            ]}
            xAxis={[{ scaleType: 'band', dataKey: 'name' }]}
            series={[
                { dataKey: 'tong', label: 'Tổng', valueFormatter: valueFormatterX, color: '#ccc' },
                { dataKey: 'duKien', label: 'Dự kiến', valueFormatter: valueFormatterX, color: '#2196f3' },
                { dataKey: 'thanhCong', label: 'Thành công', valueFormatter: valueFormatterX, color: '#4caf50' },
                { dataKey: 'thatBai', label: 'Thất bại', valueFormatter: valueFormatterX, color: '#ff3d00' },
            ]}
            {...chartSetting}
            slotProps={{
                bar: {
                    clipPath: `inset(0px round 4px 4px 0px 0px)`,
                },
                popper: {
                    sx: {
                        borderRadius: '20px',
                        '& .MuiChartsTooltip-root': {
                            '& .MuiTypography-root': {
                                color: 'black',
                            },
                        },
                    },
                },
            }}
        />
    );
}
