import * as React from 'react';
import { BarChart } from '@mui/x-charts/BarChart';
import { ContractorEstimate } from '@/interfaces/contractorEstimate';
import { Staff } from '@/interfaces/user';
import { ProjectEstimate } from '@/interfaces/projectEstimate';
import { ReportProjectInteract } from '@/interfaces/reportProjectInteract';
import { Paper } from '@mui/material';

const chartSetting = {
    height: 500,
};

const valueFormatterX = (value: number | null) => `${value?.toLocaleString()} Đ`;
const valueFormatterY = (value: number | null) => {
    if (value === null || value === undefined) return '0';
    if (value >= 1_000_000_000) {
        return `${(value / 1_000_000_000).toFixed(1)}B`;
    } else if (value >= 1_000_000) {
        return `${(value / 1_000_000).toFixed(1)}M `;
    } else if (value >= 1_000) {
        return `${(value / 1_000).toFixed(1)}K`;
    }
    return `${value.toLocaleString()} Đ`;
};
const generateTicks = (min: number, max: number, numTicks: number) => {
    const step = (max - min) / (numTicks - 1);
    return Array.from({ length: numTicks }, (_, i) => min + step * i);
};
export interface Props {
    dataContractorEstimate?: ContractorEstimate[],
    dataStaffs: Staff[],
    width?: number,
    filterResult: string | number | null
    filterStaff: number | null
}

export default function OpportunityValueContractor({ dataContractorEstimate, dataStaffs, filterResult, filterStaff }: Props) {
    const [dataset, setDataset] = React.useState([
        {
            nguoiTao: 0,
            nguoiVanHanh: 0,
            duKienTao: 0,
            thanhCongTao: 0,
            thatBaiTao: 0,
            duKienVanHanh: 0,
            thanhCongVanHanh: 0,
            thatBaiVanHanh: 0,
            name: 'Tất cả',
        },
    ]);

    const convertToAbbreviation = (fullName: string) => {
        const nameParts = fullName.split(' '); // Tách tên thành mảng các phần
        const lastName = nameParts[0]; // Họ
        const firstName = nameParts[nameParts.length - 1]; // Tên cuối
        const middleName = nameParts.slice(1, -1).join(' '); // Tên đệm (nếu có)

        // Lấy chữ cái đầu tiên của họ và tên đệm, nếu có
        const abbreviatedMiddleName = middleName ? middleName.split(' ').map(word => word.charAt(0).toUpperCase()).join('.') + '.' : '';

        // Ghép lại họ + tên viết tắt + tên cuối
        return `${lastName.charAt(0).toUpperCase()}.${abbreviatedMiddleName}${firstName}`;
    };
    React.useEffect(() => {
        if (dataContractorEstimate && dataContractorEstimate?.length > 0 && dataStaffs?.length > 0) {
            let updatedDataList: any = [];

            // Hàm lọc dữ liệu theo filterResult
            const filterData = (item: any) => {
                if (filterResult === "null") {
                    return item.ketQua === null;
                } else if (filterResult === "true") {
                    return item.ketQua === true;
                } else if (filterResult === "false") {
                    return item.ketQua === false;
                }
                return true; // Trả về tất cả nếu không có filterResult hợp lệ
            };
            // Nếu filterStaff không có giá trị -> xử lý tất cả nhân viên
            if (!filterStaff) {
                dataStaffs.forEach((staff) => {
                    let nguoiTao = 0;
                    let nguoiVanHanh = 0;
                    let duKienTao = 0, thanhCongTao = 0, thatBaiTao = 0;
                    let duKienVanHanh = 0, thanhCongVanHanh = 0, thatBaiVanHanh = 0;
                    // Lặp qua dataContractorEstimate để tính doanh thu
                    dataContractorEstimate.forEach((item) => {
                        if (filterData(item)) {
                            const nhanVienTaoID = item?.nguoiTao?.nhanVienID || item?.nhanVien?.nhanVienID;
                            const nhanVienVanHanhID = item?.nguoiVanHanh?.nhanVienID;
                            if (nhanVienTaoID === staff.nhanVienID) {
                                nguoiTao += item.doanhThuDuKien || 0;
                                if (item.ketQua === null) duKienTao += item.doanhThuDuKien || 0;
                                if (item.ketQua === true) thanhCongTao += item.doanhThuDuKien || 0;
                                if (item.ketQua === false) thatBaiTao += item.doanhThuDuKien || 0;
                            }
                            if (nhanVienVanHanhID === staff.nhanVienID) {
                                nguoiVanHanh += item.doanhThuDuKien || 0;
                                if (item.ketQua === null) duKienVanHanh += item.doanhThuDuKien || 0;
                                if (item.ketQua === true) thanhCongVanHanh += item.doanhThuDuKien || 0;
                                if (item.ketQua === false) thatBaiVanHanh += item.doanhThuDuKien || 0;
                            }
                        }
                    });

                    updatedDataList.push({
                        nguoiTao,
                        nguoiVanHanh,
                        duKienTao,
                        thanhCongTao,
                        thatBaiTao,
                        duKienVanHanh,
                        thanhCongVanHanh,
                        thatBaiVanHanh,
                        name: convertToAbbreviation(staff.tenNhanVien),
                    });
                });
            } else {
                // Xử lý riêng cho nhân viên được chỉ định (filterStaff)
                let nguoiTao = 0;
                let nguoiVanHanh = 0;
                let duKienTao = 0, thanhCongTao = 0, thatBaiTao = 0;
                let duKienVanHanh = 0, thanhCongVanHanh = 0, thatBaiVanHanh = 0;
                const tenNhanVien = dataStaffs.find((x) => x.nhanVienID === filterStaff)?.tenNhanVien ?? "";

                dataContractorEstimate.forEach((item) => {
                    if (filterData(item)) {
                        const nhanVienTaoID = item?.nguoiTao?.nhanVienID || item?.nhanVien?.nhanVienID;
                        const nhanVienVanHanhID = item?.nguoiVanHanh?.nhanVienID;

                        if (nhanVienTaoID === filterStaff) {
                            nguoiTao += item.doanhThuDuKien || 0;
                            if (item.ketQua === null) duKienTao += item.doanhThuDuKien || 0;
                            if (item.ketQua === true) thanhCongTao += item.doanhThuDuKien || 0;
                            if (item.ketQua === false) thatBaiTao += item.doanhThuDuKien || 0;
                        }
                        if (nhanVienVanHanhID === filterStaff) {
                            nguoiVanHanh += item.doanhThuDuKien || 0;
                            if (item.ketQua === null) duKienTao += item.doanhThuDuKien || 0;
                            if (item.ketQua === true) thanhCongTao += item.doanhThuDuKien || 0;
                            if (item.ketQua === false) thatBaiTao += item.doanhThuDuKien || 0;
                        }
                    }
                });
                // Thêm kết quả vào danh sách
                updatedDataList.push({
                    nguoiTao,
                    nguoiVanHanh,
                    duKienTao,
                    thanhCongTao,
                    thatBaiTao,
                    duKienVanHanh,
                    thanhCongVanHanh,
                    thatBaiVanHanh,
                    name: convertToAbbreviation(tenNhanVien),
                });
            }

            // Sắp xếp danh sách theo nguoiTao giảm dần và cập nhật dataset
            updatedDataList.sort((a: any, b: any) => b.nguoiTao - a.nguoiTao);
            setDataset(updatedDataList);
        }
    }, [filterResult, filterStaff, dataContractorEstimate, dataStaffs]);

    const CustomItemTooltipContent = (props: any) => {
        const { itemData, series } = props;
        console.log(series);

        return (
            <Paper sx={{ padding: 2, }}>
                <div><strong>{series.label}</strong></div>
                {
                    series.dataKey === "nguoiTao" && <>
                        <div><strong>Tổng:</strong> {series?.data[itemData?.dataIndex].toLocaleString()} Đ</div>
                        <div><strong>Dự kiến:</strong> {dataset[itemData?.dataIndex].duKienTao.toLocaleString()} Đ</div>
                        <div><strong>Thành công:</strong> {dataset[itemData?.dataIndex].thanhCongTao.toLocaleString()} Đ</div>
                        <div><strong>Thất bại:</strong> {dataset[itemData?.dataIndex].thatBaiTao.toLocaleString()} Đ</div>
                    </>
                }
                {
                    series.dataKey === "nguoiVanHanh" && <>
                        <div><strong>Tổng:</strong> {series?.data[itemData?.dataIndex].toLocaleString()} Đ</div>
                        <div><strong>Dự kiến:</strong> {dataset[itemData?.dataIndex].duKienVanHanh.toLocaleString()} Đ</div>
                        <div><strong>Thành công:</strong> {dataset[itemData?.dataIndex].thanhCongVanHanh.toLocaleString()} Đ</div>
                        <div><strong>Thất bại:</strong> {dataset[itemData?.dataIndex].thatBaiVanHanh.toLocaleString()} Đ</div>
                    </>
                }

            </Paper>
        );
    };
    console.log('ádasdasdasdas', dataset);

    return (
        <BarChart
            dataset={dataset.slice(0, 8)}
            yAxis={[
                {
                    scaleType: 'linear',
                    valueFormatter: valueFormatterY,
                    tickNumber: 10,
                },
            ]}
            xAxis={[{ scaleType: 'band', dataKey: 'name' }]}
            series={[
                { dataKey: 'nguoiTao', label: 'Tạo đơn hàng', valueFormatter: valueFormatterX, color: '#0CC0F1', },
                { dataKey: 'nguoiVanHanh', label: 'Vận hành đơn hàng', valueFormatter: valueFormatterX, color: ' #0e6325' },
            ]}
            {...chartSetting}
            tooltip={{ trigger: "item", itemContent: CustomItemTooltipContent }}
            slotProps={{
                bar: {
                    clipPath: `inset(0px round 4px 4px 0px 0px)`,
                },
                popper: {
                    sx: {
                        borderRadius: '20px',
                        '& .MuiChartsTooltip-root': {
                            '& .MuiTypography-root': {
                                color: 'black',
                            },
                        },
                    },
                },
            }}
        />
    );
}
