import * as React from 'react';
import { Theme, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Chip from '@mui/material/Chip';
import { Checkbox, ListItemText } from '@mui/material';
import { FieldType } from '@/interfaces/fieldType';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const names = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder',
];

function getStyles(name: string, personName: readonly string[], theme: Theme) {
    return {
        fontWeight:
            personName.indexOf(name) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightBold,
    };
}
interface Props {
    onHandleSelectedFieldTypes: (data: FieldType[]) => void;
    fieldTypes: FieldType[];
    selectedFieldTypes: FieldType[];
    error?: boolean;
    touched?: boolean;
}
export default function MultipleSelectCheckBoxFieldType({ onHandleSelectedFieldTypes, fieldTypes, error, touched, selectedFieldTypes }: Props) {
    const theme = useTheme();
    const [FieldTypeName, setFieldTypeName] = React.useState<string[]>([]);
    const FieldTypesChild = fieldTypes.map(fieldType => fieldType.tenLoaiLinhVuc);
    React.useEffect(() => {
        if (selectedFieldTypes && selectedFieldTypes.length > 0) {
            const selectedFieldTypeNames = selectedFieldTypes.map(FieldType => FieldType.tenLoaiLinhVuc);
            setFieldTypeName(selectedFieldTypeNames);
        }
        return () => {
            setFieldTypeName([]);
        };
    }, [selectedFieldTypes]);
    const handleChange = (event: SelectChangeEvent<typeof FieldTypeName>) => {
        const {
            target: { value },
        } = event;
        setFieldTypeName(
            // On autofill we get a stringified value.
            typeof value === 'string' ? value.split(',') : value,
        );
        const foundFieldTypes = fieldTypes
            .filter(fieldType => value.includes(fieldType.tenLoaiLinhVuc))
        onHandleSelectedFieldTypes(foundFieldTypes);

    };

    return (
        <div>

            <FormControl sx={{ width: '100%' }}>
                <Select
                    id="demo-multiple-chip"
                    multiple
                    error={error && touched}
                    value={FieldTypeName}
                    onChange={handleChange}
                    input={<OutlinedInput id="select-multiple-chip" />}
                    renderValue={(selected) => (
                        <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                            {selected.map((value) => (
                                <Chip key={value} label={value} />
                            ))}
                        </Box>
                    )}
                    MenuProps={MenuProps}
                >
                    {FieldTypesChild.map((name) => (
                        <MenuItem key={name} value={name}>
                            <Checkbox checked={FieldTypeName.indexOf(name) > -1} />
                            <ListItemText primary={name} />
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
        </div>
    );
}