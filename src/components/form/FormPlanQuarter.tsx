import { useState, useEffect } from 'react';
import { useTheme } from '@mui/material/styles';
import {
    Autocomplete,
    Box,
    Button,
    Checkbox,
    Divider,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    IconButton,
    InputAdornment,
    InputLabel,
    ListItemText,
    MenuItem,
    OutlinedInput,
    Paper,
    Select,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    Typography,
    useMediaQuery
} from '@mui/material';
import * as Yup from 'yup';
import { Formik } from 'formik';

import { strengthColor, strengthIndicator } from '@/utils/passwordStrength';
import AnimateButton from '@/components/button/AnimateButton';
import useAuth from '@/hooks/useAuth';
import useRole from '@/hooks/useRole';
import { toast } from 'react-toastify';
import { CustomInput } from '@/components/input';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import dayjs, { Dayjs } from 'dayjs';
import 'dayjs/locale/vi';
dayjs.locale('vi');
import { PlanQuarter } from '@/interfaces/plan';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import usePlanQuarter from '@/hooks/usePlanQuarter';
import useRoleLocalStorage from '@/hooks/useRoleLocalStorage';
import dynamic from "next/dynamic";
import "react-quill/dist/quill.snow.css";
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import useStaff from '@/hooks/useStaff';

const QuillEditor = dynamic(() => import("react-quill"), { ssr: false });

interface Props {
    title?: string,
    defaulValue?: PlanQuarter,
    handleOpenAlert?: (e: boolean) => void,
    open?: boolean,
    id?: number,
    handleOpen?: (e: boolean) => void,
    handleEdit?: (e: any) => void;
    handleDelete?: (e: any) => void;
    isEdit?: boolean;
    buttonActionText: string
}
export default function FormPlanQuarter(props: Props) {
    const theme = useTheme();
    const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));
    const { updatePlanQuarter, addPlanQuarter, dataPlanQuarter } = usePlanQuarter()
    const { dataStaff } = useStaff()
    const { userID, userName, dataStaffDepartment } = useRoleLocalStorage()
    const [content, setContent] = useState("");
    const quillModules = {
        toolbar: [
            [{ header: [1, 2, 3, false] }],
            ["bold", "italic", "underline", "strike", "blockquote"],
            [{ list: "ordered" }, { list: "bullet" }],
            ["link", "image"],
            [{ align: [] }],
            [{ color: [] }],
            ["code-block"],
            ["clean"],
        ],
    };
    const quillFormats = [
        "header",
        "bold",
        "italic",
        "underline",
        "strike",
        "blockquote",
        "list",
        "bullet",
        "link",
        "image",
        "align",
        "color",
        "code-block",
    ];

    const onSubmit = async (values: any, { setErrors, setStatus, setSubmitting, resetForm }: any) => {
        const year = Number(values?.nam.format("YYYY"));
        values.nam = year;
        // Lấy quý từ `values.quy`
        const quarter = Number(values?.quy);
        console.log('nè', year);
        // Xác định tháng bắt đầu và kết thúc của quý
        let startMonth: number;
        let endMonth: number;

        switch (quarter) {
            case 1:
                startMonth = 1; // Tháng 1
                endMonth = 3;   // Tháng 3
                break;
            case 2:
                startMonth = 4; // Tháng 4
                endMonth = 6;   // Tháng 6
                break;
            case 3:
                startMonth = 7; // Tháng 7
                endMonth = 9;   // Tháng 9
                break;
            case 4:
                startMonth = 10; // Tháng 10
                endMonth = 12;   // Tháng 12
                break;
            default:
                startMonth = 1;
                endMonth = 3;
                break;
        }
        // Tính toán ngày bắt đầu và kết thúc của quý
        values.tuNgay = dayjs(`${year}-${startMonth}-01`).startOf('month').format("DD/MM/YYYY HH:mm:ss").toString();
        values.denNgay = dayjs(`${year}-${endMonth}-01`).endOf('month').format("DD/MM/YYYY HH:mm:ss").toString();

        values.nguoiDuyet = dataStaff?.find(item => item.nhanVienID === values.nguoiDuyetID)?.tenNhanVien;
        if (Array.isArray(values.khcT_DanhGias)) {
            values.khcT_DanhGias = values.khcT_DanhGias.map((item: any) => ({
                ...item,
                trangThai: (item.trangThai === "true" || item.trangThai === true)
                    ? true
                    : (item.trangThai === "false" || item.trangThai === false)
                        ? false
                        : null, // Chuyển đổi `trangThai` từ chuỗi thành boolean hoặc null
                trangThaiQuanLy: (item.trangThaiQuanLy === "true" || item.trangThaiQuanLy === true)
                    ? true
                    : (item.trangThaiQuanLy === "false" || item.trangThaiQuanLy === false)
                        ? false
                        : null,
            }));
        }
        if (Array.isArray(values.khcT_CongViecPhatSinhs)) {
            values.khcT_CongViecPhatSinhs = values.khcT_CongViecPhatSinhs.map((item: any) => ({
                ...item,
                trangThai: (item.trangThai === "true" || item.trangThai === true)
                    ? true
                    : (item.trangThai === "false" || item.trangThai === false)
                        ? false
                        : null, // Chuyển đổi `trangThai` từ chuỗi thành boolean hoặc null
                trangThaiQuanLy: (item.trangThaiQuanLy === "true" || item.trangThaiQuanLy === true)
                    ? true
                    : (item.trangThaiQuanLy === "false" || item.trangThaiQuanLy === false)
                        ? false
                        : null,
            }));
        }
        if (props.isEdit) {
            values.quyID = props.defaulValue?.quyID;
            const rs = await updatePlanQuarter(values)

            if (rs) {
                toast.success('Cập nhật thành công')
                if (props.handleOpen) {
                    props.handleOpen(false)
                }
                setStatus({ success: true });
                setSubmitting(false);
            }
            else {
                setStatus({ success: false });
                setSubmitting(false);
                toast.error('Cập nhật thất bại')
            }
        }
        else {
            const rs = await addPlanQuarter(values)
            if (rs) {
                if (props?.handleOpen) {
                    props?.handleOpen(false)
                }
                toast.success('Thêm thành công')
                resetForm()
                setStatus({ success: true });
                setSubmitting(false);
            }
            else {
                setStatus({ success: false });
                setSubmitting(false);
                toast.error('Thêm thất bại')
            }
        }
    };
    useEffect(() => {
        if (props?.defaulValue) {
            setContent(props.defaulValue.noiDung);
        }
    }, [props?.defaulValue])
    return (
        <Formik
            initialValues={{
                noiDung: props?.defaulValue?.noiDung ?? "",
                tieuDe: props?.defaulValue?.tieuDe ?? "",
                quy: props?.defaulValue?.quy ? props?.defaulValue?.quy : 0,
                nam: props?.defaulValue?.nam ? dayjs().year(props?.defaulValue?.nam) : dayjs().add(1, 'hour'),
                nguoiDuyetID: props?.defaulValue?.nguoiDuyetID ?? 0,
                nguoiDuyet: props?.defaulValue?.nguoiDuyet ?? "",
                lyDoTuChoi: props?.defaulValue?.lyDoTuChoi ?? null,
                khcT_DanhGias: props?.defaulValue?.khcT_DanhGias ?? [
                    { noiDung: "", diemTuDanhGia: 0, diemQuanLy: 0, trangThai: null, nguyenNhanThatBai: null, trangThaiQuanLy: null }
                ],
                khcT_CongViecPhatSinhs: props?.defaulValue?.khcT_CongViecPhatSinhs ?? [
                ]
            }}
            validationSchema={Yup.object().shape({
                noiDung: Yup.string().required('Không được bỏ trống'),
                quy: Yup.number().min(1, 'Vui lòng chọn quý'),
                nam: Yup.date().required('Năm không được bỏ trống'),
                nguoiDuyetID: Yup.number().required('Không được bỏ trống'),
                tieuDe: Yup.string().required('Không được bỏ trống'),
                khcT_DanhGias: Yup.array().of(
                    Yup.object().shape({
                        noiDung: Yup.string().required('Nội dung không được bỏ trống'),
                        diemTuDanhGia: Yup.number().min(0, 'Điểm phải lớn hơn hoặc bằng 0').required('Điểm đánh giá không được bỏ trống'),
                        diemQuanLy: Yup.number().min(0, 'Điểm phải lớn hơn hoặc bằng 0').required('Điểm quản lý không được bỏ trống'),

                    })
                ).min(1, 'Cần có ít nhất một mục trong bảng'),
                khcT_CongViecPhatSinhs: Yup.array().of(
                    Yup.object().shape({
                        noiDung: Yup.string().required('Nội dung không được bỏ trống'),
                        diemTuDanhGia: Yup.number().min(0, 'Điểm phải lớn hơn hoặc bằng 0').required('Điểm đánh giá không được bỏ trống'),
                        diemQuanLy: Yup.number().min(0, 'Điểm phải lớn hơn hoặc bằng 0').required('Điểm quản lý không được bỏ trống'),

                    })
                )
            })}
            onSubmit={onSubmit}
        >
            {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values, setFieldValue }) => (
                <form noValidate onSubmit={handleSubmit} style={{ width: '100%' }}>
                    <Box display='flex' flexDirection='column' gap={2}>
                        <Grid container spacing={matchDownSM ? 1 : 2}>
                            <Grid item xs={12} sm={12}>
                                <FormControl fullWidth error={Boolean(touched.tieuDe && errors.tieuDe)}>
                                    <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                        Tiêu đề {''}
                                        <span className="required_text">(*)</span>
                                    </Typography>
                                    <CustomInput
                                        id="outlined-adornment-tieuDe"
                                        type="text"
                                        value={values?.tieuDe}
                                        name="tieuDe"
                                        disabled={props?.defaulValue?.isApprove || (props?.isEdit === true && props.defaulValue?.nguoiTaoID !== userID)}
                                        placeholder='Tiêu đề'
                                        onBlur={handleBlur}
                                        onChange={(e) => {
                                            setFieldValue('tieuDe', e.target.value);
                                        }}
                                    />
                                    {touched.tieuDe && errors.tieuDe && (
                                        <FormHelperText error id="helper-text-tieuDe">{errors.tieuDe.toString()}</FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>

                            <Grid item xs={12} sm={12} sx={{ mb: 4 }}>
                                <FormControl fullWidth error={Boolean(touched.noiDung && errors.noiDung)}>
                                    <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                        Nội dung chính {''}
                                        <span className="required_text">(*)</span>
                                    </Typography>
                                    {/* <CustomInput
                                        id="outlined-adornment-noiDung"
                                        type="text"
                                        value={values?.noiDung}
                                        name="noiDung"
                                        placeholder='Nội dung'
                                        onBlur={handleBlur}
                                        onChange={(e) => {
                                            setFieldValue('noiDung', e.target.value);
                                        }}
                                        multiline
                                        rows={5}
                                    /> */}
                                    <QuillEditor
                                        value={content}
                                        onChange={(newContent) => {
                                            setContent(newContent);
                                            setFieldValue("noiDung", newContent);
                                        }}
                                        readOnly={props?.defaulValue?.isApprove || (props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                        modules={quillModules}
                                        formats={quillFormats}
                                        style={{ height: "100%", width: "100%", paddingBottom: "32px" }}
                                        className="w-full h-full mt-10 bg-white"
                                    />
                                    {touched.noiDung && errors.noiDung && (
                                        <FormHelperText error id="helper-text-noiDung">{errors.noiDung.toString()}</FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <FormControl fullWidth error={Boolean(touched.quy && errors.quy)}>
                                    <Typography sx={{ mt: 1.5, mb: 1.5, fontWeight: "bold" }}>
                                        Quý {''}
                                        <span className="required_text">(*)</span>
                                    </Typography>
                                    <Select
                                        name="quy"
                                        value={values?.quy}
                                        onChange={(e) => setFieldValue('quy', e.target.value)}
                                        disabled={props?.defaulValue?.isApprove || (props?.isEdit === true && props.defaulValue?.nguoiTaoID !== userID)}
                                        displayEmpty
                                        fullWidth
                                    >
                                        <MenuItem value={0} disabled>
                                            Chọn quý
                                        </MenuItem>
                                        <MenuItem value={1}>Quý 1</MenuItem>
                                        <MenuItem value={2}>Quý 2</MenuItem>
                                        <MenuItem value={3}>Quý 3</MenuItem>
                                        <MenuItem value={4}>Quý 4</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <FormControl fullWidth error={Boolean(touched.nam && errors.nam)}>
                                    <Typography sx={{ mt: 1.5, mb: 1.5, fontWeight: "bold" }}>
                                        Năm {''}
                                        <span className="required_text">(*)</span>
                                    </Typography>
                                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                                        <DatePicker
                                            name='nam'
                                            sx={{ width: '100%' }}
                                            value={dayjs(values?.nam)}
                                            onChange={(value) => {
                                                setFieldValue('nam', value);
                                            }}
                                            disabled={props?.defaulValue?.isApprove || (props?.isEdit === true && props.defaulValue?.nguoiTaoID !== userID)}
                                            openTo="year"
                                            views={['year']}
                                            format="YYYY"
                                        />
                                    </LocalizationProvider>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <FormControl fullWidth error={Boolean(touched.nguoiDuyetID && errors.nguoiDuyetID)}>
                                    <Typography sx={{ mb: 1.5, fontWeight: 'bold' }}>
                                        Người duyệt kế hoạch {''}
                                        <span className="required_text">(*)</span>
                                    </Typography>
                                    <Autocomplete
                                        id="nguoiDuyetID"
                                        options={dataStaff || []}
                                        getOptionLabel={(option) => option.tenNhanVien} // Hiển thị tên nhân viên trong dropdown
                                        value={dataStaff.find((item: any) => item.nhanVienID === values.nguoiDuyetID) || null}
                                        onChange={(event, newValue) => {
                                            setFieldValue('nguoiDuyetID', newValue ? newValue.nhanVienID : null);
                                        }}
                                        disabled={props?.defaulValue?.isApprove || (props?.isEdit === true && props.defaulValue?.nguoiTaoID !== userID)}
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                error={Boolean(touched.nguoiDuyetID && errors.nguoiDuyetID)}
                                                helperText={touched.nguoiDuyetID && errors.nguoiDuyetID}
                                            />
                                        )}
                                    />
                                </FormControl>
                            </Grid>
                            {
                                values.nguoiDuyetID ?

                                    <>
                                        <Grid item xs={12}>
                                            <Typography variant="h5" sx={{ mb: 2 }}>Danh sách công việc
                                                {
                                                    !(props.isEdit && props.defaulValue?.nguoiTaoID !== userID) && <IconButton
                                                        onClick={() => {
                                                            setFieldValue('khcT_DanhGias', [...values.khcT_DanhGias, { noiDung: '', diemTuDanhGia: 0, diemQuanLy: 0 }]);
                                                        }}
                                                        color="primary"
                                                    >
                                                        <AddCircleOutlineIcon />
                                                    </IconButton>
                                                }
                                            </Typography>
                                            {/* Sử dụng TableContainer để bao quanh bảng, giúp hỗ trợ cuộn nếu cần */}
                                            <TableContainer component={Paper}>
                                                <Table>
                                                    <TableHead sx={{ backgroundColor: '#2c8bf2' }}>
                                                        <TableRow>
                                                            <TableCell width="5%" align="center" sx={{ color: 'white' }}>STT</TableCell>
                                                            <TableCell align="center" width="35%" sx={{ color: 'white' }}>Nội dung</TableCell>
                                                            {
                                                                props?.isEdit && <TableCell align="center" width="20%" sx={{ color: 'white' }} colSpan={2}>Nhân sự</TableCell>
                                                            }

                                                            {/* <TableCell align="center" width="10%" sx={{ color: 'white' }}>Điểm quản lý</TableCell> */}
                                                            {/* /{props?.isEdit && <TableCell align="center" width="10%" sx={{ color: 'white' }}>Trạng thái</TableCell>} */}
                                                            {props?.isEdit && <TableCell align="center" width="10%" sx={{ color: 'white' }}>QL đánh giá</TableCell>}
                                                            {props?.isEdit && (
                                                                (values?.khcT_DanhGias?.some(item => item.trangThai === false) ||
                                                                    values?.khcT_DanhGias?.some(item => item.trangThai === 'false') ||
                                                                    values?.khcT_DanhGias?.some(item => item.trangThaiQuanLy === false) ||
                                                                    values?.khcT_DanhGias?.some(item => item.trangThaiQuanLy === 'false'))
                                                                && (
                                                                    <TableCell align="center" width="25%" sx={{ color: 'white' }}>
                                                                        Nguyên nhân
                                                                    </TableCell>
                                                                )
                                                            )}
                                                            <TableCell align="center" width="12%" sx={{ color: 'white' }}>Thao tác</TableCell>
                                                        </TableRow>
                                                        {
                                                            props.isEdit && <TableRow>
                                                                <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                                                <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                                                <TableCell align="center" width="10%" sx={{ color: 'white' }}>Đánh giá (%)</TableCell>
                                                                {/* <TableCell align="center" width="10%" sx={{ color: 'white' }}>Điểm quản lý</TableCell> */}
                                                                {props?.isEdit && <TableCell align="center" sx={{ color: 'white' }}>Trạng thái</TableCell>}
                                                                {props?.isEdit && <TableCell align="center" sx={{ color: 'white' }}></TableCell>}
                                                                {props?.isEdit && (
                                                                    (values?.khcT_DanhGias?.some(item => item.trangThai === false) ||
                                                                        values?.khcT_DanhGias?.some(item => item.trangThai === 'false') ||
                                                                        values?.khcT_DanhGias?.some(item => item.trangThaiQuanLy === false) ||
                                                                        values?.khcT_DanhGias?.some(item => item.trangThaiQuanLy === 'false')) && (
                                                                        <TableCell align="center" sx={{ color: 'white' }}>
                                                                            {/* Nội dung ở đây */}
                                                                        </TableCell>
                                                                    )
                                                                )}
                                                                <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                                            </TableRow>
                                                        }
                                                    </TableHead>
                                                    <TableBody>
                                                        {values?.khcT_DanhGias?.map((item, index) => (
                                                            <TableRow key={index} >
                                                                {/* Nội dung */}
                                                                <TableCell
                                                                    align="center"
                                                                    style={{ textAlign: 'center', verticalAlign: 'middle' }}
                                                                >
                                                                    {index + 1}
                                                                </TableCell>
                                                                <TableCell  >
                                                                    <TextField
                                                                        type="text"
                                                                        name={`khcT_DanhGias[${index}].noiDung`}
                                                                        value={item.noiDung}
                                                                        onChange={handleChange}
                                                                        onBlur={handleBlur}
                                                                        placeholder="Nội dung"
                                                                        disabled={props?.defaulValue?.isApprove || (props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                                                        fullWidth
                                                                        multiline
                                                                        minRows={4}
                                                                        error={touched.khcT_DanhGias?.[index]?.noiDung && Boolean((errors.khcT_DanhGias as any)?.[index]?.noiDung)}
                                                                        helperText={touched.khcT_DanhGias?.[index]?.noiDung && (errors.khcT_DanhGias as any)?.[index]?.noiDung}
                                                                    />
                                                                </TableCell>
                                                                {/* Điểm đánh giá */}
                                                                {
                                                                    props?.isEdit && <TableCell align="center" sx={{ p: 2 }}>
                                                                        <CustomInput
                                                                            type="number"
                                                                            name={`khcT_DanhGias[${index}].diemTuDanhGia`}
                                                                            value={item.diemTuDanhGia}
                                                                            onChange={handleChange}
                                                                            onBlur={handleBlur}
                                                                            placeholder="Điểm đánh giá"
                                                                            disabled={userID !== props?.defaulValue?.nguoiTaoID}
                                                                        />
                                                                        {touched.khcT_DanhGias?.[index]?.diemTuDanhGia && (errors.khcT_DanhGias as any)?.[index]?.diemTuDanhGia && (
                                                                            <FormHelperText error>{(errors.khcT_DanhGias as any)?.[index]?.diemTuDanhGia}</FormHelperText>
                                                                        )}
                                                                    </TableCell>
                                                                }
                                                                {
                                                                    props?.isEdit ?
                                                                        <TableCell align="left" sx={{ p: 2 }}>
                                                                            <FormControl fullWidth>
                                                                                <Select
                                                                                    labelId={`trangThai-label-${index}`}
                                                                                    name={`khcT_DanhGias[${index}].trangThai`}
                                                                                    value={item.trangThai ?? "null"}
                                                                                    onChange={handleChange}
                                                                                    onBlur={handleBlur}
                                                                                    disabled={userID !== props?.defaulValue?.nguoiTaoID}
                                                                                >
                                                                                    <MenuItem value="null">Chưa xác định</MenuItem>
                                                                                    <MenuItem value="true">Đạt</MenuItem>
                                                                                    <MenuItem value="false">Chưa đạt</MenuItem>
                                                                                </Select>
                                                                                {touched.khcT_DanhGias?.[index]?.trangThai && (errors.khcT_DanhGias as any)?.[index]?.trangThai && (
                                                                                    <FormHelperText error>{(errors.khcT_DanhGias as any)?.[index]?.trangThai}</FormHelperText>
                                                                                )}
                                                                            </FormControl>
                                                                        </TableCell> :
                                                                        <></>
                                                                }
                                                                {
                                                                    props?.isEdit ?
                                                                        <TableCell align="left" sx={{ p: 2 }}>
                                                                            <FormControl fullWidth>
                                                                                <Select
                                                                                    labelId={`trangThaiQuanLy-label-${index}`}
                                                                                    name={`khcT_DanhGias[${index}].trangThaiQuanLy`}
                                                                                    value={item.trangThaiQuanLy ?? "null"}
                                                                                    onChange={handleChange}
                                                                                    onBlur={handleBlur}
                                                                                    disabled={userID !== props?.defaulValue?.nguoiDuyetID}
                                                                                >
                                                                                    <MenuItem value="null">Chưa xác định</MenuItem>
                                                                                    <MenuItem value="true">Đạt</MenuItem>
                                                                                    <MenuItem value="false">Chưa đạt</MenuItem>
                                                                                </Select>
                                                                                {touched.khcT_DanhGias?.[index]?.trangThaiQuanLy && (errors.khcT_DanhGias as any)?.[index]?.trangThaiQuanLy && (
                                                                                    <FormHelperText error>{(errors.khcT_DanhGias as any)?.[index]?.trangThaiQuanLy}</FormHelperText>
                                                                                )}
                                                                            </FormControl>
                                                                        </TableCell> :
                                                                        <></>
                                                                }
                                                                {props?.isEdit && ((item.trangThai === false || (item.trangThai === 'false')) || (item.trangThaiQuanLy === false || (item.trangThaiQuanLy === 'false'))) ?
                                                                    <TableCell align="left" sx={{ p: 2 }}>
                                                                        <TextField
                                                                            type="text"
                                                                            name={`khcT_DanhGias[${index}].nguyenNhanThatBai`}
                                                                            value={item.nguyenNhanThatBai}
                                                                            onChange={handleChange}
                                                                            onBlur={handleBlur}
                                                                            placeholder="Nguyên nhân"
                                                                            disabled={(props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                                                            fullWidth
                                                                            multiline
                                                                            minRows={4}
                                                                            error={touched.khcT_DanhGias?.[index]?.nguyenNhanThatBai && Boolean((errors.khcT_DanhGias as any)?.[index]?.nguyenNhanThatBai)}
                                                                            helperText={touched.khcT_DanhGias?.[index]?.nguyenNhanThatBai && (errors.khcT_DanhGias as any)?.[index]?.nguyenNhanThatBai}
                                                                        />
                                                                    </TableCell> :
                                                                    <>
                                                                        {props?.isEdit && (
                                                                            (values?.khcT_DanhGias?.some(item => item.trangThai === false) ||
                                                                                values?.khcT_DanhGias?.some(item => item.trangThai === 'false') ||
                                                                                values?.khcT_DanhGias?.some(item => item.trangThaiQuanLy === false) ||
                                                                                values?.khcT_DanhGias?.some(item => item.trangThaiQuanLy === 'false')) && (
                                                                                <TableCell align="center" sx={{ color: 'white' }}>
                                                                                    {/* Nội dung ở đây */}
                                                                                </TableCell>
                                                                            )
                                                                        )}
                                                                    </>
                                                                }
                                                                <TableCell align="center">
                                                                    {(props.isEdit && props.defaulValue?.nguoiTaoID !== userID) || values.khcT_DanhGias.length > 1 && (
                                                                        <IconButton
                                                                            onClick={() => {
                                                                                const newDanhGias = values.khcT_DanhGias.filter((_, i) => i !== index);
                                                                                setFieldValue('khcT_DanhGias', newDanhGias);
                                                                            }}
                                                                            color="error"
                                                                        >
                                                                            <RemoveCircleOutlineIcon />
                                                                        </IconButton>
                                                                    )}
                                                                </TableCell>
                                                            </TableRow>
                                                        ))}
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography variant="h5" sx={{ mb: 2 }}>Danh sách công việc phát sinh
                                                {
                                                    !(props.isEdit && props.defaulValue?.nguoiTaoID !== userID) && <IconButton
                                                        onClick={() => {
                                                            setFieldValue('khcT_CongViecPhatSinhs', [...values.khcT_CongViecPhatSinhs, { noiDung: '', diemTuDanhGia: 0, diemQuanLy: 0 }]);
                                                        }}
                                                        color="primary"
                                                    >
                                                        <AddCircleOutlineIcon />
                                                    </IconButton>
                                                }
                                            </Typography>
                                            {/* Sử dụng TableContainer để bao quanh bảng, giúp hỗ trợ cuộn nếu cần */}
                                            <TableContainer component={Paper}>
                                                <Table>
                                                    <TableHead sx={{ backgroundColor: '#2c8bf2' }}>
                                                        <TableRow>
                                                            <TableCell width="5%" align="center" sx={{ color: 'white' }}>STT</TableCell>
                                                            <TableCell align="center" width="35%" sx={{ color: 'white' }}>Nội dung</TableCell>
                                                            {
                                                                props?.isEdit && <TableCell align="center" width="20%" sx={{ color: 'white' }} colSpan={2}>Nhân sự</TableCell>
                                                            }

                                                            {/* <TableCell align="center" width="10%" sx={{ color: 'white' }}>Điểm quản lý</TableCell> */}
                                                            {/* /{props?.isEdit && <TableCell align="center" width="10%" sx={{ color: 'white' }}>Trạng thái</TableCell>} */}
                                                            {props?.isEdit && <TableCell align="center" width="10%" sx={{ color: 'white' }}>QL đánh giá</TableCell>}
                                                            {props?.isEdit && (
                                                                (values?.khcT_CongViecPhatSinhs?.some(item => item.trangThai === false) ||
                                                                    values?.khcT_CongViecPhatSinhs?.some(item => item.trangThai === 'false') ||
                                                                    values?.khcT_CongViecPhatSinhs?.some(item => item.trangThaiQuanLy === false) ||
                                                                    values?.khcT_CongViecPhatSinhs?.some(item => item.trangThaiQuanLy === 'false'))
                                                                && (
                                                                    <TableCell align="center" width="25%" sx={{ color: 'white' }}>
                                                                        Nguyên nhân
                                                                    </TableCell>
                                                                )
                                                            )}
                                                            <TableCell align="center" width="12%" sx={{ color: 'white' }}>Thao tác</TableCell>
                                                        </TableRow>
                                                        {
                                                            props.isEdit && <TableRow>
                                                                <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                                                <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                                                <TableCell align="center" width="10%" sx={{ color: 'white' }}>Đánh giá (%)</TableCell>
                                                                {/* <TableCell align="center" width="10%" sx={{ color: 'white' }}>Điểm quản lý</TableCell> */}
                                                                {props?.isEdit && <TableCell align="center" sx={{ color: 'white' }}>Trạng thái</TableCell>}
                                                                {props?.isEdit && <TableCell align="center" sx={{ color: 'white' }}></TableCell>}
                                                                {props?.isEdit && (
                                                                    (values?.khcT_CongViecPhatSinhs?.some(item => item.trangThai === false) ||
                                                                        values?.khcT_CongViecPhatSinhs?.some(item => item.trangThai === 'false') ||
                                                                        values?.khcT_CongViecPhatSinhs?.some(item => item.trangThaiQuanLy === false) ||
                                                                        values?.khcT_CongViecPhatSinhs?.some(item => item.trangThaiQuanLy === 'false')) && (
                                                                        <TableCell align="center" sx={{ color: 'white' }}>
                                                                            {/* Nội dung ở đây */}
                                                                        </TableCell>
                                                                    )
                                                                )}
                                                                <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                                            </TableRow>
                                                        }
                                                    </TableHead>
                                                    <TableBody>
                                                        {values?.khcT_CongViecPhatSinhs?.map((item, index) => (
                                                            <TableRow key={index} >
                                                                {/* Nội dung */}
                                                                <TableCell
                                                                    align="center"
                                                                    style={{ textAlign: 'center', verticalAlign: 'middle' }}
                                                                >
                                                                    {index + 1}
                                                                </TableCell>
                                                                <TableCell  >
                                                                    <TextField
                                                                        type="text"
                                                                        name={`khcT_CongViecPhatSinhs[${index}].noiDung`}
                                                                        value={item.noiDung}
                                                                        onChange={handleChange}
                                                                        onBlur={handleBlur}
                                                                        placeholder="Nội dung"
                                                                        disabled={(props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                                                        fullWidth
                                                                        multiline
                                                                        minRows={4}
                                                                        error={touched.khcT_CongViecPhatSinhs?.[index]?.noiDung && Boolean((errors.khcT_CongViecPhatSinhs as any)?.[index]?.noiDung)}
                                                                        helperText={touched.khcT_CongViecPhatSinhs?.[index]?.noiDung && (errors.khcT_CongViecPhatSinhs as any)?.[index]?.noiDung}
                                                                    />
                                                                </TableCell>
                                                                {/* Điểm đánh giá */}
                                                                {
                                                                    props?.isEdit && <TableCell align="center" sx={{ p: 2 }}>
                                                                        <CustomInput
                                                                            type="number"
                                                                            name={`khcT_CongViecPhatSinhs[${index}].diemTuDanhGia`}
                                                                            value={item.diemTuDanhGia}
                                                                            onChange={handleChange}
                                                                            onBlur={handleBlur}
                                                                            placeholder="Điểm đánh giá"
                                                                            disabled={userID !== props?.defaulValue?.nguoiTaoID}
                                                                        />
                                                                        {touched.khcT_CongViecPhatSinhs?.[index]?.diemTuDanhGia && (errors.khcT_CongViecPhatSinhs as any)?.[index]?.diemTuDanhGia && (
                                                                            <FormHelperText error>{(errors.khcT_CongViecPhatSinhs as any)?.[index]?.diemTuDanhGia}</FormHelperText>
                                                                        )}
                                                                    </TableCell>
                                                                }
                                                                {
                                                                    props?.isEdit ?
                                                                        <TableCell align="left" sx={{ p: 2 }}>
                                                                            <FormControl fullWidth>
                                                                                <Select
                                                                                    labelId={`trangThai-label-${index}`}
                                                                                    name={`khcT_CongViecPhatSinhs[${index}].trangThai`}
                                                                                    value={item.trangThai ?? "null"}
                                                                                    onChange={handleChange}
                                                                                    onBlur={handleBlur}
                                                                                    disabled={userID !== props?.defaulValue?.nguoiTaoID}
                                                                                >
                                                                                    <MenuItem value="null">Chưa xác định</MenuItem>
                                                                                    <MenuItem value="true">Đạt</MenuItem>
                                                                                    <MenuItem value="false">Chưa đạt</MenuItem>
                                                                                </Select>
                                                                                {touched.khcT_CongViecPhatSinhs?.[index]?.trangThai && (errors.khcT_CongViecPhatSinhs as any)?.[index]?.trangThai && (
                                                                                    <FormHelperText error>{(errors.khcT_CongViecPhatSinhs as any)?.[index]?.trangThai}</FormHelperText>
                                                                                )}
                                                                            </FormControl>
                                                                        </TableCell> :
                                                                        <></>
                                                                }
                                                                {
                                                                    props?.isEdit ?
                                                                        <TableCell align="left" sx={{ p: 2 }}>
                                                                            <FormControl fullWidth>
                                                                                <Select
                                                                                    labelId={`trangThaiQuanLy-label-${index}`}
                                                                                    name={`khcT_CongViecPhatSinhs[${index}].trangThaiQuanLy`}
                                                                                    value={item.trangThaiQuanLy ?? "null"}
                                                                                    onChange={handleChange}
                                                                                    onBlur={handleBlur}
                                                                                    disabled={userID !== props?.defaulValue?.nguoiDuyetID}
                                                                                >
                                                                                    <MenuItem value="null">Chưa xác định</MenuItem>
                                                                                    <MenuItem value="true">Đạt</MenuItem>
                                                                                    <MenuItem value="false">Chưa đạt</MenuItem>
                                                                                </Select>
                                                                                {touched.khcT_CongViecPhatSinhs?.[index]?.trangThaiQuanLy && (errors.khcT_CongViecPhatSinhs as any)?.[index]?.trangThaiQuanLy && (
                                                                                    <FormHelperText error>{(errors.khcT_CongViecPhatSinhs as any)?.[index]?.trangThaiQuanLy}</FormHelperText>
                                                                                )}
                                                                            </FormControl>
                                                                        </TableCell> :
                                                                        <></>
                                                                }
                                                                {props?.isEdit && ((item.trangThai === false || (item.trangThai === 'false')) || (item.trangThaiQuanLy === false || (item.trangThaiQuanLy === 'false'))) ?
                                                                    <TableCell align="left" sx={{ p: 2 }}>
                                                                        <TextField
                                                                            type="text"
                                                                            name={`khcT_CongViecPhatSinhs[${index}].nguyenNhanThatBai`}
                                                                            value={item.nguyenNhanThatBai}
                                                                            onChange={handleChange}
                                                                            onBlur={handleBlur}
                                                                            placeholder="Nguyên nhân"
                                                                            disabled={(props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                                                            fullWidth
                                                                            multiline
                                                                            minRows={4}
                                                                            error={touched.khcT_CongViecPhatSinhs?.[index]?.nguyenNhanThatBai && Boolean((errors.khcT_CongViecPhatSinhs as any)?.[index]?.nguyenNhanThatBai)}
                                                                            helperText={touched.khcT_CongViecPhatSinhs?.[index]?.nguyenNhanThatBai && (errors.khcT_CongViecPhatSinhs as any)?.[index]?.nguyenNhanThatBai}
                                                                        />
                                                                    </TableCell> :
                                                                    <>
                                                                        {props?.isEdit && (
                                                                            (values?.khcT_CongViecPhatSinhs?.some(item => item.trangThai === false) ||
                                                                                values?.khcT_CongViecPhatSinhs?.some(item => item.trangThai === 'false') ||
                                                                                values?.khcT_CongViecPhatSinhs?.some(item => item.trangThaiQuanLy === false) ||
                                                                                values?.khcT_CongViecPhatSinhs?.some(item => item.trangThaiQuanLy === 'false')) && (
                                                                                <TableCell align="center" sx={{ color: 'white' }}>
                                                                                    {/* Nội dung ở đây */}
                                                                                </TableCell>
                                                                            )
                                                                        )}
                                                                    </>
                                                                }
                                                                <TableCell align="center">
                                                                    {(props.isEdit && props.defaulValue?.nguoiTaoID !== userID) || values.khcT_CongViecPhatSinhs.length > 1 && (
                                                                        <IconButton
                                                                            onClick={() => {
                                                                                const newCongViecPhatSinhs = values.khcT_CongViecPhatSinhs.filter((_, i) => i !== index);
                                                                                setFieldValue('khcT_CongViecPhatSinhs', newCongViecPhatSinhs);
                                                                            }}
                                                                            color="error"
                                                                        >
                                                                            <RemoveCircleOutlineIcon />
                                                                        </IconButton>
                                                                    )}
                                                                </TableCell>
                                                            </TableRow>
                                                        ))}
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </Grid>
                                    </>
                                    : <>
                                    </>
                            }
                        </Grid>
                    </Box>
                    <Box sx={{ mt: 2 }}>
                        <AnimateButton>
                            <Button disableElevation disabled={isSubmitting} fullWidth size="large" type="submit" variant="contained">
                                {props.defaulValue?.isApprove ? "Đánh giá" : props.buttonActionText}
                            </Button>
                        </AnimateButton>
                    </Box>
                </form>
            )}
        </Formik >
    )
}