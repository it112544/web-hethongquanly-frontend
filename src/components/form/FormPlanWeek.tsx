import { useState, useEffect } from 'react';
import { useTheme } from '@mui/material/styles';
import {
    Autocomplete,
    Box,
    Button,
    Checkbox,
    Divider,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    IconButton,
    InputAdornment,
    InputLabel,
    ListItemText,
    MenuItem,
    OutlinedInput,
    Paper,
    Select,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
    Typography,
    useMediaQuery
} from '@mui/material';
import * as Yup from 'yup';
import { Formik } from 'formik';
import AnimateButton from '@/components/button/AnimateButton';
import { toast } from 'react-toastify';
import { CustomInput } from '@/components/input';
import dayjs, { Dayjs } from 'dayjs';
import 'dayjs/locale/vi';
dayjs.locale('vi');
import { PlanMonth, PlanWeek } from '@/interfaces/plan';
import usePlanWeek from '@/hooks/usePlanWeek';
import usePlanMonth from '@/hooks/usePlanMonth';
import useRoleLocalStorage from '@/hooks/useRoleLocalStorage';
import dynamic from "next/dynamic";
import "react-quill/dist/quill.snow.css";
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import useStaff from '@/hooks/useStaff';
import { format } from 'date-fns';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const QuillEditor = dynamic(() => import("react-quill"), { ssr: false });
import { vi } from "date-fns/locale";
import { IconCalendar } from '@tabler/icons-react';
interface CustomInputProps {
    value: string;
    onClick: () => void;
}
export function CustomInputDate({ value, onClick }: CustomInputProps) {
    return (
        <div className="input-group position-relative">
            <input
                type="text"
                className="form-control custom-input"
                value={value}
                onClick={onClick}
                readOnly
                style={{ width: '100%', height: '20px !important' }} // Giảm chiều rộng để có chỗ cho biểu tượng "X"
            />
            <span className="icon-background" onClick={onClick}>
                <IconCalendar color="white" />
            </span>

        </div>
    );
}
interface Props {
    title?: string,
    defaulValue?: PlanWeek,
    handleOpenAlert?: (e: boolean) => void,
    open?: boolean,
    id?: number,
    handleOpen?: (e: boolean) => void,
    handleEdit?: (e: any) => void;
    handleDelete?: (e: any) => void;
    isEdit?: boolean;
    buttonActionText: string
}
export default function FormPlanWeek(props: Props) {
    const theme = useTheme();
    const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));
    const { updatePlanWeek, addPlanWeek, dataPlanWeek } = usePlanWeek()
    const { getAllPlanMonth, updatePlanMonth, addPlanMonth, dataPlanMonth, deleteMulPlanMonth } = usePlanMonth()


    const { userID, userName } = useRoleLocalStorage()
    const [denNgay, setDenNgay] = useState(new Date());
    const [tuNgay, setTuNgay] = useState(new Date());
    const [planMonths, setPlanMonths] = useState<PlanMonth[]>([]);

    const { dataStaff } = useStaff()

    const [account, setAccount] = useState<any>()

    useEffect(() => {
        setAccount(JSON.parse(localStorage.getItem('account')!))
    }, []);


    useEffect(() => {
        if (dataPlanMonth && props.defaulValue) {
            const { thangID, khcT_Thang } = props.defaulValue;

            // Lọc các phần tử theo `createBy`
            let filteredPlanMonths = dataPlanMonth.filter(plan => plan?.createBy === userName);
            console.log('gia tri', props.defaulValue);

            // Kiểm tra nếu `khcT_Thang` không nằm trong `filteredPlanMonths`
            if (khcT_Thang && !filteredPlanMonths.some(plan => plan.thangID === khcT_Thang.thangID)) {
                filteredPlanMonths.push(khcT_Thang);
            }

            // Cập nhật state
            setPlanMonths(filteredPlanMonths);
        }
        if (dataPlanMonth && props.isEdit === false) {
            // Lọc các phần tử theo `createBy`
            let filteredPlanMonths = dataPlanMonth.filter(plan => plan?.createBy === userName);
            // Cập nhật state
            setPlanMonths(filteredPlanMonths);
        }
    }, [props.defaulValue, dataPlanMonth, userName, props.isEdit]);
    const { handleOpen } = props;
    const [content, setContent] = useState("");
    const quillModules = {
        toolbar: [
            [{ header: [1, 2, 3, false] }],
            ["bold", "italic", "underline", "strike", "blockquote"],
            [{ list: "ordered" }, { list: "bullet" }],
            ["link", "image"],
            [{ align: [] }],
            [{ color: [] }],
            ["code-block"],
            ["clean"],
        ],
    };
    const quillFormats = [
        "header",
        "bold",
        "italic",
        "underline",
        "strike",
        "blockquote",
        "list",
        "bullet",
        "link",
        "image",
        "align",
        "color",
        "code-block",
    ];
    useEffect(() => {
        getAllPlanMonth()
    }, [])

    const onSubmit = async (values: any, { setErrors, setStatus, setSubmitting, resetForm }: any) => {
        values.nam = dataPlanMonth.find(item => item.thangID === values.thangID)?.nam
        values.nguoiDuyet = dataStaff?.find(item => item.nhanVienID === values.nguoiDuyetID)?.tenNhanVien
        console.log("ds", values);
        dayjs(values.ngayBatDau).format('DD/MM/YYYY HH:mm:ss')
        //  values.tuNgay = dayjs(values.tuNgay).format('DD/MM/YYYY HH:mm:ss');
        //values.denNgay = dayjs(values.denNgay).format('DD/MM/YYYY HH:mm:ss');
        if (Array.isArray(values.khcT_DanhGias)) {
            values.khcT_DanhGias = values.khcT_DanhGias.map((item: any) => ({
                ...item,
                trangThai: (item.trangThai === "true" || item.trangThai === true)
                    ? true
                    : (item.trangThai === "false" || item.trangThai === false)
                        ? false
                        : null,
                trangThaiQuanLy: (item.trangThaiQuanLy === "true" || item.trangThaiQuanLy === true)
                    ? true
                    : (item.trangThaiQuanLy === "false" || item.trangThaiQuanLy === false)
                        ? false
                        : null,
            }));
        }
        if (Array.isArray(values.khcT_CongViecPhatSinhs)) {
            values.khcT_CongViecPhatSinhs = values.khcT_CongViecPhatSinhs.map((item: any) => ({
                ...item,
                trangThai: (item.trangThai === "true" || item.trangThai === true)
                    ? true
                    : (item.trangThai === "false" || item.trangThai === false)
                        ? false
                        : null,
                trangThaiQuanLy: (item.trangThaiQuanLy === "true" || item.trangThaiQuanLy === true)
                    ? true
                    : (item.trangThaiQuanLy === "false" || item.trangThaiQuanLy === false)
                        ? false
                        : null,
            }));
        }
        if (props.isEdit) {
            values.tuanID = props.defaulValue?.tuanID;
            const rs = await updatePlanWeek(values)
            if (rs) {
                toast.success('Cập nhật thành công')
                setStatus({ success: true });
                setSubmitting(false);
                if (handleOpen) {
                    handleOpen(false)
                }
            }
            else {
                setStatus({ success: false });
                setSubmitting(false);
                toast.error('Cập nhật thất bại')
            }
        }
        else {
            const rs = await addPlanWeek(values)
            if (rs) {
                if (handleOpen) {
                    handleOpen(false)
                }
                toast.success('Thêm thành công')
                resetForm()
                setStatus({ success: true });
                setSubmitting(false);
            }
            else {
                setStatus({ success: false });
                setSubmitting(false);
                toast.error('Thêm thất bại')
            }
        }
    };
    function parseDateString(dateString: string) {
        const regex = /(\d{2})\/(\d{2})\/(\d{4}) (\d{2}):(\d{2}):(\d{2})/;
        const match = dateString.match(regex);
        if (!match) return null;

        const [, day, month, year, hour, minute, second] = match.map(Number);
        return new Date(year, month - 1, day, hour, minute, second);
    }
    useEffect(() => {
        if (props?.defaulValue) {
            setContent(props.defaulValue.noiDung);
            const parseTuNgay = props.defaulValue.tuNgay ? parseDateString(props.defaulValue.tuNgay) : new Date();
            const parseDenNgay = props.defaulValue.denNgay ? parseDateString(props.defaulValue.denNgay) : new Date();
            setDenNgay(parseDenNgay as Date);
            setTuNgay(parseTuNgay as Date);
        }
    }, [props?.defaulValue])
    return (
        <Formik
            initialValues={{
                noiDung: props?.defaulValue?.noiDung ?? '',
                tieuDe: props?.defaulValue?.tieuDe ?? '',
                nam: props?.defaulValue?.nam ?? 0,
                tuan_Thang: props?.defaulValue?.tuan_Thang ?? 0,
                thangID: props?.defaulValue?.thangID ?? 0,
                tuNgay: props.defaulValue?.tuNgay ? props.defaulValue?.tuNgay : format(new Date(), 'dd/MM/yyyy HH:mm:ss'),
                denNgay: props.defaulValue?.denNgay ? props.defaulValue?.denNgay : format(new Date(), 'dd/MM/yyyy HH:mm:ss'),
                nguoiDuyetID: props?.defaulValue?.nguoiDuyetID ?? 0,
                nguoiDuyet: props?.defaulValue?.nguoiDuyet ?? 0,
                lyDoTuChoi: props?.defaulValue?.lyDoTuChoi ?? null,
                khcT_DanhGias: props?.defaulValue?.khcT_DanhGias ?? [
                    { noiDung: "", diemTuDanhGia: 0, diemQuanLy: 0, trangThai: null, trangThaiQuanLy: null, nguyenNhanThatBai: null }
                ],
                khcT_CongViecPhatSinhs: props?.defaulValue?.khcT_CongViecPhatSinhs ?? [
                   
                ]
            }}
            validationSchema={Yup.object().shape({
                noiDung: Yup.string().required('Không được bỏ trống'),
                thangID: Yup.number().required('Không được bỏ trống'),
                nguoiDuyetID: Yup.number().required('Không được bỏ trống'),
                tieuDe: Yup.string().required('Không được bỏ trống'),
                tuan_Thang: Yup.number().required('Không được bỏ trống'),
                tuNgay: Yup.string()
                    .required('Thời gian bắt đầu không được bỏ trống'),
                denNgay: Yup.string()
                    .required('Thời gian kết thúc không được bỏ trống'),
                khcT_DanhGias: Yup.array().of(
                    Yup.object().shape({
                        noiDung: Yup.string().required('Nội dung không được bỏ trống'),
                        diemTuDanhGia: Yup.number().min(0, 'Điểm phải lớn hơn hoặc bằng 0').required('Điểm đánh giá không được bỏ trống'),
                        diemQuanLy: Yup.number().min(0, 'Điểm phải lớn hơn hoặc bằng 0').required('Điểm quản lý không được bỏ trống')
                    })
                ).min(1, 'Cần có ít nhất một mục trong bảng'),
                khcT_CongViecPhatSinhs: Yup.array().of(
                    Yup.object().shape({
                        noiDung: Yup.string().required('Nội dung không được bỏ trống'),
                        diemTuDanhGia: Yup.number().min(0, 'Điểm phải lớn hơn hoặc bằng 0').required('Điểm đánh giá không được bỏ trống'),
                        diemQuanLy: Yup.number().min(0, 'Điểm phải lớn hơn hoặc bằng 0').required('Điểm quản lý không được bỏ trống')
                    })
                )
            })}
            onSubmit={onSubmit}
        >
            {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values, setFieldValue }) => (
                <form noValidate onSubmit={handleSubmit} style={{ width: '100%' }}>
                    <Box display='flex' flexDirection='column' gap={2}>
                        <Grid container spacing={matchDownSM ? 1 : 2}>
                            <Grid item xs={12} sm={12}>
                                <FormControl fullWidth error={Boolean(touched.tieuDe && errors.tieuDe)}>
                                    <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                        Tiêu đề {''}
                                        <span className="required_text">(*)</span>
                                    </Typography>
                                    <CustomInput
                                        id="outlined-adornment-tieuDe"
                                        type="text"
                                        disabled={props?.defaulValue?.isApprove || (props?.isEdit === true && props.defaulValue?.nguoiTaoID !== userID)}
                                        value={values?.tieuDe}
                                        name="tieuDe"
                                        placeholder='Tiêu đề'
                                        onBlur={handleBlur}
                                        onChange={(e) => {
                                            setFieldValue('tieuDe', e.target.value);
                                        }}
                                    />
                                    {touched.tieuDe && errors.tieuDe && (
                                        <FormHelperText error id="helper-text-tieuDe">{errors.tieuDe.toString()}</FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <FormControl fullWidth error={Boolean(touched.thangID && errors.thangID)}>
                                    <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                        Kế hoạch tháng {''}
                                        <span className="required_text">(*)</span>
                                    </Typography>
                                    <Select
                                        labelId="demo-simple-select-label-month"
                                        label="Tháng"
                                        name="thang"
                                        disabled={props?.defaulValue?.isApprove || (props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                        value={values.thangID}
                                        onChange={(e) => {
                                            setFieldValue('thangID', e.target.value);
                                        }}
                                        input={<CustomInput />}
                                    >

                                        {planMonths?.map((item, index) => (
                                            <MenuItem key={index} value={item.thangID}>Tháng {item.thang}: {item.tieuDe}</MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <FormControl fullWidth error={Boolean(touched.noiDung && errors.noiDung)}>
                                    <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                        Nội dung chính {''}
                                        <span className="required_text">(*)</span>
                                    </Typography>
                                    {/* <CustomInput
                                        id="outlined-adornment-noiDung"
                                        type="text"
                                        value={values?.noiDung}
                                        name="noiDung"
                                        placeholder='Nội dung'
                                        onBlur={handleBlur}
                                        onChange={(e) => {
                                            setFieldValue('noiDung', e.target.value);
                                        }}
                                        multiline
                                        rows={5}
                                    /> */}
                                    <QuillEditor
                                        value={content}
                                        onChange={(newContent) => {
                                            setContent(newContent);
                                            setFieldValue("noiDung", newContent);
                                        }}
                                        readOnly={props?.defaulValue?.isApprove || (props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                        modules={quillModules}
                                        formats={quillFormats}
                                        style={{ height: "100%", width: "100%", paddingBottom: "32px" }}
                                        className="w-full h-full mt-10 bg-white"
                                    />
                                    {touched.noiDung && errors.noiDung && (
                                        <FormHelperText error id="helper-text-noiDung">{errors.noiDung.toString()}</FormHelperText>
                                    )}
                                </FormControl>

                            </Grid>
                            {/* Từ ngày */}
                            <Grid item xs={12} md={6}>
                                <Box style={{ width: "100%" }} >
                                    <Typography sx={{ mb: 1.5, mt: 1.5, fontWeight: "bold" }}>
                                        Từ ngày  <span className="required_text">(*)</span>{" "}
                                    </Typography>
                                    <DatePicker
                                        locale={vi}
                                        disabled={props?.defaulValue?.isApprove || (props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                        className="react-date-picker"
                                        selected={tuNgay}
                                        onChange={(date: Date) => {
                                            setTuNgay(date);
                                            const dateString = date ? format(date, 'dd/MM/yyyy HH:mm:ss') : '';
                                            setFieldValue("tuNgay", dateString);
                                        }}
                                        onYearChange={(date: Date) => {
                                            setTuNgay(date);
                                            const dateString = date ? format(date, 'dd/MM/yyyy HH:mm:ss') : '';
                                            setFieldValue("tuNgay", dateString);

                                        }}
                                        onMonthChange={(date: Date) => {
                                            setTuNgay(date);
                                            const dateString = date ? format(date, 'dd/MM/yyyy HH:mm:ss') : '';
                                            setFieldValue("tuNgay", dateString);
                                        }}
                                        customInput={<CustomInputDate value={tuNgay ? tuNgay.toLocaleDateString() : ''} onClick={() => { }} />}
                                        dateFormat="dd/MM/yyyy HH:mm" // Date and time format
                                        showTimeSelect // Enable time selection
                                        timeIntervals={1} // Time intervals in minutes
                                        timeFormat="HH:mm" // Format for time
                                        timeCaption="Thời gian" // Caption for time selection
                                        popperPlacement="top-end"
                                        showPopperArrow={true} // Optionally show arrow on the popper
                                    />
                                    {touched.tuNgay &&
                                        Boolean(errors.tuNgay) && (
                                            <FormHelperText className="required_text">
                                                {" "}
                                                {errors.tuNgay
                                                    ? errors.tuNgay.toString()
                                                    : ""}
                                            </FormHelperText>
                                        )}
                                </Box>
                            </Grid>
                            {/* Đến ngày */}
                            <Grid item xs={12} md={6}>
                                <Box style={{ width: "100%" }} >
                                    <Typography sx={{ mb: 1.5, mt: 1.5, fontWeight: "bold" }}>
                                        Đến ngày  <span className="required_text">(*)</span>{" "}
                                    </Typography>
                                    <DatePicker
                                        locale={vi}
                                        className="react-date-picker"
                                        selected={denNgay}
                                        disabled={props?.defaulValue?.isApprove || (props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                        onChange={(date: Date) => {
                                            setDenNgay(date);
                                            const dateString = date ? format(date, 'dd/MM/yyyy HH:mm:ss') : '';
                                            setFieldValue("denNgay", dateString);
                                        }}
                                        onYearChange={(date: Date) => {
                                            setDenNgay(date);
                                            const dateString = date ? format(date, 'dd/MM/yyyy HH:mm:ss') : '';
                                            setFieldValue("denNgay", dateString);

                                        }}
                                        onMonthChange={(date: Date) => {
                                            setDenNgay(date);
                                            const dateString = date ? format(date, 'dd/MM/yyyy HH:mm:ss') : '';
                                            setFieldValue("denNgay", dateString);
                                        }}
                                        customInput={<CustomInputDate value={denNgay ? denNgay.toLocaleDateString() : ''} onClick={() => { }} />}
                                        dateFormat="dd/MM/yyyy HH:mm" // Date and time format
                                        showTimeSelect // Enable time selection
                                        timeIntervals={1} // Time intervals in minutes
                                        timeFormat="HH:mm" // Format for time
                                        timeCaption="Thời gian" // Caption for time selection
                                        popperPlacement="top-end"
                                        showPopperArrow={true} // Optionally show arrow on the popper
                                    />
                                    {touched.denNgay &&
                                        Boolean(errors.denNgay) && (
                                            <FormHelperText className="required_text">
                                                {" "}
                                                {errors.denNgay
                                                    ? errors.denNgay.toString()
                                                    : ""}
                                            </FormHelperText>
                                        )}
                                </Box>
                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <FormControl fullWidth error={Boolean(touched.tuan_Thang && errors.tuan_Thang)}>
                                    <Typography sx={{ mb: 1.5, fontWeight: "bold" }}>
                                        Tuần {''}
                                        <span className="required_text">(*)</span>
                                    </Typography>
                                    <Select
                                        disabled={props?.defaulValue?.isApprove || (props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                        labelId="demo-simple-select-label-week"
                                        label="Tuần"
                                        name="tuan_Thang"
                                        value={values.tuan_Thang}
                                        onChange={(e) => {
                                            setFieldValue('tuan_Thang', e.target.value);
                                        }}
                                        input={<CustomInput />}
                                    >

                                        {[1, 2, 3, 4, 5].map((item, index) => (
                                            <MenuItem key={index} value={item}>{item}</MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <FormControl fullWidth error={Boolean(touched.nguoiDuyetID && errors.nguoiDuyetID)}>
                                    <Typography sx={{ mb: 1.5, fontWeight: 'bold' }}>
                                        Người duyệt kế hoạch {''}
                                        <span className="required_text">(*)</span>
                                    </Typography>
                                    <Autocomplete
                                        id="nguoiDuyetID"
                                        options={dataStaff || []}
                                        getOptionLabel={(option) => option.tenNhanVien} // Hiển thị tên nhân viên trong dropdown
                                        value={dataStaff.find(item => item.nhanVienID === values.nguoiDuyetID) || null}
                                        onChange={(event, newValue) => {
                                            setFieldValue('nguoiDuyetID', newValue ? newValue.nhanVienID : null);
                                        }}
                                        disabled={props?.defaulValue?.isApprove || (props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                error={Boolean(touched.nguoiDuyetID && errors.nguoiDuyetID)}
                                                helperText={touched.nguoiDuyetID && errors.nguoiDuyetID}
                                            />
                                        )}
                                    />
                                </FormControl>
                            </Grid>
                            {
                                values.nguoiDuyetID ?
                                    <>
                                        <Grid item xs={12}>
                                            <Typography variant="h5" sx={{ mb: 2 }}>Danh sách công việc
                                                {
                                                    !(props.isEdit && props.defaulValue?.nguoiTaoID !== userID) && <IconButton
                                                        onClick={() => {
                                                            setFieldValue('khcT_DanhGias', [...values.khcT_DanhGias, { noiDung: '', diemTuDanhGia: 0, diemQuanLy: 0 }]);
                                                        }}
                                                        color="primary"
                                                    >
                                                        <AddCircleOutlineIcon />
                                                    </IconButton>
                                                }
                                            </Typography>
                                            {/* Sử dụng TableContainer để bao quanh bảng, giúp hỗ trợ cuộn nếu cần */}
                                            <TableContainer component={Paper}>
                                                <Table>
                                                    <TableHead sx={{ backgroundColor: '#2c8bf2' }}>
                                                        <TableRow>
                                                            <TableCell width="5%" align="center" sx={{ color: 'white' }}>STT</TableCell>
                                                            <TableCell align="center" width="35%" sx={{ color: 'white' }}>Nội dung</TableCell>
                                                            {
                                                                props?.isEdit && <TableCell align="center" width="20%" sx={{ color: 'white' }} colSpan={2}>Nhân sự</TableCell>
                                                            }

                                                            {/* <TableCell align="center" width="10%" sx={{ color: 'white' }}>Điểm quản lý</TableCell> */}
                                                            {/* /{props?.isEdit && <TableCell align="center" width="10%" sx={{ color: 'white' }}>Trạng thái</TableCell>} */}
                                                            {props?.isEdit && <TableCell align="center" width="10%" sx={{ color: 'white' }}>QL đánh giá</TableCell>}
                                                            {props?.isEdit && (
                                                                (values?.khcT_DanhGias?.some(item => item.trangThai === false) ||
                                                                    values?.khcT_DanhGias?.some(item => item.trangThai === 'false') ||
                                                                    values?.khcT_DanhGias?.some(item => item.trangThaiQuanLy === false) ||
                                                                    values?.khcT_DanhGias?.some(item => item.trangThaiQuanLy === 'false'))
                                                                && (
                                                                    <TableCell align="center" width="25%" sx={{ color: 'white' }}>
                                                                        Nguyên nhân
                                                                    </TableCell>
                                                                )
                                                            )}
                                                            <TableCell align="center" width="12%" sx={{ color: 'white' }}>Thao tác</TableCell>
                                                        </TableRow>
                                                        {
                                                            props.isEdit && <TableRow>
                                                                <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                                                <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                                                <TableCell align="center" width="10%" sx={{ color: 'white' }}>Đánh giá (%)</TableCell>
                                                                {/* <TableCell align="center" width="10%" sx={{ color: 'white' }}>Điểm quản lý</TableCell> */}
                                                                {props?.isEdit && <TableCell align="center" sx={{ color: 'white' }}>Trạng thái</TableCell>}
                                                                {props?.isEdit && <TableCell align="center" sx={{ color: 'white' }}></TableCell>}
                                                                {props?.isEdit && (
                                                                    (values?.khcT_DanhGias?.some(item => item.trangThai === false) ||
                                                                        values?.khcT_DanhGias?.some(item => item.trangThai === 'false') ||
                                                                        values?.khcT_DanhGias?.some(item => item.trangThaiQuanLy === false) ||
                                                                        values?.khcT_DanhGias?.some(item => item.trangThaiQuanLy === 'false')) && (
                                                                        <TableCell align="center" sx={{ color: 'white' }}>
                                                                            {/* Nội dung ở đây */}
                                                                        </TableCell>
                                                                    )
                                                                )}
                                                                <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                                            </TableRow>
                                                        }
                                                    </TableHead>
                                                    <TableBody>
                                                        {values?.khcT_DanhGias?.map((item, index) => (
                                                            <TableRow key={index} >
                                                                {/* Nội dung */}
                                                                <TableCell
                                                                    align="center"
                                                                    style={{ textAlign: 'center', verticalAlign: 'middle' }}
                                                                >
                                                                    {index + 1}
                                                                </TableCell>
                                                                <TableCell  >
                                                                    <TextField
                                                                        type="text"
                                                                        name={`khcT_DanhGias[${index}].noiDung`}
                                                                        value={item.noiDung}
                                                                        onChange={handleChange}
                                                                        onBlur={handleBlur}
                                                                        placeholder="Nội dung"
                                                                        disabled={props?.defaulValue?.isApprove || (props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                                                        fullWidth
                                                                        multiline
                                                                        minRows={4}
                                                                        error={touched.khcT_DanhGias?.[index]?.noiDung && Boolean((errors.khcT_DanhGias as any)?.[index]?.noiDung)}
                                                                        helperText={touched.khcT_DanhGias?.[index]?.noiDung && (errors.khcT_DanhGias as any)?.[index]?.noiDung}
                                                                    />
                                                                </TableCell>
                                                                {/* Điểm đánh giá */}
                                                                {
                                                                    props.isEdit && <TableCell align="center" sx={{ p: 2 }}>
                                                                        <CustomInput
                                                                            type="number"
                                                                            name={`khcT_DanhGias[${index}].diemTuDanhGia`}
                                                                            value={item.diemTuDanhGia}
                                                                            onChange={handleChange}
                                                                            onBlur={handleBlur}
                                                                            placeholder="Điểm đánh giá"
                                                                            disabled={userID !== props?.defaulValue?.nguoiTaoID}
                                                                        />
                                                                        {touched.khcT_DanhGias?.[index]?.diemTuDanhGia && (errors.khcT_DanhGias as any)?.[index]?.diemTuDanhGia && (
                                                                            <FormHelperText error>{(errors.khcT_DanhGias as any)?.[index]?.diemTuDanhGia}</FormHelperText>
                                                                        )}
                                                                    </TableCell>
                                                                }
                                                                {/* Điểm quản lý */}
                                                                {/* <TableCell align="center">
                                                                <CustomInput
                                                                    type="number"
                                                                    name={`khcT_DanhGias[${index}].diemQuanLy`}
                                                                    value={item.diemQuanLy}
                                                                    onChange={handleChange}
                                                                    onBlur={handleBlur}
                                                                    disabled={props.isEdit === false || userID !== props?.defaulValue?.nguoiDuyetID}
                                                                    placeholder="Điểm quản lý"
                                                                />
                                                                {touched.khcT_DanhGias?.[index]?.diemQuanLy && (errors.khcT_DanhGias as any)?.[index]?.diemQuanLy && (
                                                                    <FormHelperText error>{(errors.khcT_DanhGias as any)?.[index]?.diemQuanLy}</FormHelperText>
                                                                )}
                                                            </TableCell> */}
                                                                {
                                                                    props?.isEdit ?
                                                                        <TableCell align="center" sx={{ p: 2 }}>
                                                                            <FormControl fullWidth>
                                                                                <Select
                                                                                    labelId={`trangThai-label-${index}`}
                                                                                    name={`khcT_DanhGias[${index}].trangThai`}
                                                                                    value={item.trangThai ?? "null"}
                                                                                    onChange={handleChange}
                                                                                    onBlur={handleBlur}
                                                                                    disabled={userID !== props?.defaulValue?.nguoiTaoID}
                                                                                >
                                                                                    <MenuItem value="null">Chưa xác định</MenuItem>
                                                                                    <MenuItem value="true">Đạt</MenuItem>
                                                                                    <MenuItem value="false">Chưa đạt</MenuItem>
                                                                                </Select>
                                                                                {touched.khcT_DanhGias?.[index]?.trangThai && (errors.khcT_DanhGias as any)?.[index]?.trangThai && (
                                                                                    <FormHelperText error>{(errors.khcT_DanhGias as any)?.[index]?.trangThai}</FormHelperText>
                                                                                )}
                                                                            </FormControl>
                                                                        </TableCell> :
                                                                        <></>
                                                                }
                                                                {
                                                                    props?.isEdit ?
                                                                        <TableCell align="center" sx={{ p: 2 }}>
                                                                            <FormControl fullWidth>
                                                                                <Select
                                                                                    labelId={`trangThaiQuanLy-label-${index}`}
                                                                                    name={`khcT_DanhGias[${index}].trangThaiQuanLy`}
                                                                                    value={item.trangThaiQuanLy ?? "null"}
                                                                                    onChange={handleChange}
                                                                                    onBlur={handleBlur}
                                                                                    disabled={userID !== props?.defaulValue?.nguoiDuyetID}
                                                                                >
                                                                                    <MenuItem value="null">Chưa xác định</MenuItem>
                                                                                    <MenuItem value="true">Đạt</MenuItem>
                                                                                    <MenuItem value="false">Chưa đạt</MenuItem>
                                                                                </Select>
                                                                                {touched.khcT_DanhGias?.[index]?.trangThaiQuanLy && (errors.khcT_DanhGias as any)?.[index]?.trangThaiQuanLy && (
                                                                                    <FormHelperText error>{(errors.khcT_DanhGias as any)?.[index]?.trangThaiQuanLy}</FormHelperText>
                                                                                )}
                                                                            </FormControl>
                                                                        </TableCell> :
                                                                        <></>
                                                                }
                                                                {props?.isEdit && ((item.trangThai === false || (item.trangThai === 'false')) || (item.trangThaiQuanLy === false || (item.trangThaiQuanLy === 'false'))) ?
                                                                    <TableCell align="left" sx={{ p: 2 }}>
                                                                        <TextField
                                                                            type="text"
                                                                            name={`khcT_DanhGias[${index}].nguyenNhanThatBai`}
                                                                            value={item.nguyenNhanThatBai}
                                                                            onChange={handleChange}
                                                                            onBlur={handleBlur}
                                                                            placeholder="Nguyên nhân"
                                                                            disabled={(props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                                                            fullWidth
                                                                            multiline
                                                                            minRows={4}
                                                                            error={touched.khcT_DanhGias?.[index]?.nguyenNhanThatBai && Boolean((errors.khcT_DanhGias as any)?.[index]?.nguyenNhanThatBai)}
                                                                            helperText={touched.khcT_DanhGias?.[index]?.nguyenNhanThatBai && (errors.khcT_DanhGias as any)?.[index]?.nguyenNhanThatBai}
                                                                        />
                                                                    </TableCell> :
                                                                    <>
                                                                        {props?.isEdit && (
                                                                            (values?.khcT_DanhGias?.some(item => item.trangThai === false) ||
                                                                                values?.khcT_DanhGias?.some(item => item.trangThai === 'false') ||
                                                                                values?.khcT_DanhGias?.some(item => item.trangThaiQuanLy === false) ||
                                                                                values?.khcT_DanhGias?.some(item => item.trangThaiQuanLy === 'false')) && (
                                                                                <TableCell align="center" sx={{ color: 'white' }}>
                                                                                    {/* Nội dung ở đây */}
                                                                                </TableCell>
                                                                            )
                                                                        )}
                                                                    </>
                                                                }
                                                                <TableCell align="center">
                                                                    {(props.isEdit && props.defaulValue?.nguoiTaoID !== userID) || values.khcT_DanhGias.length > 1 && (
                                                                        <IconButton
                                                                            onClick={() => {
                                                                                const newDanhGias = values.khcT_DanhGias.filter((_, i) => i !== index);
                                                                                setFieldValue('khcT_DanhGias', newDanhGias);
                                                                            }}
                                                                            color="error"
                                                                        >
                                                                            <RemoveCircleOutlineIcon />
                                                                        </IconButton>
                                                                    )}
                                                                </TableCell>
                                                            </TableRow>
                                                        ))}
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </Grid>
                                        <Grid item xs={12}>
                                            <Typography variant="h5" sx={{ mb: 2 }}>Danh sách công việc phát sinh
                                                {
                                                    !(props.isEdit && props.defaulValue?.nguoiTaoID !== userID) && <IconButton
                                                        onClick={() => {
                                                            setFieldValue('khcT_CongViecPhatSinhs', [...values.khcT_CongViecPhatSinhs, { noiDung: '', diemTuDanhGia: 0, diemQuanLy: 0 }]);
                                                        }}
                                                        color="primary"
                                                    >
                                                        <AddCircleOutlineIcon />
                                                    </IconButton>
                                                }
                                            </Typography>
                                            {/* Sử dụng TableContainer để bao quanh bảng, giúp hỗ trợ cuộn nếu cần */}
                                            <TableContainer component={Paper}>
                                                <Table>
                                                    <TableHead sx={{ backgroundColor: '#2c8bf2' }}>
                                                        <TableRow>
                                                            <TableCell width="5%" align="center" sx={{ color: 'white' }}>STT</TableCell>
                                                            <TableCell align="center" width="35%" sx={{ color: 'white' }}>Nội dung</TableCell>
                                                            {
                                                                props?.isEdit && <TableCell align="center" width="20%" sx={{ color: 'white' }} colSpan={2}>Nhân sự</TableCell>
                                                            }

                                                            {/* <TableCell align="center" width="10%" sx={{ color: 'white' }}>Điểm quản lý</TableCell> */}
                                                            {/* /{props?.isEdit && <TableCell align="center" width="10%" sx={{ color: 'white' }}>Trạng thái</TableCell>} */}
                                                            {props?.isEdit && <TableCell align="center" width="10%" sx={{ color: 'white' }}>QL đánh giá</TableCell>}
                                                            {props?.isEdit && (
                                                                (values?.khcT_CongViecPhatSinhs?.some(item => item.trangThai === false) ||
                                                                    values?.khcT_CongViecPhatSinhs?.some(item => item.trangThai === 'false') ||
                                                                    values?.khcT_CongViecPhatSinhs?.some(item => item.trangThaiQuanLy === false) ||
                                                                    values?.khcT_CongViecPhatSinhs?.some(item => item.trangThaiQuanLy === 'false'))
                                                                && (
                                                                    <TableCell align="center" width="25%" sx={{ color: 'white' }}>
                                                                        Nguyên nhân
                                                                    </TableCell>
                                                                )
                                                            )}
                                                            <TableCell align="center" width="12%" sx={{ color: 'white' }}>Thao tác</TableCell>
                                                        </TableRow>
                                                        {
                                                            props.isEdit && <TableRow>
                                                                <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                                                <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                                                <TableCell align="center" width="10%" sx={{ color: 'white' }}>Đánh giá (%)</TableCell>
                                                                {/* <TableCell align="center" width="10%" sx={{ color: 'white' }}>Điểm quản lý</TableCell> */}
                                                                {props?.isEdit && <TableCell align="center" sx={{ color: 'white' }}>Trạng thái</TableCell>}
                                                                {props?.isEdit && <TableCell align="center" sx={{ color: 'white' }}></TableCell>}
                                                                {props?.isEdit && (
                                                                    (values?.khcT_CongViecPhatSinhs?.some(item => item.trangThai === false) ||
                                                                        values?.khcT_CongViecPhatSinhs?.some(item => item.trangThai === 'false') ||
                                                                        values?.khcT_CongViecPhatSinhs?.some(item => item.trangThaiQuanLy === false) ||
                                                                        values?.khcT_CongViecPhatSinhs?.some(item => item.trangThaiQuanLy === 'false')) && (
                                                                        <TableCell align="center" sx={{ color: 'white' }}>
                                                                            {/* Nội dung ở đây */}
                                                                        </TableCell>
                                                                    )
                                                                )}
                                                                <TableCell align="center" sx={{ color: 'white' }}></TableCell>
                                                            </TableRow>
                                                        }
                                                    </TableHead>
                                                    <TableBody>
                                                        {values?.khcT_CongViecPhatSinhs?.map((item, index) => (
                                                            <TableRow key={index} >
                                                                {/* Nội dung */}
                                                                <TableCell
                                                                    align="center"
                                                                    style={{ textAlign: 'center', verticalAlign: 'middle' }}
                                                                >
                                                                    {index + 1}
                                                                </TableCell>
                                                                <TableCell  >
                                                                    <TextField
                                                                        type="text"
                                                                        name={`khcT_CongViecPhatSinhs[${index}].noiDung`}
                                                                        value={item.noiDung}
                                                                        onChange={handleChange}
                                                                        onBlur={handleBlur}
                                                                        placeholder="Nội dung"
                                                                        disabled={(props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                                                        fullWidth
                                                                        multiline
                                                                        minRows={4}
                                                                        error={touched.khcT_CongViecPhatSinhs?.[index]?.noiDung && Boolean((errors.khcT_CongViecPhatSinhs as any)?.[index]?.noiDung)}
                                                                        helperText={touched.khcT_CongViecPhatSinhs?.[index]?.noiDung && (errors.khcT_CongViecPhatSinhs as any)?.[index]?.noiDung}
                                                                    />
                                                                </TableCell>
                                                                {/* Điểm đánh giá */}
                                                                {
                                                                    props?.isEdit && <TableCell align="center" sx={{ p: 2 }}>
                                                                        <CustomInput
                                                                            type="number"
                                                                            name={`khcT_CongViecPhatSinhs[${index}].diemTuDanhGia`}
                                                                            value={item.diemTuDanhGia}
                                                                            onChange={handleChange}
                                                                            onBlur={handleBlur}
                                                                            placeholder="Điểm đánh giá"
                                                                            disabled={userID !== props?.defaulValue?.nguoiTaoID}
                                                                        />
                                                                        {touched.khcT_CongViecPhatSinhs?.[index]?.diemTuDanhGia && (errors.khcT_CongViecPhatSinhs as any)?.[index]?.diemTuDanhGia && (
                                                                            <FormHelperText error>{(errors.khcT_CongViecPhatSinhs as any)?.[index]?.diemTuDanhGia}</FormHelperText>
                                                                        )}
                                                                    </TableCell>
                                                                }
                                                                {
                                                                    props?.isEdit ?
                                                                        <TableCell align="left" sx={{ p: 2 }}>
                                                                            <FormControl fullWidth>
                                                                                <Select
                                                                                    labelId={`trangThai-label-${index}`}
                                                                                    name={`khcT_CongViecPhatSinhs[${index}].trangThai`}
                                                                                    value={item.trangThai ?? "null"}
                                                                                    onChange={handleChange}
                                                                                    onBlur={handleBlur}
                                                                                    disabled={userID !== props?.defaulValue?.nguoiTaoID}
                                                                                >
                                                                                    <MenuItem value="null">Chưa xác định</MenuItem>
                                                                                    <MenuItem value="true">Đạt</MenuItem>
                                                                                    <MenuItem value="false">Chưa đạt</MenuItem>
                                                                                </Select>
                                                                                {touched.khcT_CongViecPhatSinhs?.[index]?.trangThai && (errors.khcT_CongViecPhatSinhs as any)?.[index]?.trangThai && (
                                                                                    <FormHelperText error>{(errors.khcT_CongViecPhatSinhs as any)?.[index]?.trangThai}</FormHelperText>
                                                                                )}
                                                                            </FormControl>
                                                                        </TableCell> :
                                                                        <></>
                                                                }
                                                                {
                                                                    props?.isEdit ?
                                                                        <TableCell align="left" sx={{ p: 2 }}>
                                                                            <FormControl fullWidth>
                                                                                <Select
                                                                                    labelId={`trangThaiQuanLy-label-${index}`}
                                                                                    name={`khcT_CongViecPhatSinhs[${index}].trangThaiQuanLy`}
                                                                                    value={item.trangThaiQuanLy ?? "null"}
                                                                                    onChange={handleChange}
                                                                                    onBlur={handleBlur}
                                                                                    disabled={userID !== props?.defaulValue?.nguoiDuyetID}
                                                                                >
                                                                                    <MenuItem value="null">Chưa xác định</MenuItem>
                                                                                    <MenuItem value="true">Đạt</MenuItem>
                                                                                    <MenuItem value="false">Chưa đạt</MenuItem>
                                                                                </Select>
                                                                                {touched.khcT_CongViecPhatSinhs?.[index]?.trangThaiQuanLy && (errors.khcT_CongViecPhatSinhs as any)?.[index]?.trangThaiQuanLy && (
                                                                                    <FormHelperText error>{(errors.khcT_CongViecPhatSinhs as any)?.[index]?.trangThaiQuanLy}</FormHelperText>
                                                                                )}
                                                                            </FormControl>
                                                                        </TableCell> :
                                                                        <></>
                                                                }
                                                                {props?.isEdit && ((item.trangThai === false || (item.trangThai === 'false')) || (item.trangThaiQuanLy === false || (item.trangThaiQuanLy === 'false'))) ?
                                                                    <TableCell align="left" sx={{ p: 2 }}>
                                                                        <TextField
                                                                            type="text"
                                                                            name={`khcT_CongViecPhatSinhs[${index}].nguyenNhanThatBai`}
                                                                            value={item.nguyenNhanThatBai}
                                                                            onChange={handleChange}
                                                                            onBlur={handleBlur}
                                                                            placeholder="Nguyên nhân"
                                                                            disabled={(props.isEdit && props.defaulValue?.nguoiTaoID !== userID)}
                                                                            fullWidth
                                                                            multiline
                                                                            minRows={4}
                                                                            error={touched.khcT_CongViecPhatSinhs?.[index]?.nguyenNhanThatBai && Boolean((errors.khcT_CongViecPhatSinhs as any)?.[index]?.nguyenNhanThatBai)}
                                                                            helperText={touched.khcT_CongViecPhatSinhs?.[index]?.nguyenNhanThatBai && (errors.khcT_CongViecPhatSinhs as any)?.[index]?.nguyenNhanThatBai}
                                                                        />
                                                                    </TableCell> :
                                                                    <>
                                                                        {props?.isEdit && (
                                                                            (values?.khcT_CongViecPhatSinhs?.some(item => item.trangThai === false) ||
                                                                                values?.khcT_CongViecPhatSinhs?.some(item => item.trangThai === 'false') ||
                                                                                values?.khcT_CongViecPhatSinhs?.some(item => item.trangThaiQuanLy === false) ||
                                                                                values?.khcT_CongViecPhatSinhs?.some(item => item.trangThaiQuanLy === 'false')) && (
                                                                                <TableCell align="center" sx={{ color: 'white' }}>
                                                                                    {/* Nội dung ở đây */}
                                                                                </TableCell>
                                                                            )
                                                                        )}
                                                                    </>
                                                                }
                                                                <TableCell align="center">
                                                                    {(props.isEdit && props.defaulValue?.nguoiTaoID !== userID) || values.khcT_CongViecPhatSinhs.length > 1 && (
                                                                        <IconButton
                                                                            onClick={() => {
                                                                                const newCongViecPhatSinhs = values.khcT_CongViecPhatSinhs.filter((_, i) => i !== index);
                                                                                setFieldValue('khcT_CongViecPhatSinhs', newCongViecPhatSinhs);
                                                                            }}
                                                                            color="error"
                                                                        >
                                                                            <RemoveCircleOutlineIcon />
                                                                        </IconButton>
                                                                    )}
                                                                </TableCell>
                                                            </TableRow>
                                                        ))}
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </Grid>
                                    </>
                                    : <>

                                    </>
                            }
                        </Grid>
                    </Box>
                    <Box sx={{ mt: 2 }}>
                        <AnimateButton>
                            <Button disableElevation disabled={isSubmitting} fullWidth size="large" type="submit" variant="contained">
                                {props.defaulValue?.isApprove ? "Đánh giá" : props.buttonActionText}
                            </Button>
                        </AnimateButton>
                    </Box>
                </form>
            )}
        </Formik >
    )
}