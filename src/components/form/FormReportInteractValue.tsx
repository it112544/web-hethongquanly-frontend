import { useTheme } from '@mui/material/styles';
import {
    Autocomplete,
    Box,
    Button,
    Chip,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    IconButton,
    InputAdornment,
    MenuItem,
    Radio,
    RadioGroup,
    Select,
    TextField,
    Typography,
    useMediaQuery
} from '@mui/material';
import * as Yup from 'yup';
import { Formik } from 'formik';
import AnimateButton from '@/components/button/AnimateButton';
import { toast } from 'react-toastify';
import { CustomInput } from '@/components/input';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import dayjs, { Dayjs } from 'dayjs';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import useOfficers from '@/hooks/useOfficers';
import useInteraction from '@/hooks/useInteraction';
import { step } from '@/constant';
import { useEffect, useState } from 'react';
import useProjectEstimate from '@/hooks/useProjectEstimate';
import { formatCurrencyNoUnit, removeCommasAndDots } from '@/utils/formatCurrency';
import { IconUpload } from '@tabler/icons-react';
import FileDownloadIcon from "@mui/icons-material/FileDownload";
import { apiPort, getAllNhanVienByTuKhoa } from '@/constant/api';
import { useRouter } from 'next/router';
import { Staff } from '@/interfaces/user';
import useDebounce from '@/hooks/useDebounce';
import axios from 'axios';
import removeUnicode from '@/utils/removeUnicode';


interface Props {
    title?: string,
    defaulValue?: any,
    handleOpenAlert?: (e: boolean) => void,
    open?: boolean,
    id?: number,
    handleOpen?: (e: boolean) => void,
    handleEdit?: (e: any) => void;
    handleDelete?: (e: any) => void;
    isEdit?: boolean;
    buttonActionText: string
}
const FormReportInteractValue = (props: Props) => {
    const theme = useTheme();
    const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));

    /* Custom Hook */
    const { addProjectEstimate, updateProjectEstimate, dataSteps, getAllSteps } = useProjectEstimate()

    const [fileCompany, setFileCompany] = useState<File | null>(null);
    const [fileProject, setFileProject] = useState<File | null>(null);
    const [fileCompanyBase64, setFileCompanyBase64] = useState<string | null>(null);
    const [fileProjectBase64, setFileProjectBase64] = useState<string | null>(null);
    const [valueResult, setValueResult] = useState<any>(null)
    const [creators, setCreators] = useState<Staff | null>();
    const [dropdownOptionCreators, setDropdownOptionCreators] = useState<Staff[]>([]);
    const [loadingSearchCreator, setLoadingSearchCreator] = useState<boolean>(false);
    const [searchStringCreator, setSearchStringCreator] = useState('');
    const debouncedSearchTermCreator = useDebounce(searchStringCreator, 500);


    const [operators, setOperators] = useState<Staff | null>();
    const [dropdownOptionOperators, setDropdownOptionOperators] = useState<Staff[]>([]);
    const [loadingSearchOperator, setLoadingSearchOperator] = useState<boolean>(false);
    const [searchStringOperator, setSearchStringOperator] = useState('');
    const debouncedSearchTermOperator = useDebounce(searchStringOperator, 500);

    const router = useRouter()
    const currentUrl = router.asPath;
    useEffect(() => {
        getAllSteps()
    }, [])
    useEffect(() => {
        const fetchData = async () => {
            if (debouncedSearchTermCreator.length >= 2) {
                setLoadingSearchCreator(true);
                try {
                    const accessToken = window.localStorage.getItem("accessToken");
                    if (!accessToken) {
                        throw new Error("No access token found");
                    }
                    const headers = { Authorization: `Bearer ${accessToken}` };
                    const response = await axios.get(getAllNhanVienByTuKhoa + '?Key=' + debouncedSearchTermCreator, { headers });
                    if (response.data) {
                        setDropdownOptionCreators(response.data);
                    }
                } catch (error) {
                    console.error("Error fetching data:", error);
                } finally {
                    setLoadingSearchCreator(false);
                }
                console.log('API Call with:', debouncedSearchTermCreator);
            } else {
                setDropdownOptionCreators([]); // Clear options if the search term is too short
            }
        };
        fetchData();
    }, [debouncedSearchTermCreator]);
    useEffect(() => {
        const fetchData = async () => {
            if (debouncedSearchTermOperator.length >= 2) {
                setLoadingSearchOperator(true);
                try {
                    const accessToken = window.localStorage.getItem("accessToken");
                    if (!accessToken) {
                        throw new Error("No access token found");
                    }
                    const headers = { Authorization: `Bearer ${accessToken}` };
                    const response = await axios.get(getAllNhanVienByTuKhoa + '?Key=' + debouncedSearchTermOperator, { headers });

                    if (response.data) {
                        setDropdownOptionOperators(response.data);
                    }
                } catch (error) {
                    console.error("Error fetching data:", error);
                } finally {
                    setLoadingSearchOperator(false);
                }
                console.log('API Call with:', debouncedSearchTermOperator);
            } else {
                setDropdownOptionOperators([]); // Clear options if the search term is too short
            }
        };

        fetchData();
    }, [debouncedSearchTermOperator]);
    const handleChangeText = (event: any) => {
        const inputValue = event.target.value.trim();
        setSearchStringCreator(inputValue);
    };
    const handleChangeTextOperator = (event: any) => {
        const inputValue = event.target.value.trim();
        setSearchStringOperator(inputValue);
    };
    /* Default value */
    const handleFileCompanyChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];
        if (file) {
            setFileCompany(file)
            const reader = new FileReader();
            reader.onload = (e) => {
                if (e.target?.result) {
                    const fileBase64 = e.target.result as string;
                    setFileCompanyBase64(fileBase64)
                }
            };
            reader.readAsDataURL(file);
        }
    };

    const handleFileProjectChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];
        if (file) {
            setFileProject(file)
            const reader = new FileReader();
            reader.onload = (e) => {
                if (e.target?.result) {
                    const fileBase64 = e.target.result as string;
                    setFileProjectBase64(fileBase64)
                }
            };
            reader.readAsDataURL(file);
        }
    };

    const handleDownloadFile = (file: string) => {
        const downloadUrl = `${apiPort}${file}`;
        window.open(downloadUrl, "_blank");
    };

    const onSubmit = async (values: any, { setErrors, setStatus, setSubmitting, resetForm }: any) => {
        console.log({ values });

        values.coQuanID = Number(values?.coQuanID)
        console.log('kkk', values);
        if (dayjs.isDayjs(values.thoiGianKetThucDuKien)) {
            values.thoiGianKetThucDuKien = values.thoiGianKetThucDuKien.format("DD/MM/YYYY HH:mm:ss");
        }

        // Kiểm tra và định dạng `thoiGian`
        if (dayjs.isDayjs(values.thoiGian)) {
            values.thoiGian = values.thoiGian.format("DD/MM/YYYY HH:mm:ss");
        }
        let rs;
        // Nếu như thêm mới
        let loaiDuAnID = null;
        if (currentUrl.includes('/customer/project')) {
            loaiDuAnID = 1;
        } else if (currentUrl.includes('/customer/school')) {
            loaiDuAnID = 2;
        }
        if (!props?.isEdit) {
            values.coQuanID = Number(props?.id)
            values.doanhThuDuKien = Number(values.doanhThuDuKien)
            if (values.buocThiTruongId !== 5) {
                values.fileCoQuan = fileProjectBase64
                values.tenFileCoQuan = fileProject?.name
            }
            else {
                values.fileCoQuan = null
                values.tenFileCoQuan = null
            }
            values.fileCongTy = null,
                values.tenFileCongTy = null,
                values.loaiDuAnID = loaiDuAnID
            rs = await addProjectEstimate(values, currentUrl)
        }
        // Nếu cập nhật
        else {
            // Nếu hoàn thành và đến bước cuối cùng
            if (values.buocThiTruongID === 7) {

                if (values.ketQua === "Thành công") {

                    rs = await updateProjectEstimate({
                        duToanID: Number(props?.id),
                        coQuanID: values.coQuanID,
                        tenDuToan: values.tenDuToan,
                        ketQua: true,
                        doanhThuThucTe: Number(values.doanhThuThucTe),
                        doanhThuDuKien: values?.doanhThuDuKien,
                        fileCoQuan: fileProjectBase64 ? fileProjectBase64 : values.fileCoQuan,
                        tenFileCoQuan: fileProject?.name ? fileProject?.name : values.tenFileCoQuan,
                        nguoiTaoID: creators ? values.nguoiTaoID : null,
                        nguoiVanHanhID: operators ? values.nguoiVanHanhID : null,
                        fileCongTy: values.fileCongTy,
                        tenFileCongTy: values.tenFileCongTy,
                        buocThiTruongID: values?.buocThiTruongID,
                        loaiDuAnID: loaiDuAnID,
                        thoiGian: values?.thoiGian,
                        thoiGianKetThucDuKien: values?.thoiGianKetThucDuKien
                    }, Number(values?.coQuanID!)!, currentUrl)
                }
                else {
                    rs = await updateProjectEstimate({
                        duToanID: Number(props?.id),
                        coQuanID: values.coQuanID,
                        tenDuToan: values.tenDuToan,
                        ketQua: false,
                        lyDoThatBai: values.lyDoThatBai,
                        fileCoQuan: fileProjectBase64 ? fileProjectBase64 : values.fileCoQuan,
                        tenFileCoQuan: fileProject?.name ? fileProject?.name : values.tenFileCoQuan,
                        nguoiTaoID: creators ? values.nguoiTaoID : null,
                        nguoiVanHanhID: operators ? values.nguoiVanHanhID : null,
                        fileCongTy: values.fileCongTy,
                        tenFileCongTy: values.tenFileCongTy,
                        buocThiTruongID: values?.buocThiTruongID,
                        doanhThuDuKien: values?.doanhThuDuKien,
                        loaiDuAnID: loaiDuAnID,
                        thoiGian: values?.thoiGian,
                        thoiGianKetThucDuKien: values?.thoiGianKetThucDuKien
                    }, Number(values?.coQuanID!)!, currentUrl)
                }
            }
            // Nếu chưa hoàn thành tất cả bước
            else {
                values.duToanID = Number(props?.id);
                values.ketQua = valueResult;
                values.fileCoQuan = fileProjectBase64 ? fileProjectBase64 : values.fileCoQuan;
                values.tenFileCoQuan = fileProject?.name ? fileProject?.name : values.tenFileCoQuan;
                values.loaiDuAnID = loaiDuAnID;
                rs = await updateProjectEstimate(values, values?.coQuanID!, currentUrl)
            }
        }
        // console.log("values", values);
        // const rs = props.isEdit ?  : 
        console.log('m là gì ', rs);

        if (rs) {
            toast.success('Thành công')
            resetForm()
            props?.handleOpen!(false)
            setStatus({ success: true });
            setSubmitting(false);
        }
        else {
            setStatus({ success: false });
            setSubmitting(false);
            toast.error('Thất bại')
        }

    };


    useEffect(() => {
        if (props?.defaulValue) {
            if (props.defaulValue.ketQua === true) {
                setValueResult("Thành công");
            }

            setCreators(props?.defaulValue?.nguoiTao);
            console.log('creators none', creators);
            const newDropdownOptions = [...dropdownOptionCreators]; // Create a copy of the existing options
            if (props?.defaulValue?.nguoiTao) {
                newDropdownOptions.push(props?.defaulValue?.nguoiTao); // Push the default value into the array
            }
            setDropdownOptionCreators(newDropdownOptions);
            setOperators(props?.defaulValue?.nguoiVanHanh);
            const newDropdownOptionOperators = [...dropdownOptionOperators]; // Create a copy of the existing options
            if (props?.defaulValue?.nguoiTao) {
                newDropdownOptionOperators.push(props?.defaulValue?.nguoiVanHanh); // Push the default value into the array
            }
            setDropdownOptionOperators(newDropdownOptionOperators);
        }
    }, [props?.defaulValue])
    console.log('creators', creators);
    console.log('dropdown', dropdownOptionCreators);


    return (
        <Formik
            initialValues={{
                coQuanID: props?.defaulValue?.coQuanID,
                tenDuToan: props.defaulValue?.tenDuToan,
                fileCoQuan: props.defaulValue?.fileCoQuan,
                tenFileCoQuan: props.defaulValue?.tenFileCoQuan,
                fileCongTy: props.defaulValue?.fileCongTy,
                tenFileCongTy: props.defaulValue?.tenFileCongTy,
                buocThiTruongID: props.defaulValue?.buocThiTruongID,
                nguoiTaoID: props.defaulValue?.nguoiTaoID,
                nguoiVanHanhID: props.defaulValue?.nguoiVanHanhID,
                doanhThuDuKien: props.defaulValue?.doanhThuDuKien,
                ghiChu: props.defaulValue?.ghiChu,
                ketQua: props.defaulValue ? props.defaulValue.ketQua === false ? "Thất bại" : "Thành công" : null,
                lyDoThatBai: props.defaulValue?.lyDoThatBai,
                doanhThuThucTe: props.defaulValue?.doanhThuThucTe,
                thoiGian: props.defaulValue?.thoiGian ?? dayjs(new Date()),
                thoiGianKetThucDuKien: props.defaulValue?.thoiGianKetThucDuKien ?? dayjs(new Date()),
            }}
            validationSchema={
                valueResult === "Thành công" ?
                    Yup.object().shape({
                        tenDuToan: Yup.string().required('Không được bỏ trống'),
                        buocThiTruongID: Yup.number().min(0).required('Không được bỏ trống'),
                        doanhThuDuKien: Yup.number().min(0).required('Không được bỏ trống'),
                        ketQua: Yup.string().required('Không được bỏ trống'),
                        doanhThuThucTe: Yup.number().required('Không được bỏ trống'),
                    })
                    :
                    valueResult === "Thất bại" ?
                        Yup.object().shape({
                            tenDuToan: Yup.string().required('Không được bỏ trống'),
                            buocThiTruongID: Yup.number().min(0).required('Không được bỏ trống'),
                            doanhThuDuKien: Yup.number().min(0).required('Không được bỏ trống'),
                            ketQua: Yup.string().required('Không được bỏ trống'),
                            lyDoThatBai: Yup.string().required('Không được bỏ trống'),
                        })
                        :
                        Yup.object().shape({
                            // thoiGianKetThucDuKien: Yup.date().required('Thời gian không được bỏ trống'),
                        })
            }
            onSubmit={onSubmit}
        >
            {({ errors, handleBlur, handleChange, handleSubmit, isSubmitting, touched, values, setFieldValue }) => (
                <form noValidate onSubmit={handleSubmit} style={{ width: '100%' }}>
                    <Box display='flex' flexDirection='column' gap={2}>
                        <Grid container spacing={matchDownSM ? 1 : 2}>
                            <Grid item xs={12} sm={12}>
                                <FormControl fullWidth error={Boolean(touched.tenDuToan && errors.tenDuToan)}>
                                    <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>Tên dự toán <span className="required_text">(*)</span>{" "}</Typography>
                                    <CustomInput
                                        name='tenDuToan'
                                        style={{ width: '100%' }}
                                        value={values?.tenDuToan}
                                        onChange={handleChange}
                                    />
                                    {touched.tenDuToan && errors.tenDuToan && (
                                        <FormHelperText error>{errors.tenDuToan.toString()}</FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <FormControl fullWidth error={Boolean(touched.buocThiTruongID && errors.buocThiTruongID)}>
                                    <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>Bước thị trường <span className="required_text">(*)</span>{" "}</Typography>
                                    <Select
                                        name='buocThiTruongID'
                                        style={{ width: '100%' }}
                                        value={values?.buocThiTruongID}
                                        onChange={handleChange}
                                        fullWidth
                                        input={<CustomInput />}
                                    >
                                        {step?.slice(4, 7)?.map((item, index) => (
                                            <MenuItem key={index} value={item.buocThiTruongID}>{item.buocThiTruongTen}</MenuItem>
                                        ))}
                                    </Select>
                                    {touched.buocThiTruongID && errors.buocThiTruongID && (
                                        <FormHelperText error>{errors.buocThiTruongID.toString()}</FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <FormControl fullWidth error={Boolean(touched.doanhThuDuKien && errors.doanhThuDuKien)}>
                                    <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>Doanh thu dự kiến <span className="required_text">(*)</span>{" "}</Typography>
                                    <CustomInput
                                        // type='number'
                                        name='doanhThuDuKien'
                                        style={{ width: '100%' }}
                                        value={formatCurrencyNoUnit(values?.doanhThuDuKien)}
                                        endAdornment={<InputAdornment position="start">VNĐ</InputAdornment>}
                                        onChange={(e) => {
                                            setFieldValue('doanhThuDuKien', Number(removeCommasAndDots(e.target.value)));
                                        }}
                                    />
                                    {touched.doanhThuDuKien && errors.doanhThuDuKien && (
                                        <FormHelperText error>{errors.doanhThuDuKien.toString()}</FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <FormControl fullWidth error={Boolean(touched.thoiGian && errors.thoiGian)}>
                                    <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>Thời gian tiếp xúc <span className="required_text">(*)</span>{" "}</Typography>
                                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                                        <DatePicker
                                            name='thoiGian'
                                            sx={{ width: '100%' }}
                                            value={dayjs(values?.thoiGian, 'DD/MM/YYYY HH:mm:ss')}
                                            onChange={(newValue) => {
                                                setFieldValue('thoiGian', newValue);
                                            }}
                                            format="DD/MM/YYYY"
                                        />
                                    </LocalizationProvider>
                                    {touched.thoiGian && errors.thoiGian && (
                                        <FormHelperText error id="helper-text-thoiGian">{errors.thoiGian.toString()}</FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <FormControl fullWidth error={Boolean(touched.thoiGianKetThucDuKien && errors.thoiGianKetThucDuKien)}>
                                    <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>Thời gian dự kiến kết thúc <span className="required_text">(*)</span>{" "}</Typography>
                                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                                        <DatePicker
                                            name='thoiGianKetThucDuKien'
                                            sx={{ width: '100%' }}
                                            value={dayjs(values?.thoiGianKetThucDuKien, 'DD/MM/YYYY HH:mm:ss')}
                                            onChange={(newValue) => {
                                                setFieldValue('thoiGianKetThucDuKien', newValue);
                                            }}
                                            format="DD/MM/YYYY"
                                        />
                                    </LocalizationProvider>
                                    {touched.thoiGianKetThucDuKien && errors.thoiGianKetThucDuKien && (
                                        <FormHelperText error>{errors.thoiGianKetThucDuKien.toString()}</FormHelperText>
                                    )}
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} md={12}>
                                <Box style={{ width: "100%" }}>
                                    <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>
                                        <span style={{ marginRight: '8px' }}>Người tạo</span>
                                    </Typography>
                                    <Autocomplete
                                        getOptionLabel={(option) => (option?.tenNhanVien)}
                                        loading={loadingSearchCreator}
                                        options={dropdownOptionCreators}
                                        value={creators ?? null}
                                        renderOption={(props, option) => (
                                            <li {...props}>
                                                {option?.tenNhanVien} {/* Display both */}
                                            </li>
                                        )}
                                        onChange={(event, value) => {
                                            if (value) {
                                                setFieldValue('nguoiTaoID', value?.nhanVienID);
                                                setCreators(value);
                                            }
                                        }
                                        }
                                        onInputChange={(event, newInputValue, reason) => {
                                            if (reason === 'clear') {
                                                setCreators(null);
                                                setFieldValue('nguoiTaoID', null);
                                                setDropdownOptionCreators([])
                                                return
                                            }
                                        }}
                                        renderInput={(params) => <TextField onChange={(event) =>
                                            handleChangeText(event)
                                        }  {...params} />}
                                    />
                                    {touched.nguoiTaoID &&
                                        Boolean(errors.nguoiTaoID) && (
                                            <FormHelperText className="required_text">
                                                {" "}
                                                {errors.nguoiTaoID
                                                    ? errors.nguoiTaoID.toString()
                                                    : ""}
                                            </FormHelperText>
                                        )}
                                </Box>
                            </Grid>
                            <Grid item xs={12} md={12}>
                                <Box style={{ width: "100%" }}>
                                    <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>
                                        <span style={{ marginRight: '8px' }}>Người vận hành</span>
                                    </Typography>
                                    <Autocomplete
                                        getOptionLabel={(option) => (option?.tenNhanVien)}
                                        loading={loadingSearchOperator}
                                        options={dropdownOptionOperators}
                                        value={operators ?? null}
                                        renderOption={(props, option) => (
                                            <li {...props}>
                                                {option?.tenNhanVien} {/* Display both */}
                                            </li>
                                        )}
                                        onChange={(event, value) => {
                                            if (value) {
                                                setFieldValue('nguoiVanHanhID', value?.nhanVienID);
                                                setOperators(value);
                                            }
                                        }
                                        }
                                        onInputChange={(event, newInputValue, reason) => {
                                            if (reason === 'clear') {
                                                setOperators(null);
                                                setFieldValue('nguoiVanHanhID', null);
                                                setDropdownOptionOperators([])
                                                return
                                            }
                                        }}
                                        renderInput={(params) => <TextField onChange={(event) =>
                                            handleChangeTextOperator(event)
                                        }  {...params} />}
                                    />
                                    {touched.nguoiVanHanhID &&
                                        Boolean(errors.nguoiVanHanhID) && (
                                            <FormHelperText className="required_text">
                                                {" "}
                                                {errors.nguoiVanHanhID
                                                    ? errors.nguoiVanHanhID.toString()
                                                    : ""}
                                            </FormHelperText>
                                        )}
                                </Box>
                            </Grid>
                            <Grid item xs={12} sm={12}>
                                <FormControl fullWidth error={Boolean(touched.ghiChu && errors.ghiChu)}>
                                    <Typography sx={{ display: 'flex', alignItems: 'center', mb: 1.5, fontWeight: "bold" }}>Ghi chú</Typography>
                                    <CustomInput
                                        name='ghiChu'
                                        multiline
                                        rows={2}
                                        style={{ width: '100%' }}
                                        value={values?.ghiChu}
                                        onChange={handleChange}
                                    />
                                </FormControl>
                            </Grid>

                            {values?.buocThiTruongID === 7 && props?.isEdit &&
                                <>
                                    <Grid item xs={12} sm={12}>
                                        <FormControl fullWidth error={Boolean(touched.ketQua && errors.ketQua)}>
                                            <Typography>Kết quả <span className="required_text">(*)</span>{" "}</Typography>
                                            <Select
                                                style={{ width: '100%' }}
                                                name='ketQua'
                                                value={values?.ketQua}
                                                onChange={(e) => {
                                                    setFieldValue('ketQua', e.target.value);
                                                    setValueResult(e.target.value)

                                                }}
                                                fullWidth
                                                input={<CustomInput />}
                                            >
                                                <MenuItem value='' disabled>Chọn trạng thái</MenuItem>
                                                {["Thành công", "Thất bại"]?.map((item, index) => (
                                                    <MenuItem key={index} value={item}>{item}</MenuItem>
                                                ))}
                                            </Select>
                                            {touched.ketQua && errors.ketQua && (
                                                <FormHelperText error>{errors.ketQua.toString()}</FormHelperText>
                                            )}
                                        </FormControl>
                                    </Grid>
                                    {values.ketQua === "Thành công" ?
                                        <Grid item xs={12} sm={12}>
                                            <FormControl fullWidth error={Boolean(touched.doanhThuThucTe && errors.doanhThuThucTe)}>
                                                <Typography>Doanh thu thực tế <span className="required_text">(*)</span>{" "}</Typography>
                                                <CustomInput
                                                    // type='number'
                                                    name='doanhThuThucTe'
                                                    style={{ width: '100%' }}
                                                    value={formatCurrencyNoUnit(values?.doanhThuThucTe!)}
                                                    endAdornment={<InputAdornment position="start">VNĐ</InputAdornment>}
                                                    onChange={(e) => {
                                                        setFieldValue('doanhThuThucTe', Number(removeCommasAndDots(e.target.value)));
                                                    }}
                                                />
                                                {touched.doanhThuThucTe && errors.doanhThuThucTe && (
                                                    <FormHelperText error>{errors.doanhThuThucTe.toString()}</FormHelperText>
                                                )}
                                            </FormControl>
                                        </Grid>
                                        :
                                        values.ketQua === "Thất bại" ?
                                            <Grid item xs={12} sm={12}>
                                                <FormControl fullWidth error={Boolean(touched.lyDoThatBai && errors.lyDoThatBai)}>
                                                    <Typography>Lý do thất bại <span className="required_text">(*)</span>{" "}</Typography>
                                                    <CustomInput
                                                        // type='number'
                                                        multiline
                                                        rows={3}
                                                        name='lyDoThatBai'
                                                        style={{ width: '100%' }}
                                                        value={values?.lyDoThatBai}
                                                        onChange={(e) => {
                                                            setFieldValue('lyDoThatBai', e.target.value);
                                                        }}
                                                    />
                                                    {touched.lyDoThatBai && errors.lyDoThatBai && (
                                                        <FormHelperText error>{errors.lyDoThatBai.toString()}</FormHelperText>
                                                    )}
                                                </FormControl>
                                            </Grid>
                                            : null
                                    }
                                </>


                            }


                            <Grid item xs={12} sm={12}>
                                <FormControl fullWidth error={Boolean(touched.fileCoQuan && errors.fileCoQuan)}>
                                    <Box sx={{ width: '100%', }}>
                                        <Typography>File dự toán <span className="required_text">(*)</span>{" "}</Typography>
                                        <Box display="flex" justifyContent="space-between" alignItems="center" py={1}>

                                            {fileProject?.name ?
                                                <Chip
                                                    variant='outlined'
                                                    sx={{ borderRadius: "8px" }}
                                                    label={<Typography>{fileProject?.name}</Typography>}
                                                    onDelete={() => setFileProject(null)}
                                                />
                                                :
                                                <label htmlFor="raised-button-file-project">
                                                    <Button

                                                        startIcon={<IconUpload size={18} stroke={2.5} />}
                                                        sx={{ borderRadius: "8px", textTransform: "none", fontWeight: 400 }}
                                                        variant="outlined"
                                                        size='large'
                                                        fullWidth
                                                        component="span" >
                                                        Tải file từ máy tính
                                                    </Button>
                                                </label>
                                            }
                                        </Box>

                                        <form className='form'>
                                            <input
                                                // accept=".xlsx,.xls,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                                style={{ display: 'none' }}
                                                id="raised-button-file-project"
                                                multiple
                                                onChange={handleFileProjectChange}
                                                type="file"
                                            />
                                        </form>
                                    </Box>
                                    {touched.fileCoQuan && errors.fileCoQuan && (
                                        <FormHelperText error>{errors.fileCoQuan.toString()}</FormHelperText>
                                    )}
                                </FormControl>
                                {
                                    props?.defaulValue?.tenFileCoQuan && <>
                                        <Box sx={{ position: 'relative', mb: 1 }}>
                                            <Typography variant="body1" component="span" sx={{ cursor: 'pointer' }}>
                                                {props?.defaulValue?.tenFileCoQuan}
                                            </Typography>
                                            <IconButton>
                                                <FileDownloadIcon
                                                    sx={{ ml: 1, cursor: 'pointer' }}
                                                    onClick={() => handleDownloadFile(props.defaulValue.fileCoQuan!)}
                                                />
                                            </IconButton>
                                        </Box>

                                    </>
                                }
                            </Grid>
                            {/* <Grid item xs={12} sm={12}>
                                <FormControl fullWidth error={Boolean(touched.fileCongTy && errors.fileCongTy)}>
                                    <Box sx={{ width: '100%', }}>
                                        <Typography>File công ty</Typography>
                                        <Box display="flex" justifyContent="space-between" alignItems="center" py={1}>

                                            {fileCompany?.name ?
                                                <Chip
                                                    variant='outlined'
                                                    sx={{ borderRadius: "8px" }}
                                                    label={<Typography>{fileCompany?.name}</Typography>}
                                                    onDelete={() => setFileCompany(null)}
                                                />
                                                :
                                                <label htmlFor="raised-button-file">
                                                    <Button variant="outlined" size='large' fullWidth component="span" >
                                                        Tải file từ máy tính
                                                    </Button>
                                                </label>
                                            }

                                        </Box>

                                        <form className='form'>
                                            <input
                                                // accept=".xlsx,.xls,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                                style={{ display: 'none' }}
                                                id="raised-button-file"
                                                multiple
                                                onChange={handleFileCompanyChange}
                                                type="file"
                                            />
                                        </form>
                                    </Box>
                                    {touched.fileCongTy && errors.fileCongTy && (
                                        <FormHelperText error>{errors.fileCongTy.toString()}</FormHelperText>
                                    )}
                                </FormControl>
                            </Grid> */}
                        </Grid>
                    </Box>
                    <Box sx={{ mt: 2 }}>
                        <AnimateButton>
                            <Button disableElevation disabled={isSubmitting} fullWidth size="large" type="submit" variant="contained">
                                {props.buttonActionText}
                            </Button>
                        </AnimateButton>
                    </Box>
                </form>
            )}
        </Formik >
    )
}
export default FormReportInteractValue