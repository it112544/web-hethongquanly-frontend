
export const QUAN_TRI_DOI_TAC ='Quản trị đối tác'

export const QUAN_TRI ='Admin'

export const TONG_GIAM_DOC ='Tổng giám đốc'
export const PHO_TONG_GIAM_DOC ='Phó tổng giám đốc'

export const BAN_PHAP_CHE_HC_NS_NHAN_VIEN = "Ban pháp chế, hành chính, nhân sự - Nhân viên"
export const BAN_PHAP_CHE_HC_NS_TRUONG_BAN = "Ban pháp chế, hành chính, nhân sự - Trưởng ban"
export const BAN_PHAP_CHE_HC_NS_PHO_BAN = "Ban pháp chế, hành chính, nhân sự - Phó ban"

export const BAN_SAN_PHAM_NHAN_VIEN = "Ban sản phẩm - Nhân viên"
export const BAN_SAN_PHAM_TRUONG_BAN = "Ban sản phẩm - Trưởng ban"
export const BAN_SAN_PHAM_PHO_BAN = "Ban sản phẩm - Phó ban"

export const BAN_TAI_CHINH_KE_HOACH_NHAN_VIEN = "Ban tài chính, kế hoạch - Nhân viên"
export const BAN_TAI_CHINH_KE_HOACH_TRUONG_BAN = "Ban tài chính, kế hoạch - Trưởng ban"
export const BAN_TAI_CHINH_KE_HOACH_PHO_BAN = "Ban tài chính, kế hoạch - Phó ban"

export const BAN_THI_TRUONG_TRUONG_BAN ='Ban thị trường - Trưởng ban'
export const BAN_THI_TRUONG_PHO_BAN ='Ban thị trường - Phó ban'
export const BAN_THI_TRUONG_GIAM_DOC_DU_AN ='Ban thị trường - Giám đốc dự án'
export const BAN_THI_TRUONG_GIAM_DOC_CHI_NHANH ='Ban thị trường - Giám đốc chi nhánh'
export const BAN_THI_TRUONG_GIAM_DOC_KINH_DOANH ='Ban thị trường - Giám đốc kinh doanh'
export const BAN_THI_TRUONG_NHAN_VIEN_KINH_DOANH ='Ban thị trường - Nhân viên kinh doanh'

export const NHAN_VIEN ='Nhân viên'