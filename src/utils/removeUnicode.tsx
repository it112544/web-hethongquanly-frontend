export default function removeUnicode(str: string): string {
    if (!str) return '';
    return str
        .normalize('NFD') // Tách các ký tự dấu ra khỏi ký tự gốc
        .replace(/[\u0300-\u036f]/g, '') // Loại bỏ các ký tự dấu
        .replace(/đ/g, 'd') // Chuyển đ thành d
        .replace(/Đ/g, 'D'); // Chuyển Đ thành D
}


