export function stringToSlug(str: string) {
    if (!str) {
        return ''; // Return an empty string if the input is undefined or null
    }

    // Remove accents
    var from = "àáãảạăằắẳẵặâầấẩẫậèéẻẽẹêềếểễệđùúủũụưừứửữựòóỏõọôồốổỗộơờớởỡợìíỉĩịäëïîöüûñçýỳỹỵỷ",
        to = "aaaaaaaaaaaaaaaaaeeeeeeeeeeeduuuuuuuuuuuoooooooooooooooooiiiiiaeiiouuncyyyyy";
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(RegExp(from[i], "gi"), to[i]);
    }

    // Convert to lowercase, trim, and replace non-alphanumeric characters with hyphens
    str = str.toLowerCase()
        .trim()
        .replace(/[^a-z0-9\-]/g, '-')  // Replace invalid characters with hyphen
        .replace(/-+/g, '-');          // Collapse multiple hyphens into one

    return str.toLocaleLowerCase();
}
